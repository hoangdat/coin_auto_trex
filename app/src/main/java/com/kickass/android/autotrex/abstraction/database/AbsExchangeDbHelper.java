package com.kickass.android.autotrex.abstraction.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

/**
 * Created by dat.ph on 12/14/2017.
 */

public abstract class AbsExchangeDbHelper extends SQLiteOpenHelper {
    public AbsExchangeDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public synchronized Cursor query(String table, String[] projection, String selection,
                        String[] selectionArgs, String groupBy, String having,
                        String orderBy, String limit) {
        SQLiteDatabase db = getReadableDatabase();
        return db.query(table, projection, selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    public long insert(String table, @Nullable ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        return db.insert(table, null, values);
    }

    public int delete(String table, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(table, selection, selectionArgs);
    }

    public int update(String table, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = getWritableDatabase();
        return db.update(table, values, selection, selectionArgs);
    }
}
