package com.kickass.android.autotrex.service.filter;

import android.content.Context;

import com.kickass.android.autotrex.listener.ListenerMgr;

/**
 * Created by dat.ph on 1/22/2018.
 */

public abstract class AbsFilter {
    private ListenerMgr mListenerMgr;
    private Context mContext;

    public AbsFilter(Context context) {
        mListenerMgr = new ListenerMgr();
        mContext = context;
    }

    public void create() {
        mListenerMgr.notifyCreate();
    }

    public void destroy() {
        mListenerMgr.notifyDestroy();
    }

    public Context getContext() {
        return mContext;
    }

    public ListenerMgr getListenerMgr() {
        return mListenerMgr;
    }
}
