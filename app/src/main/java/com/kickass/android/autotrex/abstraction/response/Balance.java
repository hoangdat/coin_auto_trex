package com.kickass.android.autotrex.abstraction.response;

/**
 * Created by dat.ph on 12/8/2017.
 */

public class Balance {
    private String mCurrency;
    private double mBalance;
    private double mAvailable;
    private double mPending;
    private String mAddress;

    public Balance(String mCurrency, double mBalance, double mAvailable, double mPending, String mAddress) {
        this.mCurrency = mCurrency;
        this.mBalance = mBalance;
        this.mAvailable = mAvailable;
        this.mPending = mPending;
        this.mAddress = mAddress;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String mCurrency) {
        this.mCurrency = mCurrency;
    }

    public double getBalance() {
        return mBalance;
    }

    public void setBalance(double mBalance) {
        this.mBalance = mBalance;
    }

    public double getAvailable() {
        return mAvailable;
    }

    public void setAvailable(double mAvailable) {
        this.mAvailable = mAvailable;
    }

    public double getPending() {
        return mPending;
    }

    public void setPending(double mPending) {
        this.mPending = mPending;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    @Override
    public String toString() {
        return this.getCurrency() + ": " + this.getBalance() + " at address: " + this.getAddress();
    }
}
