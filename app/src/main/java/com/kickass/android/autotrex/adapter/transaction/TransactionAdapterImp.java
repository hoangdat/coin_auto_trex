package com.kickass.android.autotrex.adapter.transaction;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.abstraction.database.table.TransactionTable;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.adapter.AbsCursorAdapterImp;
import com.kickass.android.autotrex.adapter.AppViewHolder;
import com.kickass.android.autotrex.adapter.IndexViewHolder;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.provider.ExchangeProvider;
import com.kickass.android.autotrex.util.Utils;

/**
 * Created by dat.ph on 1/25/2018.
 */

public class TransactionAdapterImp extends AbsCursorAdapterImp {
    public TransactionAdapterImp(Context context, int layoutId) {
        super(context, layoutId);
    }

    @Override
    protected AppViewHolder _createViewHolder(View itemView) {
        AppViewHolder appViewHolder = AppViewHolder.createViewHolder(itemView, IndexViewHolder.MAX);

        appViewHolder.addView(IndexViewHolder.MH_MARKET_NAME, itemView.findViewById(R.id.trs_txtMarketName));
        appViewHolder.addView(IndexViewHolder.TR_PRICE_BUY_VALUE, itemView.findViewById(R.id.trs_txtPriceValue));
        appViewHolder.addView(IndexViewHolder.TR_QUANTITY_VALUE, itemView.findViewById(R.id.trs_txtQuantityValue));
        appViewHolder.addView(IndexViewHolder.TR_PRICE_SELL_VALUE, itemView.findViewById(R.id.trs_txtPrSellValue));
        appViewHolder.addView(IndexViewHolder.TR_TIME_BUY_VALUE, itemView.findViewById(R.id.trs_txtTimeBuyValue));
        appViewHolder.addView(IndexViewHolder.TR_TIME_SELL_VALUE, itemView.findViewById(R.id.trs_timeSellValue));
        appViewHolder.addView(IndexViewHolder.TR_STT_VALUE, itemView.findViewById(R.id.trs_txtStatusValue));

        return appViewHolder;
    }

    @Override
    protected void _bindView(AppViewHolder holder, int position) {
        Cursor cursor = getCursor();
        if (cursor == null || cursor.isClosed() || cursor.getCount() <= position) {
            Log.d(this, "_bindView cursor not proper");
            return;
        }

        try {
            cursor.moveToPosition(position);
            String marketName = cursor.getString(cursor.getColumnIndex(TransactionTable.TransactionColumns.MARKET_NAME));
            Double priceBuy = cursor.getDouble(cursor.getColumnIndex(TransactionTable.TransactionColumns.PRICE_BUY));
            Double quantity = cursor.getDouble(cursor.getColumnIndex(TransactionTable.TransactionColumns.QUANTITY));
            Double priceSell = cursor.getDouble(cursor.getColumnIndex(TransactionTable.TransactionColumns.PRICE_SELL));
            Long timeBuy = cursor.getLong(cursor.getColumnIndex(TransactionTable.TransactionColumns.TIME_BUY));
            Long timeSell = cursor.getLong(cursor.getColumnIndex(TransactionTable.TransactionColumns.TIME_SELL));
            String status = cursor.getString(cursor.getColumnIndex(TransactionTable.TransactionColumns.STATUS));

            TextView txtName = holder.getView(IndexViewHolder.MH_MARKET_NAME, TextView.class);
            if (txtName != null) {
                txtName.setText(marketName);
            }

            TextView txtPriceBuyValue = holder.getView(IndexViewHolder.TR_PRICE_BUY_VALUE, TextView.class);
            if (txtPriceBuyValue != null) {
                txtPriceBuyValue.setText(String.format("%f", priceBuy != null ? priceBuy.doubleValue() : null));
            }

            TextView txtQuantity = holder.getView(IndexViewHolder.TR_QUANTITY_VALUE, TextView.class);
            if (txtQuantity != null) {
                txtQuantity.setText(String.format("%f", quantity != null ? quantity.doubleValue() : null));
            }

            TextView txtPriceSellValue = holder.getView(IndexViewHolder.TR_PRICE_SELL_VALUE, TextView.class);
            if (txtPriceSellValue != null) {
                txtPriceSellValue.setText(String.format("%f", priceSell != null ? priceSell.doubleValue() : null));
            }

            TextView txtTimeBuy = holder.getView(IndexViewHolder.TR_TIME_BUY_VALUE, TextView.class);
            if (txtTimeBuy != null) {
                txtTimeBuy.setText(timeBuy != null ? Utils.convertMillisecondToDateTime(timeBuy) : null);
            }

            TextView txtTimeSell = holder.getView(IndexViewHolder.TR_TIME_SELL_VALUE, TextView.class);
            if (txtTimeSell != null) {
                txtTimeSell.setText(timeSell != null ? Utils.convertMillisecondToDateTime(timeSell) : null);
            }

            TextView txtStatus = holder.getView(IndexViewHolder.TR_STT_VALUE, TextView.class);
            if (txtStatus != null) {
                txtStatus.setText(status);
            }

        } catch (IllegalStateException e) {
            Log.e(this, "_bindView IllegalStateException:" + e.getMessage());
        }
    }

    @Override
    public Uri getUri() {
        return ExchangeProvider.UriString.getTransactionUri(ExchangeOperationMgr.ExchangeType.Bittrex);
    }

    @Override
    public String[] getProjection() {
        return null;
    }
}
