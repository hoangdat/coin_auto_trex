package com.kickass.android.autotrex.adapter;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import com.kickass.android.autotrex.log.Log;

/**
 * Created by dat.ph on 1/10/2018.
 */

public abstract class AbsCursorAdapterImp extends AbsAdapterImp {
    private Cursor mCursor;
    private ContentObserver mContentChange;

    public AbsCursorAdapterImp(Context context, int layoutId) {
        super(context, layoutId);
        mContentChange = new ChangeObserver();
    }

    public void changeCursor(Cursor newCursor) {
        Log.d(this, "changeCursor(): old = " + mCursor + "-new = " + newCursor);
        if (newCursor == mCursor) {
            return;
        }

        Cursor oldCursor = mCursor;
        if (oldCursor != null) {
            if (mContentChange != null) {
//                Log.d(this, "changeCursor(): unregister old");
                oldCursor.unregisterContentObserver(mContentChange);
            }
            oldCursor.close();
        }
        mCursor = newCursor;
        if (newCursor != null) {
            if (mContentChange != null) {
//                Log.d(this, "changeCursor(): register new");
                newCursor.registerContentObserver(mContentChange);
            }
        }

        notifyDataSetChanged();
    }

    public Cursor getCursor() {
        return mCursor;
    }

    public abstract Uri getUri();
    public abstract String[] getProjection();

    private class ChangeObserver extends ContentObserver {
        public ChangeObserver() {
            super(new Handler());
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
//            Log.d(this, "onChange");
            onContentChanged();
        }
    }

    protected void onContentChanged() {
        if (mCursor != null && !mCursor.isClosed()) {
            Log.d(this, "Auto requerying " + mCursor + " due to update");
            mCursor.requery();
        }
    }

    @Override
    protected int _getItemCount() {
        Cursor c = getCursor();
        int ret = 0;
        if (c != null) {
            ret = c.getCount();
        }
        return ret;
    }
}
