package com.kickass.android.autotrex.bittrex.operation;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import com.kickass.android.autotrex.abstraction.AutoTrexRequest;
import com.kickass.android.autotrex.abstraction.database.table.MarketHistoryTable;
import com.kickass.android.autotrex.abstraction.database.table.TransactionTable;
import com.kickass.android.autotrex.abstraction.operation.AbsOperation;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.Balance;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.abstraction.response.PriceHistory;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.abstraction.transaction.TransactionMgr;
import com.kickass.android.autotrex.bittrex.request.BuyLimitRequest;
import com.kickass.android.autotrex.bittrex.request.GetBalanceRequest;
import com.kickass.android.autotrex.bittrex.request.GetBalancesRequest;
import com.kickass.android.autotrex.bittrex.request.GetMarketHistoryRequest;
import com.kickass.android.autotrex.bittrex.request.GetMarketOrderHistoryRequest;
import com.kickass.android.autotrex.bittrex.request.GetMarketsRequest;
import com.kickass.android.autotrex.bittrex.request.GetMarketSumRequest;
import com.kickass.android.autotrex.bittrex.request.GetOpenOrdersRequest;
import com.kickass.android.autotrex.bittrex.request.GetOrderRequest;
import com.kickass.android.autotrex.bittrex.request.GetSummariesRequest;
import com.kickass.android.autotrex.bittrex.request.GetTickerRequest;
import com.kickass.android.autotrex.bittrex.request.SellLimitRequest;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.provider.ExchangeProvider;
import com.kickass.android.autotrex.util.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class BittrexOperation extends AbsOperation{
    private static final Uri BITTREX_MARKET_HISTORY = ExchangeProvider.UriString.getMarketHistoryUri(ExchangeOperationMgr.ExchangeType.Bittrex);
    private static final int BULK_OPERATION_LIMIT = 250;

    public BittrexOperation(Context context) {
        super(context);
    }

    @Override
    public HashMap<String, MarketItem> getMarkets() {
        HashMap<String, MarketItem> market = null;
        Log.d(this, "getMarkets");
        try {
            GetMarketsRequest request = (GetMarketsRequest)AutoTrexRequest.getInstance(getContext()).getRequest(AutoTrexRequest.RequestType.GET_MARKETS);
            market = request.execute(getContext());
        } catch (Exception e) {
            Log.e(this, "Exception e = " + e.getMessage());
        }

        return market;
    }

    @Override
    public MarketSummary getMarketSum(String market) {
        MarketSummary sum = null;
        Log.d(this, "getMarketSum");
        try {
            GetMarketSumRequest request = (GetMarketSumRequest) AutoTrexRequest.getInstance(getContext()).getRequest(AutoTrexRequest.RequestType.GET_MARKET_SUM, market);
            sum = request.execute(getContext());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this, "Exception e = " + e.getMessage());
        }
        return sum;
    }

    @Override
    public List<MarketSummary> getSummaries() {
        Log.d(this, "getSummaries");
        List<MarketSummary> summaries = null;
        try {
            GetSummariesRequest request = (GetSummariesRequest) AutoTrexRequest.getInstance(getContext()).getRequest(AutoTrexRequest.RequestType.GET_MARKET_SUMMARIES);
            summaries = request.execute(getContext());
        } catch (Exception e) {
            Log.e(this, "Exception e: " + e.getMessage());
        }
        return summaries;
    }

    @Override
    public ArrayList<Balance> getBalances() {
        Log.d(this, "getBalances");
        ArrayList<Balance> balances = null;
        try {
            GetBalancesRequest request = (GetBalancesRequest) AutoTrexRequest.getInstance(getContext()).getRequest(AutoTrexRequest.RequestType.GET_BALANCES);
            balances = request.execute(getContext());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this, "getBalances Exception e = " + e.getMessage());
        }
        return balances;
    }

    @Override
    public Balance getBalance(String currency) {
        Log.d(this, "getBalance");
        Balance balance = null;
        try {
            GetBalanceRequest request = (GetBalanceRequest) AutoTrexRequest.getInstance(getContext())
                    .getRequest(AutoTrexRequest.RequestType.GET_BALANCE, currency);
            balance = request.execute(getContext());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this, "getBalances Exception e = " + e.getMessage());
        }
        return balance;
    }

    public String buyLimit(Order order) {
        Log.d(this, "buyLimit()");
        String uuid = null;
        if (order == null) {
            return uuid;
        }

        MarketItem.BasedMarketType basedMarketType = order.getBaseMarketType();
        String market = order.getMarketName();
        double quantity = order.getQuantity();
        double rate = order.getPrice();
        double expectedTP = order.getExpectedTP();
        double expectedSL = order.getExpectedSL();
        Log.d(this, "toBuyContentValues() rate = " + rate + "-exTP = " + expectedTP + "-exSL = " + expectedSL);
        try {
            Log.d(this, "buyLimit() prepare call");
//            BuyLimitRequest request = (BuyLimitRequest) AutoTrexRequest.getInstance(getContext())
//                    .getRequest(AutoTrexRequest.RequestType.BUY_LIMIT, market, Double.toString(quantity), Double.toString(rate));
//            uuid = request.execute(getContext());
            uuid = Long.toString(System.currentTimeMillis()) + "buyLimit";

            Log.d(this, "buyLimit() uuid = " + uuid);
            if (uuid == null) {
                Log.d(this, "buyLimit uuid null");
                return uuid;
            }

            Uri uri = ExchangeProvider.UriString.getTransactionUri(getExchangeType());
            ContentValues values = toBuySellContentValues(basedMarketType, market, rate, quantity, expectedTP,
                                                expectedSL, uuid, TransactionMgr.StatusType.BOUGHT);
            if (values != null) {
                Log.d(this, "buyLimit insert");
                Uri uriIn = getContext().getContentResolver().insert(uri, values);
                getContext().getContentResolver().notifyChange(ExchangeProvider.UriString.getTransactionUri(ExchangeOperationMgr.ExchangeType.Bittrex), null);
                Log.d(this, "buyLimit insert ok = " + uriIn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this, "buyLimit Exception e = " + e.getMessage());
        }

        Log.d(this, "buyLimit ret = " + uuid);
        return uuid;
    }

    @Override
    public String sellLimit(Order order) {
        Log.d(this, "sellLimit");
        String uuid = null;
        if (order == null) {
            Log.d(this, "sellLimit order null");
            return uuid;
        }

        String market = order.getMarketName();
        double quantity = order.getQuantity();
        double rate = order.getPriceEnd();

        try {
//            SellLimitRequest request = (SellLimitRequest) AutoTrexRequest.getInstance(getContext()).
//                    getRequest(AutoTrexRequest.RequestType.SELL_LIMIT, market, Double.toString(quantity), Double.toString(rate));
//            uuid = request.execute(getContext());
            uuid = Long.toString(System.currentTimeMillis()) + "sellLimit";

            if (uuid == null) {
                Log.d(this, "sellLimit uuid null");
                return  uuid;
            }

            order.setSellUuid(uuid);
            Uri uri = ExchangeProvider.UriString.getTransactionUri(getExchangeType());
            ContentValues values = toUpdateOrderContentValues(TransactionMgr.StatusType.SOLD, order);
            String where = TransactionTable.TransactionColumns.BUY_UUID + "=? ";
            if (values != null) {
                int update = getContext().getContentResolver().update(uri, values, where, new String[]{order.getBuyUuid()});
                Log.d(this, "sellLimit update = " + update);
                if (update < 0) {
                    getContext().getContentResolver().insert(uri, values);
                }
                getContext().getContentResolver().notifyChange(ExchangeProvider.UriString.getTransactionUri(ExchangeOperationMgr.ExchangeType.Bittrex), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this, "sellLimit Exception e = " + e.getMessage());
        }
        return uuid;
    }

    @Override
    public Ticker getTicker(String market) {
        Log.d(this, "getTicker() " + market);
        Ticker ticker = null;
        try {
            GetTickerRequest request = (GetTickerRequest) AutoTrexRequest.getInstance(getContext()).
                    getRequest(AutoTrexRequest.RequestType.GET_TICKER, market);
            ticker = request.execute(getContext());
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(this, "sellLimit Exception e = " + e.getMessage());
        }
        return ticker;
    }

    @Override
    public ArrayList<PriceHistory> getPriceHistory(String market) {
        Log.d(this, "getPriceHistory");
        ArrayList<PriceHistory> priceHistory = null;
        try {
            GetMarketHistoryRequest request = (GetMarketHistoryRequest) AutoTrexRequest.getInstance(getContext())
                    .getRequest(AutoTrexRequest.RequestType.GET_MARKET_HISTORY, market);
            priceHistory = request.execute(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return priceHistory;
    }

    @Override
    public boolean updateMarketHistory(String market, long internalTime) {
        Log.d(this, "updateMarketHistory");
        try {
            GetMarketHistoryRequest request = (GetMarketHistoryRequest) AutoTrexRequest.getInstance(getContext())
                    .getRequest(AutoTrexRequest.RequestType.GET_MARKET_HISTORY, market);
            ArrayList<PriceHistory> priceHistory = request.execute(getContext());

            if (priceHistory != null && !priceHistory.isEmpty()) {
                putMarketHistoryDb(priceHistory, internalTime);
                getContext().getContentResolver().notifyChange(ExchangeProvider.UriString.getMarketHistoryListUri(ExchangeOperationMgr.ExchangeType.Bittrex, market), null);
                getContext().getContentResolver().notifyChange(ExchangeProvider.UriString.getMarketHistoryListVolumeUri(ExchangeOperationMgr.ExchangeType.Bittrex), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(this, "updateMarketHistory");
        return true;
    }

    @Override
    public ExchangeOperationMgr.ExchangeType getExchangeType() {
        return ExchangeOperationMgr.ExchangeType.Bittrex;
    }

    @Override
    public Order getOrder(String uuid) {
        Log.d(this, "getOrder");
        Order order = null;
        try {
            GetOrderRequest request = (GetOrderRequest) AutoTrexRequest.getInstance(getContext()).getRequest(AutoTrexRequest.RequestType.GET_ORDER, uuid);
            order = request.execute(getContext());
        } catch (Exception e) {
            Log.d(this, "getOrder() e = " + e.getMessage());
        }
        return order;
    }

    @Override
    public ArrayList<Order> getOpenOrders() {
        Log.d(this, "getOpenOrders");
        ArrayList<Order> orders = new ArrayList<>();
        try {
            GetOpenOrdersRequest request = (GetOpenOrdersRequest) AutoTrexRequest.getInstance(getContext()).getRequest(AutoTrexRequest.RequestType.GET_OPEN_ORDERS);
            orders = request.execute(getContext());
        } catch (Exception e) {
            Log.d(this, "getOrder() e = " + e.getMessage());
        }
        return orders;
    }

    @Override
    public void updateOrderInfo(TransactionMgr.StatusType statusType, Order order) {
        Log.d(this, "updateOrderInfo() status = " + statusType);
        if (order == null) {
            return;
        }
        Uri uri = ExchangeProvider.UriString.getTransactionUri(getExchangeType());
        ContentValues values = toUpdateOrderContentValues(statusType, order);
        String uuid = order.getBuyUuid();
        Log.d(this, "updateOrderInfo() status = " + statusType + "-buy uuid: " + uuid);
        String where = TransactionTable.TransactionColumns.BUY_UUID + "=?";
        if (uuid != null && values != null) {
            int update = getContext().getContentResolver().update(uri, values, where, new String[]{uuid});
            Log.d(this, "updateOrderInfo() status = " + statusType + "-uuid: " + uuid + "-update = " + update);
            if (update < 0) {
                getContext().getContentResolver().insert(uri, values);
            }
            getContext().getContentResolver().notifyChange(ExchangeProvider.UriString.getTransactionUri(ExchangeOperationMgr.ExchangeType.Bittrex), null);
        }
    }

    @Override
    public ArrayList<Order> getMarketOrderHistory(String market) {
        Log.d(this, "getMarketOrderHistory(): " + market);
        ArrayList<Order> orders = new ArrayList<>();
        if (market == null) {
            return null;
        }

        try {
            GetMarketOrderHistoryRequest request = (GetMarketOrderHistoryRequest) AutoTrexRequest.getInstance(getContext())
                    .getRequest(AutoTrexRequest.RequestType.GET_MARKET_ORDER_HISTORY, market);
            orders = request.execute(getContext());
        } catch (Exception e) {
            Log.d(this, "getMarketOrderHistory(): exception " + e.getMessage());
            e.printStackTrace();
        }

        return orders;
    }

    private void putMarketHistoryDb(Collection<PriceHistory> priceHistories, long internalTime) {
        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();

        long internalAgo;
        ContentProviderOperation.Builder op;
        //update new history
        final String where = MarketHistoryTable.MarketHistoryColumns.HISTORY_ID + "=?";
        for (PriceHistory priceHistory : priceHistories) {
//            Log.d(this, "putMarketHistoryDb() id = " + priceHistory.getHistoryId() + "-price: " + priceHistory.getMarketName());
            try (Cursor cursor = getContext().getContentResolver().query(BITTREX_MARKET_HISTORY,
                                                new String[]{MarketHistoryTable.MarketHistoryColumns.HISTORY_ID,
                                                             MarketHistoryTable.MarketHistoryColumns.QUANTITY},
                                                where, new String[]{Long.toString(priceHistory.getHistoryId())}, null)) {

                ContentValues values = toMarketHistoryContentValues(priceHistory);

                if (cursor != null && cursor.moveToFirst()) {
//                    Log.d(this, "putMarketHistoryDb() update id = " + priceHistory.getHistoryId());
                    double quantity = cursor.getDouble(cursor.getColumnIndex(MarketHistoryTable.MarketHistoryColumns.QUANTITY));
                    if (quantity != priceHistory.getQuantity()) {
//                        Log.d(this, "putMarketHistoryDb() update id = " + priceHistory.getHistoryId() + " with new quantity");
                        op = ContentProviderOperation.newUpdate(BITTREX_MARKET_HISTORY).withValues(values)
                                .withSelection(where, new String[]{Long.toString(priceHistory.getHistoryId())});
                        operationList.add(op.build());
                    }
                } else {
//                    Log.d(this, "putMarketHistoryDb() insert id = " + priceHistory.getHistoryId() + "-time = " + priceHistory.getTimeStamp() + "-at: " + Utils.convertMillisecondToDateTime(priceHistory.getTimeStamp()));
                    op = ContentProviderOperation.newInsert(BITTREX_MARKET_HISTORY).withValues(values);
                    operationList.add(op.build());
                }
            } catch (Exception e) {
                Log.e(this, "putMarketHistoryDb: " + e.getMessage());
            }

            if (operationList.size() >= BULK_OPERATION_LIMIT) {
//                Log.d(this, "putMarketHistoryDb() apply with size = BULK_LIMIT");
                applyDB(operationList);
            }
        }

        //delete old history
        try {
            internalAgo = Utils.convertLocalTimeToGMT(System.currentTimeMillis()) - internalTime;
            final String whereOldHistory = MarketHistoryTable.MarketHistoryColumns.TIME_DONE + "<=?";
//            Log.d(this, "putMarketHistoryDb() delete old history with time < " + internalAgo + "(" + Utils.convertMillisecondToDateTime(internalAgo) + ")");
            op = ContentProviderOperation.newDelete(BITTREX_MARKET_HISTORY).withSelection(whereOldHistory, new String[]{Long.toString(internalAgo)});
            operationList.add(op.build());
        } catch (ParseException e) {
            Log.e(this, "putMarketHistoryDb() e: " + e.getMessage());
        }

        if (operationList.size() > 0) {
//            Log.d(this, "putMarketHistoryDb() apply with size = " + operationList.size());
            applyDB(operationList);
        }
    }

    private void applyDB(ArrayList<ContentProviderOperation> operationList) {
        try {
            getContext().getContentResolver().applyBatch(ExchangeProvider.UriString.EXCHANGE_AUTHORITY, operationList);
        } catch (RemoteException e) {
            Log.e(this, "applyDB: " + e.getMessage());
        } catch (OperationApplicationException e) {
            Log.e(this, "applyDB: " + e.getMessage());
        } finally {
            operationList.clear();
        }
    }

    private ContentValues toMarketHistoryContentValues(PriceHistory priceHistory) {
        ContentValues values = new ContentValues();

        values.put(MarketHistoryTable.MarketHistoryColumns.HISTORY_ID, priceHistory.getHistoryId());
        values.put(MarketHistoryTable.MarketHistoryColumns.MARKET_NAME, priceHistory.getMarketName());
        values.put(MarketHistoryTable.MarketHistoryColumns.ORDER_TYPE, priceHistory.getOrderType());
        values.put(MarketHistoryTable.MarketHistoryColumns.PRICE, priceHistory.getPrice());
        values.put(MarketHistoryTable.MarketHistoryColumns.QUANTITY, priceHistory.getQuantity());
        values.put(MarketHistoryTable.MarketHistoryColumns.TOTAL, priceHistory.getTotal());
        values.put(MarketHistoryTable.MarketHistoryColumns.TIME_DONE, priceHistory.getTimeStamp());

        return values;
    }

    private ContentValues toBuySellContentValues(MarketItem.BasedMarketType basedMarketType, String marketName, double rate, double quantity,
                                                 double expectedTP, double expectedSL, String uuid, TransactionMgr.StatusType statusType) {
        ContentValues values = new ContentValues();
        Log.d(this, "toBuyContentValues() rate = " + rate + "-exTP = " + expectedTP + "-exSL = " + expectedSL);

        values.put(TransactionTable.TransactionColumns.MARKET_NAME, marketName);
        values.put(TransactionTable.TransactionColumns.PRICE_BUY, rate);
        values.put(TransactionTable.TransactionColumns.TIME_BUY, System.currentTimeMillis());
        values.put(TransactionTable.TransactionColumns.EXPECTED_TAKE_PROFIT, expectedTP);
        values.put(TransactionTable.TransactionColumns.EXPECTED_STOP_LOSS, expectedSL);
        values.put(TransactionTable.TransactionColumns.QUANTITY, quantity);
        values.put(TransactionTable.TransactionColumns.STATUS, statusType.getValue());
        values.put(TransactionTable.TransactionColumns.BASE_MARKET, basedMarketType.getValue());
        values.put(TransactionTable.TransactionColumns.BUY_UUID, uuid);

        return values;
    }

    private ContentValues toUpdateOrderContentValues(TransactionMgr.StatusType statusType, Order order) {
        ContentValues values = new ContentValues();

        Log.d(this, "toUpdateOrderContentValues() rate = " + order.getPrice());
        values.put(TransactionTable.TransactionColumns.STATUS, statusType.getValue());
        values.put(TransactionTable.TransactionColumns.QUANTITY, order.getQuantity());
        switch (statusType) {
            case BOUGHT:
                values.put(TransactionTable.TransactionColumns.TIME_BUY, order.getBoughtTime());
                if (order.getPrice() > 0) {
                    values.put(TransactionTable.TransactionColumns.PRICE_BUY, order.getPrice());
                }
                break;
            case SOLD:
                values.put(TransactionTable.TransactionColumns.TIME_SELL, order.getSoldTime());
                if (order.getPrice() > 0) {
                    values.put(TransactionTable.TransactionColumns.PRICE_SELL, order.getPriceEnd());
                }
                break;
            case PLACE_SELL:
                values.put(TransactionTable.TransactionColumns.SELL_UUID, order.getSellUuid());
                if (order.getPriceEnd() > 0) {
                    values.put(TransactionTable.TransactionColumns.PRICE_SELL, order.getPriceEnd());
                }
                break;
        }

        return values;
    }
}
