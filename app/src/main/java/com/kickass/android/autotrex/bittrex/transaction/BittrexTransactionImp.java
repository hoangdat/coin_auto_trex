package com.kickass.android.autotrex.bittrex.transaction;

import android.content.Context;

import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.transaction.TransactionImp;
import com.kickass.android.autotrex.bittrex.Utils;

/**
 * Created by dat.ph on 1/18/2018.
 */

public class BittrexTransactionImp extends TransactionImp {
    public BittrexTransactionImp(Context context) {
        super(context);
    }

    @Override
    public ExchangeOperationMgr.ExchangeType getExchangeType() {
        return ExchangeOperationMgr.ExchangeType.Bittrex;
    }

    @Override
    public MarketItem.BasedMarketType getBaseMarket(String marketName) {
        return Utils.getBaseFromMarketName(marketName);
    }
}
