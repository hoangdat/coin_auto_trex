package com.kickass.android.autotrex.abstraction.response;

import android.text.TextUtils;

/**
 * Created by dat.ph on 1/19/2018.
 */

public class Order {
    public enum OrderType {
        LIMIT_BUY("LIMIT_BUY"),
        LIMIT_SELL("LIMIT_SELL"),
        NONE(" ");

        OrderType(String value) {
            mValue = value;
        }

        private String mValue;

        public String getValue() {
            return mValue;
        }

        public static OrderType fromString(String value) {
            for (OrderType order : OrderType.values()) {
                if (TextUtils.equals(order.getValue(), value)) {
                    return order;
                }
            }
            return null;
        }
    }

    /**
     * for response request order
     * @param mBuyUuid
     * @param mMarketName
     * @param mQuantity
     * @param mQuantityRemaining
     * @param mOrderType
     * @param mPrice
     */
    public Order(String mBuyUuid, String sellUUID, String mMarketName, double mQuantity, double mQuantityRemaining,
                 OrderType mOrderType, double mPrice) {
        this.mBuyUuid = mBuyUuid;
        this.mMarketName = mMarketName;
        this.mQuantity = mQuantity;
        this.mQuantityRemaining = mQuantityRemaining;
        this.mOrderType = mOrderType;
        this.mPrice = mPrice;
        this.mSellUuid = sellUUID;
    }

    /**
     * for transfer data buy limit
     * @param basedMarketType
     * @param marketName
     * @param quantity
     * @param price
     * @param expectedTP
     * @param expectedSL
     */
    public Order(MarketItem.BasedMarketType basedMarketType, String marketName, double quantity, double price, double expectedTP, double expectedSL) {
        this.mBaseMarketType = basedMarketType;
        this.mMarketName = marketName;
        this.mQuantity = quantity;
        this.mPrice = price;
        this.mExpectedTP = expectedTP;
        this.mExpectedSL = expectedSL;
    }

    /**
     * for get order history
     * @param mBuyUuid
     * @param mMarketName
     * @param mQuantity
     * @param mPrice
     * @param mOrderType
     * @param mTimeStamp
     */
    public Order(String mBuyUuid, String mMarketName, double mQuantity, double mPrice, OrderType mOrderType, long mTimeStamp) {
        this.mBuyUuid = mBuyUuid;
        this.mMarketName = mMarketName;
        this.mQuantity = mQuantity;
        this.mPrice = mPrice;
        this.mOrderType = mOrderType;
        this.mTimeStamp = mTimeStamp;
    }

    public void setBuyUuid(String mUuid) {
        this.mBuyUuid = mUuid;
    }

    public void setBoughtTime(long mBoughtTime) {
        this.mBoughtTime = mBoughtTime;
    }

    public void setSoldTime(long mSoldTime) {
        this.mSoldTime = mSoldTime;
    }

    private String mBuyUuid;
    private String mSellUuid;
    private String mMarketName;
    private double mQuantity;
    private double mQuantityRemaining;
    private double mCommission;
    private long mBoughtTime;
    private long mSoldTime;
    private double mPrice;
    private boolean mIsOpen;
    private OrderType mOrderType;
    private MarketItem.BasedMarketType mBaseMarketType;
    private double mExpectedTP;
    private double mExpectedSL;
    private long mTimeStamp;
    private double mPriceEnd;

    public String getBuyUuid() {
        return mBuyUuid;
    }

    public String getMarketName() {
        return mMarketName;
    }

    public double getQuantity() {
        return mQuantity;
    }

    public double getQuantityRemaining() {
        return mQuantityRemaining;
    }

    public double getCommission() {
        return mCommission;
    }

    public long getBoughtTime() {
        return mBoughtTime;
    }

    public long getSoldTime() {
        return mSoldTime;
    }

    public double getPrice() {
        return mPrice;
    }

    public boolean isOpen() {
        return mIsOpen;
    }

    public void setIsOpen(boolean mIsOpen) {
        this.mIsOpen = mIsOpen;
    }

    public OrderType getOrderType() {
        return mOrderType;
    }

    public MarketItem.BasedMarketType getBaseMarketType() {
        return mBaseMarketType;
    }

    public double getExpectedTP() {
        return mExpectedTP;
    }

    public double getExpectedSL() {
        return mExpectedSL;
    }

    public void setOrderType(OrderType mOrderType) {
        this.mOrderType = mOrderType;
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }

    public String getSellUuid() {
        return mSellUuid;
    }

    public void setSellUuid(String mSellUuid) {
        this.mSellUuid = mSellUuid;
    }

    public double getPriceEnd() {
        return mPriceEnd;
    }

    public void setPriceEnd(double mPriceEnd) {
        this.mPriceEnd = mPriceEnd;
    }
}
