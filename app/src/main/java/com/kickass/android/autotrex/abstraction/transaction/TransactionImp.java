package com.kickass.android.autotrex.abstraction.transaction;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kickass.android.autotrex.abstraction.MarketsMgr;
import com.kickass.android.autotrex.abstraction.database.table.TransactionTable;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.listener.MarketHistoryObserver;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.obsevable.Observable;
import com.kickass.android.autotrex.provider.ExchangeProvider;
import com.kickass.android.autotrex.service.filter.data.TransactionDataFilter;
import com.kickass.android.autotrex.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dat.ph on 1/18/2018.
 */

public abstract class TransactionImp {
    private Context mContext;

    private static final int INDEX_TAKE_PROFIT = 0;
    private static final int INDEX_STOP_LOSS = 1;
    private static final long PERIOD_PROCESS_BUY_VALID = 1000 * 60 * 90;

    public TransactionImp(Context context) {
        mContext = context;
    }

    private static final double AVAILABLE_BUDGET_RATIO = 0.9975;

    private HashMap<MarketItem.BasedMarketType, Double> mMapMaxBudget = new HashMap<>();
    private HashMap<MarketItem.BasedMarketType, Boolean> mMapUsing = new HashMap<>();
    private double[] mPercentTPSL = new double[2];

    public void setUsingBudget(MarketItem.BasedMarketType basedMarketType, boolean using) {
        synchronized (mMapUsing) {
            mMapUsing.put(basedMarketType, using);
        }
    }

    public boolean isBudgetUsing(MarketItem.BasedMarketType basedMarketType) {
        boolean ret;
        synchronized (mMapUsing) {
            Boolean objBudget = mMapUsing.get(basedMarketType);
            ret = (objBudget != null) ? objBudget.booleanValue() : false;
        }
        return ret;
    }

    public void setBudget(MarketItem.BasedMarketType basedMarketType, double budget) {
        synchronized (mMapMaxBudget) {
            mMapMaxBudget.put(basedMarketType, budget);
        }
    }

    public double getBudget(MarketItem.BasedMarketType basedMarketType) {
        double ret;
        synchronized (mMapMaxBudget) {
            Double objBudget = mMapMaxBudget.get(basedMarketType);
            ret = (objBudget != null) ? objBudget.doubleValue() : 0;
        }
        return ret;
    }

    public double[] getPercentTPSL() {
        synchronized (mPercentTPSL) {
            return mPercentTPSL;
        }
    }

    public void setPercentTPSL(double takeProfit, double stopLoss) {
        synchronized (mPercentTPSL) {
            mPercentTPSL[INDEX_TAKE_PROFIT] = takeProfit;
            mPercentTPSL[INDEX_STOP_LOSS] = stopLoss;
        }
    }

    private boolean hasSameTransactionBefore(String marketName) {
        Log.d(this, "hasSameTransactionBefore() " + marketName);
        boolean ret = false;
        Uri listUri = ExchangeProvider.UriString.getTransactionListUri(getExchangeType(), marketName);
        String where = TransactionTable.TransactionColumns.STATUS + "=?";
        String[] args = new String[] {marketName, TransactionMgr.StatusType.BOUGHT.getValue()};
        try (Cursor cursor = mContext.getContentResolver().query(listUri, null, where, args, null)) {
            if (cursor != null && cursor.moveToFirst()) {
                ret = true;
            }
        }
        Log.d(this, "hasSameTransactionBefore() return " + ret);
        return ret;
    }

    public String doSellLimit(TransactionDataFilter dataFilter) {
        Log.d(this, "doSellLimit() ");
        String uuid = null;
        Order order = null;
        ExchangeOperationMgr.ExchangeType exchangeType = null;
        String marketName = null;
        if (dataFilter != null) {
            order = dataFilter.getOrderInfo();
            marketName = order.getMarketName();

            Log.d(this, "doSellLimit() buy uuid = " + order.getBuyUuid());

            exchangeType = dataFilter.getExchangeType();
            if (order != null) {
                if (isValidSell(exchangeType, order)) {
                    uuid = ExchangeOperationMgr.getInstance(mContext).sellLimit(exchangeType, order);
                }
            }
        }

        Log.d(this, "doSellLimit() sell uuid = " + uuid);
        if (uuid != null) {
            Log.d(this, "doSellLimit() sell uuid = " + uuid + " done place sell " + marketName);
//            order.setSellUuid(uuid);
//            TransactionDataFilter data = new TransactionDataFilter(exchangeType, order, ExchangeOperationMgr.ActionType.Monitor_to_Sold);
//            Observable.getInstance().notifyObservers(Observable.ObservableType.TRANSACTION, data);
        } else {
            TransactionMgr.getInstance(mContext).removeFromProcessingCache(marketName);
        }

        return uuid;
    }

    public String doBuyLimit(MarketHistoryObserver.MarketHistoryDataFilter dataFilter) {
        Log.d(this, "doBuyLimit() ");
        String uuid = null;
        ExchangeOperationMgr.ExchangeType exchangeType = dataFilter.getExchangeType();
        String marketName = dataFilter.getMarketName();
        MarketItem.BasedMarketType basedMarketType = getBaseMarket(marketName);
        Order order = null;

        if (isValidPreBuy(exchangeType, basedMarketType, marketName)) {
            Ticker ticker = ExchangeOperationMgr.getInstance(mContext).getTicker(exchangeType, marketName);
            if (ticker != null) {
                double lastPrice = ticker.getLast();
                double remainBudget = getRemainBudget(basedMarketType, marketName);
                double quantity = (remainBudget * AVAILABLE_BUDGET_RATIO) / lastPrice;
                Log.d(this, "doBuyLimit() " + marketName + ": " + lastPrice + "-q: " + quantity + "-remain: " + remainBudget);
                if (isValidBuy(exchangeType, marketName, quantity)) {
                    Log.d(this, "doBuyLimit() " + marketName + ": " + lastPrice + "-q: " + quantity + "-remain: " + remainBudget + "-doBuy");
                    double[] arrTPSL = getPercentTPSL();
                    order = new Order(basedMarketType, marketName, quantity, lastPrice,
                            lastPrice * (1 + arrTPSL[INDEX_TAKE_PROFIT]), lastPrice * (1 + arrTPSL[INDEX_STOP_LOSS]));
                    uuid = ExchangeOperationMgr.getInstance(mContext).buyLimit(exchangeType, order);
                }
            }
        }

        if (uuid == null && order != null) {
            Log.d(this, "doBuyLimit() find uuid cause of null");
            uuid = findUUID(exchangeType, order);
            if (uuid == null) {
                Log.d(this, "doBuyLimit() find uuid still null, update fb");
                updateBuyFailReturnFB(order.getMarketName(), order.getPrice(), order.getQuantity());
            }
        }

        Log.d(this, "doBuyLimit() uuid = " + uuid);
        setUsingBudget(basedMarketType, false);
        if (uuid != null) {
            Log.d(this, "doBuyLimit() uuid = " + uuid + " done place buy " + marketName);
            order.setBuyUuid(uuid);
            TransactionDataFilter data = new TransactionDataFilter(exchangeType, order, ExchangeOperationMgr.ActionType.Monitor_to_Sell);
            Observable.getInstance().notifyObservers(Observable.ObservableType.TRANSACTION, data);
        } else {
            TransactionMgr.getInstance(mContext).removeFromProcessingCache(marketName);
        }

        return uuid;
    }

    private boolean isValidSell(ExchangeOperationMgr.ExchangeType exchangeType, Order order) {
        Log.d(this, "isValidSell()");
        if (order == null) return false;
        String marketName = order.getMarketName();
        double quantity = order.getQuantity();
        double price = order.getPriceEnd();
        Log.d(this, "isValidSell() exchange: " + exchangeType + "-name = " + marketName + "-q: " + quantity + "-p: " + price);
        if (exchangeType == null || marketName == null || quantity <= 0 ||price <= 0) {
            return false;
        }
        return true;
    }

    private boolean isValidPreBuy(ExchangeOperationMgr.ExchangeType exchangeType, MarketItem.BasedMarketType basedMarketType, String marketName) {
        Log.d(this, "isValidPreBuy() ex " + exchangeType + "-" + marketName);
        boolean preCondition = exchangeType != null && exchangeType != ExchangeOperationMgr.ExchangeType.None
                    && basedMarketType != null && basedMarketType != MarketItem.BasedMarketType.NONE
                    && marketName != null && !hasSameTransactionBefore(marketName);

        if (!preCondition) {
            Log.d(this, "isValidPreBuy() ex " + exchangeType + "-" + marketName + "-preCon false");
            return false;
        }

//
//        double maxBudget = getBudget(basedMarketType);
//        if (maxBudget <= 0) {
//            return false;
//        }


        //deo can dieu kien nay lam
//        ArrayList<Order> orders = ExchangeOperationMgr.getInstance(mContext).getMarketOrderHistory(exchangeType, marketName);
//        try {
//            if (orders != null && !orders.isEmpty()) {
//                for (Order order : orders) {
//                    long timeOrder = order.getTimeStamp();
//                    if ((Utils.convertLocalTimeToGMT(System.currentTimeMillis()) - timeOrder) <= PERIOD_PROCESS_BUY_VALID) {
//                        return false;
//                    }
//                }
//            }
//        } catch (ParseException e) {
//            Log.d(this, "isValidPreBuy() Exception: " + e.getMessage());
//        }

        return true;
    }

    private boolean isValidBuy(ExchangeOperationMgr.ExchangeType exchangeType, String marketName,
                               double quantity) {
        Log.d(this, "isValidBuy() " + marketName + "-q: " + quantity);
        boolean ret = false;
        if (marketName == null || quantity <= 0) {
            return ret;
        }

        MarketItem item = MarketsMgr.getInstance().getMarkets(exchangeType).get(marketName);
        if (item != null) {
            double minSize = item.getMinTradeSize();
            Log.d(this, "isValidBuy() " + minSize);
            if (quantity > minSize) {
                ret = true;
            }
        }
        Log.d(this, "isValidBuy() ret: " + ret);
        return ret;
    }

    public abstract ExchangeOperationMgr.ExchangeType getExchangeType();
    public abstract MarketItem.BasedMarketType getBaseMarket(String marketName);

    private double getRemainBudget(MarketItem.BasedMarketType basedMarketType, String marketName) {
        Uri uri = ExchangeProvider.UriString.getTransactionUri(getExchangeType());
        String where = TransactionTable.TransactionColumns.STATUS + " !=? AND " +
                  TransactionTable.TransactionColumns.BASE_MARKET + " =?";
        String[] projection = new String[] {"TOTAL(" + TransactionTable.TransactionColumns.PRICE_BUY +
                "*" + TransactionTable.TransactionColumns.QUANTITY +
                ") as " + TransactionTable.TransactionColumns.AS_USED_BUDGET};
        String[] args = new String[] {TransactionMgr.StatusType.SOLD.getValue(), basedMarketType.getValue()};
        Log.d(this, "getRemainBudget() " + marketName + "-" + uri);
        double maxBudget = getBudget(getBaseMarket(marketName));
        double usedBudget = maxBudget;
        try (Cursor cursor = mContext.getContentResolver().query(uri, projection, where, args, null)) {
            if (cursor != null && cursor.moveToFirst()) {
                usedBudget = cursor.getDouble(cursor.getColumnIndex(TransactionTable.TransactionColumns.AS_USED_BUDGET));
                Log.d(this, "getRemainBudget() used = " + usedBudget);
            } else {
                Log.d(this, "getRemainBudget() cursor = null");
            }
        }

        Log.d(this, "getRemainBudget() = " + (maxBudget - usedBudget));
        return (maxBudget - usedBudget);
    }

    private String findUUID(ExchangeOperationMgr.ExchangeType exchangeType, Order order) {
        Log.d(this, "findUUIDinOpenOrder()");
        if (order == null) {
            return null;
        }
        String uuid = null;

        String marketName = order.getMarketName();
        double rate = order.getPrice();
        double quantity = order.getQuantity();

        if (rate > 0 && quantity > 0) {
            ArrayList<Order> orders = ExchangeOperationMgr.getInstance(mContext).getOpenOrders(exchangeType);
            uuid = getUUIDExisted(orders, rate, quantity);

            if (uuid == null) {
                orders.clear();
                orders = ExchangeOperationMgr.getInstance(mContext).getMarketOrderHistory(exchangeType, marketName);
                uuid = getUUIDExisted(orders, rate, quantity);
            }
        }

        return uuid;
    }

    private String getUUIDExisted(ArrayList<Order> orders, double rate, double quantity) {
        String uuid = null;
        if (orders != null && !orders.isEmpty()) {
            for (Order historyOrder : orders) {
                double historyQuantity = historyOrder.getQuantity();
                double historyPrice = historyOrder.getPrice();

                if (rate == historyPrice && quantity == historyQuantity) {
                    uuid = historyOrder.getBuyUuid();
                    if (uuid != null) {
                        break;
                    }
                }
            }
        }
        return uuid;
    }

    private void updateBuyFailReturnFB(String marketName, double rate, double quantity) {
        Log.d(this, "updateBuyFailReturnFB(): " + marketName + "-pr = " + rate + "-qu = " + quantity);
        DatabaseReference dataRef = FirebaseDatabase.getInstance().getReference("buy_failed");
        DatabaseReference child = dataRef.push();
        child.child("marketName").setValue(marketName);
        child.child("price").setValue(rate);
        child.child("quantity").setValue(quantity);
        child.child("time").setValue(Utils.convertMillisecondToDateTime(System.currentTimeMillis()));
    }
}
