package com.kickass.android.autotrex.service.worker;

import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

import com.kickass.android.autotrex.abstraction.MarketsMgr;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.service.ExchangeService;
import com.kickass.android.autotrex.util.JUnitHandler;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 1/8/2018.
 */

public class WorkerUpdateMarketHistory implements Runnable {
    private static final int INTERNAL_HISTORY_UNIT = 45 * 1000 * 60;
    private static final int POOL_MARKET_HISTORY_NUMBER = 4;
    private static final String MARKET_HISTORY_THREAD_NAME = "market_history_thread_";

    private static WorkerUpdateMarketHistory sInstance;
    public static WorkerUpdateMarketHistory getInstance(ExchangeService service) {
        if (sInstance == null) {
            sInstance = new WorkerUpdateMarketHistory(service);
        }
        return sInstance;
    }

    private HandlerThread[] mMarketHistoryThread;
    private MarketHistoryHandler[] mMarketHistoryHandler;

    private WeakReference<ExchangeService> mRef;
    private WorkerUpdateMarketHistory(ExchangeService service) {
        mRef = new WeakReference<>(service);

        mMarketHistoryThread = new HandlerThread[POOL_MARKET_HISTORY_NUMBER];
        mMarketHistoryHandler = new MarketHistoryHandler[POOL_MARKET_HISTORY_NUMBER];

        for (int i = 0; i < POOL_MARKET_HISTORY_NUMBER; i++) {
            mMarketHistoryThread[i] = new HandlerThread(MARKET_HISTORY_THREAD_NAME + i, Process.THREAD_PRIORITY_BACKGROUND);
            mMarketHistoryThread[i].start();

            Looper looper = mMarketHistoryThread[i].getLooper();
            if (looper != null) {
                mMarketHistoryHandler[i] = new MarketHistoryHandler(MARKET_HISTORY_THREAD_NAME + i, service, looper);
            }
        }
    }

    @Override
    public void run() {
        Log.d(this, "run=========================================================================");
        ExchangeService service = mRef.get();
        if (service != null) {
            HashMap<String, MarketItem> mapMarketHot = MarketsMgr.getInstance().getHotMarkets(ExchangeOperationMgr.ExchangeType.Bittrex);
            Collection<MarketItem> values = mapMarketHot.values();
            ArrayList<MarketItem> lstMarketItem = new ArrayList<>(values);

            int szHotMarket = lstMarketItem.size();
            int quarter = (int) Math.ceil(szHotMarket / 4);
            Log.d(this, "run() size-quarter = " + szHotMarket  + "-" + quarter);

            int start = 0;

            for (int i = 0; i < POOL_MARKET_HISTORY_NUMBER; i++) {
                int end = quarter * (i + 1);
                end = (end >= szHotMarket || (i + 1 == POOL_MARKET_HISTORY_NUMBER && end < szHotMarket)) ? szHotMarket : end;

//                Log.d(this, "run() name = " + MARKET_HISTORY_THREAD_NAME + i + "---at s-e = " + start + "-" + end + "-f: " + lstMarketItem.get(start).getMarketName() + "-t: " + lstMarketItem.get(end - 1).getMarketName());
                mMarketHistoryHandler[i].sendMessageAtFrontOfQueue(mMarketHistoryHandler[i].obtainMessage(0, lstMarketItem.subList(start, end)));

                start = end;
            }
        }
    }

    private final static class MarketHistoryHandler extends JUnitHandler {
        private WeakReference<ExchangeService> mRef;
        private String mName;

        public MarketHistoryHandler(String name, ExchangeService service, Looper looper) {
            super(looper);
            mRef = new WeakReference<>(service);
            mName = name;
        }

        @Override
        public void handleMessage(Message msg) {
            List<MarketItem> lstItem = (List<MarketItem>) msg.obj;
            ExchangeService service = mRef.get();
            if (service != null && lstItem != null) {
                for (MarketItem item : lstItem) {
//                    Log.d(this, "handleMessage() thread = " + mName + " getMarketHistory: " + item.getMarketName());
                    ExchangeOperationMgr.getInstance(service).updateMarketHistory(ExchangeOperationMgr.ExchangeType.Bittrex,
                            item.getMarketName(), INTERNAL_HISTORY_UNIT);
                }
            }
        }


    }
}
