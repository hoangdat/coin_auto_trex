package com.kickass.android.autotrex.abstraction.database.table;

/**
 * Created by dat.ph on 1/31/2018.
 */

public class BuySellPointTable extends AbsExchangeTable {
    public static final String TABLE_NAME = "buy_sell_point";

    public static class BuySellPointColumns extends BaseColumns {
        public static final String PRICE_LAST = "price_last";
        public static final String TYPE = "type";
        public static final String EXPECTED_TP = "expectedTP";
        public static final String EXPECTED_SL = "expectedSL";
        public static final String TIME_BUY = "timeBuy";
        public static final String PRICE_BUY = "priceBuy";
        public static final String CURRENT_TIME = "currentTime";
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public void initTable() {
        mColumns.add(new ColumnFormat(BuySellPointColumns.ID, "INTEGER PRIMARY KEY AUTOINCREMENT,"));
        mColumns.add(new ColumnFormat(BuySellPointColumns.PRICE_LAST, "REAL,"));
        mColumns.add(new ColumnFormat(BuySellPointColumns.TYPE, "TEXT,"));
        mColumns.add(new ColumnFormat(BuySellPointColumns.EXPECTED_TP, "REAL,"));
        mColumns.add(new ColumnFormat(BuySellPointColumns.EXPECTED_SL, "REAL,"));
        mColumns.add(new ColumnFormat(BuySellPointColumns.TIME_BUY, "TEXT,"));
        mColumns.add(new ColumnFormat(BuySellPointColumns.PRICE_BUY, "REAL,"));
        mColumns.add(new ColumnFormat(BuySellPointColumns.CURRENT_TIME, "TEXT"));
    }
}