package com.kickass.android.autotrex.listener;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;

import com.kickass.android.autotrex.log.Log;

/**
 * Created by dat.ph on 1/8/2018.
 */

public abstract class AbsContentObserver extends AbsListenerImp {
    protected Context mContext;
    protected ContentObserver mObserver;

    public AbsContentObserver(Context context, ListenerMgr.LifeCycle registerTime, ListenerMgr.LifeCycle unregisterTime) {
        super(registerTime, unregisterTime);
        mContext = context;
    }

    abstract ContentObserver getContentObserver();
    abstract Uri getUri();

    @Override
    public void registerListener() {
        try {
            mContext.getContentResolver().registerContentObserver(getUri(), true, getContentObserver());
        } catch (NullPointerException e) {
            Log.e(this, "registerListener() e: " + e.getMessage());
        }
    }

    @Override
    public void unregisterListener() {
        try {
            mContext.getContentResolver().unregisterContentObserver(mObserver);
        } catch (Exception e) {
            Log.e(this, "unregisterListener(): " + e.getMessage());
        }
    }
}
