package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.Balance;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by dat.ph on 12/8/2017.
 */

public class GetBalancesRequest extends AbsBittrexRequest<ArrayList<Balance>> {

    public GetBalancesRequest(String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
    }

    public static GetBalancesRequest createInstance(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl();
        return new GetBalancesRequest(url, true, null, listener, errorListener);
    }

    private static String makeUrl() {
        return addParameter(
                BittrexConstant.ENDPOINT_V1_1 + BittrexConstant.ACCOUNT_API +
                Constant.SPLASH + BittrexConstant.GET_BALANCES,
                true
        );
    }

    @Override
    protected ArrayList<Balance> parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        return null;
    }
}
