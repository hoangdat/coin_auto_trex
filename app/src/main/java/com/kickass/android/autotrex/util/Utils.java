package com.kickass.android.autotrex.util;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.DateFormat;

import com.kickass.android.autotrex.log.Log;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class Utils{
    public static String signUrl(@NonNull String url, String seccretKey) {
        String digest = null;

        try {
            final SecretKeySpec key = new SecretKeySpec((seccretKey).getBytes("UTF-8"), "HmacSHA512");
            final Mac mac = Mac.getInstance("HmacSHA512");

            mac.init(key);

            byte[] bytes = mac.doFinal(url.getBytes("UTF-8"));
            final StringBuilder hash = new StringBuilder();

            for (byte aByte : bytes) {
                String hex = Integer.toHexString(0xFF & aByte);

                if (hex.length() == 1) {
                    hash.append('0');
                }

                hash.append(hex);
            }

            digest = hash.toString();
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException e) {
            // I should throw...
        }

        return digest;
    }

    public static void sendLocalBroadcastIntent(Context context, String action, String tag) {
        Intent intent = new Intent(action);
        if (!TextUtils.isEmpty(tag)) {
            intent.putExtra(Constant.EXTRA_TAG, tag);
        }

        if (context != null) {
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }

    public static boolean isNullOrWhiteSpace(@Nullable String value) {
        if (value == null) {
            return true;
        }

        for (int i = 0; i < value.length(); i++) {
            if (!Character.isWhitespace(value.charAt(i))) {
                return false;
            }
        }

        return true;
    }

    /**
     *
     * @param modifiedTime yyyy-MM-ddThh:mm:ss" ex) 2017-01-01T09:05:43
     * @return
     */
    public static long convertLastModifiedTime(String modifiedTime) {
        long ret = 0;
        if (modifiedTime == null || modifiedTime.isEmpty()) {
            return ret;
        }

        int index_T = modifiedTime.indexOf("T");
        String date = modifiedTime.substring(0, index_T);
        String time = modifiedTime.substring(index_T + 1);

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
            Date convertDate = formatter.parse(date + " " + time);
            ret = convertDate.getTime();
        } catch (ParseException e) {
            Log.e(Utils.class.getSimpleName(), "convertLastModifiedTime() - modifiedTime : " + modifiedTime);
            e.printStackTrace();
        }

        return ret;
    }

    public static String convertMillisecondToDateTime(long dateTime) {
        String dateString = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(dateTime)).toString();
        return dateString;
    }

    public static String convertMillisecondToTime(long dateTime) {
        String timeString = new SimpleDateFormat("hh:mm:ss").format(new Date(dateTime)).toString();
        return timeString;
    }

    public static long convertLocalTimeToGMT(long localDateTime) throws ParseException{
        Date localDate;
        SimpleDateFormat formatter;

        localDate = new Date(localDateTime);
//        Log.d(Utils.class.getSimpleName(), "convertLocalTimeToGMT() date = " + localDate + "-cu = " + convertMillisecondToDateTime(localDateTime));

        formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
        String gmt  = formatter.format(localDate);

        return convertLastModifiedTime(gmt);
    }

    public static String getCurrentTime() {
        long now = System.currentTimeMillis();
        return convertMillisecondToDateTime(now);
    }

    public static boolean isNetworkOn(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null) {
                Log.d("isNetworkOn", "netInfo.getType() = " + netInfo.getType());
                Log.d("isNetworkOn", "netInfo.getSubtype() = " + netInfo.getSubtype());
            } else {
                Log.d("isNetworkOn", "netInfo == null");
            }

            if (netInfo != null
                    && NetworkInfo.DetailedState.CONNECTED.equals(netInfo.getDetailedState())
                    && (netInfo.getType() == ConnectivityManager.TYPE_WIFI || netInfo.getType() == ConnectivityManager.TYPE_BLUETOOTH
                    || (netInfo.getType() == ConnectivityManager.TYPE_ETHERNET)
                    || (netInfo.getType() == ConnectivityManager.TYPE_MOBILE && (netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_UMTS
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_LTE
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_HSPAP
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_HSDPA
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_HSUPA
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_HSPA
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_CDMA
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_1xRTT
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_0
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_A
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_B
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EHRPD
                    || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS || netInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_EDGE)) // Add
                    // 2G
                    // type;
            )) {
                Log.w("isNetworkOn", " if (netInfo != null) netInfo.getType() = " + netInfo.getType());
                return true;
            }
        }
        return false;
    }

    /*public static boolean isWifiOn(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            Network networks[] = connectivityManager.getAllNetworks();
            if (networks != null && networks.length > 0) {
                for (Network network : networks) {
                    if (network != null) {
                        NetworkInfo netInfo = connectivityManager.getNetworkInfo(network);
                        if (netInfo != null && isWifiNetwork(netInfo) && NetworkInfo.DetailedState.CONNECTED.equals(netInfo.getDetailedState())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }*/

    private static boolean isWifiNetwork(NetworkInfo netInfo) {
        int type = netInfo.getType();
        boolean ret = false;
        if (type == ConnectivityManager.TYPE_WIFI
                || type == ConnectivityManager.TYPE_BLUETOOTH
                || type == ConnectivityManager.TYPE_ETHERNET) {
            ret = true;
        }
        return ret;
    }
}
