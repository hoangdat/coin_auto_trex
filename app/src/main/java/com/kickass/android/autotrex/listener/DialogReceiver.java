package com.kickass.android.autotrex.listener;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.kickass.android.autotrex.dialog.AbsDialogFragment;
import com.kickass.android.autotrex.util.Constant;

public class DialogReceiver extends AbsBroadcastReceiverImp {
    private AbsDialogFragment mDialogFragment;

    public DialogReceiver(Context context, AbsDialogFragment fragment, ListenerMgr.LifeCycle registerTime, ListenerMgr.LifeCycle unregisterTime) {
        super(context, registerTime, unregisterTime);
        mDialogFragment = fragment;
    }

    @Override
    IntentFilter getIntentFilter() {
        if (mIntentFilter == null) {
            mIntentFilter = new IntentFilter(Constant.DISMISS_DIALOG);
        }
        return mIntentFilter;
    }

    @Override
    BroadcastReceiver getBroadcastReceiver() {
        if (mReceiver == null) {
            mReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(Constant.DISMISS_DIALOG)) {
                        if (mDialogFragment != null && mDialogFragment.getDialog() != null && mDialogFragment.getDialog().isShowing()) {
                            Activity activity = mDialogFragment.getActivity();
                            String tag = intent.getStringExtra(Constant.EXTRA_TAG);
                            if (activity != null) {
                                if (TextUtils.isEmpty(tag) || mDialogFragment.getTag().equals(tag)) {
                                    mDialogFragment.dismissByBroadcast();
                                }
                            }
                        }
                    }
                }
            };
        }
        return mReceiver;
    }

    @Override
    public void registerListener() {
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    public void unregisterListener() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReceiver);
        mDialogFragment = null;
    }
}
