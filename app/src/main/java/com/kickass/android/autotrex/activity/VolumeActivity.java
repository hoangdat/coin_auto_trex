package com.kickass.android.autotrex.activity;

import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.adapter.markethistory.VolumeAdapterImp;
import com.kickass.android.autotrex.bittrex.BittrexInfo;
import com.kickass.android.autotrex.dialog.AddKeyDialog;
import com.kickass.android.autotrex.log.Log;

public class VolumeActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, AddKeyDialog.OnSavedAPIKeyListener {
    private VolumeAdapterImp mVolumeAdapter;
    private RecyclerView mRecycleVolume;
    LoaderManager mLoaderMgr;
    int mLoaderID;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume);

        init();
    }

    private void init() {
        mRecycleVolume = (RecyclerView) findViewById(R.id.listVolume);
        mVolumeAdapter = new VolumeAdapterImp(getApplicationContext(), R.layout.volume_row_item);

        mLayoutManager = new LinearLayoutManager(this);
        mRecycleVolume.setLayoutManager(mLayoutManager);
        mRecycleVolume.setAdapter(mVolumeAdapter);

        mLoaderMgr = getSupportLoaderManager();
        mLoaderID = this.hashCode();

        try {
            mLoaderMgr.initLoader(mLoaderID, null, this);
        } catch (IllegalStateException e) {
            Log.e(this, "IllegalStateException:" + e.toString());
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//        Log.d(this, "onCreateLoader");
        if (mVolumeAdapter == null) {
            return null;
        }

        return new CursorLoader(getApplicationContext(), mVolumeAdapter.getUri(), mVolumeAdapter.getProjection(), null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//        Log.d(this, "onLoadFinished");
        if (mVolumeAdapter != null) {
            mVolumeAdapter.changeCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        try {
            mVolumeAdapter.changeCursor(null);
        } catch (NullPointerException e) {
            Log.e(this, "NullPointerException:" + e.toString());
        }
    }

    @Override
    protected void onResume() {
        BittrexInfo info = BittrexInfo.getInstance(getApplicationContext());
        if (!info.isHasKey() && !BittrexInfo.isAddingKey()) {
            info.addKey(this, this);
        } else {
            info.reloadKey();
        }
        super.onResume();
    }

    @Override
    public void onSavedSuccess(String api, String secret) {
        Log.d(this, "onSavedSuccess: api = " + api + "-s = " + secret);
        BittrexInfo.setKey(api, secret);
    }
}
