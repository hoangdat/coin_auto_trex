package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.Balance;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dat.ph on 12/8/2017.
 */

public class GetBalanceRequest extends AbsBittrexRequest<Balance> {
    public GetBalanceRequest(String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
    }

    public static GetBalanceRequest createInstance(String currency, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl(currency);
        return new GetBalanceRequest(url, true, null, listener, errorListener);
    }

    private static String makeUrl(String currency) {
        return addParameter(
                BittrexConstant.ENDPOINT_V1_1 + BittrexConstant.ACCOUNT_API +
                Constant.SPLASH + BittrexConstant.GET_BALANCE +
                Constant.QUESTION_MARK + BittrexConstant.CURRENCY +
                Constant.EQUAL_SIGN + currency,
                true
        );
    }

    @Override
    protected Balance parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        Balance balance = null;
        if (response != null) {
            try {
                JSONObject result = response.getJSONObject(BittrexConstant.RESPONSE_RESULT);
                String currency = result.getString(BittrexConstant.RESPONSE_CURRENCY);
                double dBalance = result.getDouble(BittrexConstant.RESPONSE_BALANCE);
                double dAvailable = result.getDouble(BittrexConstant.RESPONSE_AVAILABLE);
                double dPending = result.optDouble(BittrexConstant.RESPONSE_PENDING);
                String address = result.getString(BittrexConstant.RESPONSE_ADDRESS_CRYPTO);
                balance = new Balance(currency, dBalance, dAvailable, dPending, address);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(this, "parse: JSONException " + e.getMessage());
            }
        }
        return balance;
    }
}
