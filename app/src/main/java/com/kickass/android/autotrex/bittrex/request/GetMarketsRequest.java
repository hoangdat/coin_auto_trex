package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class GetMarketsRequest extends AbsBittrexRequest<HashMap<String, MarketItem>> {
    public GetMarketsRequest(String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
    }

    public static GetMarketsRequest getInstance(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl();
        return new GetMarketsRequest(url, false, null, listener, errorListener);
    }

    public static String makeUrl() {
        return addParameter(BittrexConstant.ENDPOINT + Constant.SPLASH +
               BittrexConstant.VER_1_1 + Constant.SPLASH +
               BittrexConstant.PUBLIC_API + Constant.SPLASH +
               BittrexConstant.GET_MARKETS, false);
    }

    @Override
    protected HashMap<String, MarketItem> parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        HashMap<String, MarketItem> lstMarket = new HashMap<>();
        if (response != null) {
            try {
                JSONArray mktArray = response.getJSONArray(BittrexConstant.RESPONSE_RESULT);
                for (int i = 0; i < mktArray.length(); i++) {
                    JSONObject mktItem = mktArray.getJSONObject(i);
                    String marketName = mktItem.getString(BittrexConstant.RESPONSE_MARKET_NAME);
                    String cur = mktItem.getString(BittrexConstant.RESPONSE_MARKET_CUR);
                    String baseCur = mktItem.getString(BittrexConstant.RESPONSE_BASE_CUR);
                    String curLong = mktItem.getString(BittrexConstant.RESPONSE_MARKET_CUR_LONG);
                    double minTradeSize = mktItem.getDouble(BittrexConstant.RESPONSE_MIN_TRADE_SIZE);
                    MarketItem marketItem = new MarketItem(marketName, cur, curLong, baseCur, minTradeSize);
                    lstMarket.put(marketName, marketItem);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(this, "parse Exception: " + e.getMessage());
            }
        }
        return lstMarket;
    }
}
