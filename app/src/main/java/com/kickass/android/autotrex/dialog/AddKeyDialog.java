package com.kickass.android.autotrex.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.util.PreferenceUtils;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class AddKeyDialog extends AbsDialogFragment{
    private static final String EXCHANGE_TYPE = "exchange_type";
    private OnSavedAPIKeyListener mListener;

    public static AddKeyDialog createInstance(String exchangeType, OnSavedAPIKeyListener listener) {
        AddKeyDialog dialog = new AddKeyDialog();
        Bundle args = new Bundle();
        args.putString(EXCHANGE_TYPE, exchangeType);
        dialog.setArguments(args);
        dialog.setOnSavedKeyListener(listener);
        return dialog;
    }

    private void setOnSavedKeyListener(OnSavedAPIKeyListener listener) {
        mListener = listener;
    }

    public interface OnSavedAPIKeyListener {
        void onSavedSuccess(String api, String secret);
    }

    @Override
    protected Dialog _createDialog() {
        Bundle bundle = getArguments();
        String exchangeType = bundle.getString(EXCHANGE_TYPE);
        String title = (exchangeType != null) ? String.format("Add %s API key", exchangeType) : "Add API Key";

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        LayoutInflater factory = LayoutInflater.from(getActivity());
        View customDialog = factory.inflate(R.layout.add_key_dialog, null);

        final EditText editAPI = customDialog.findViewById(R.id.api_input);
        final EditText editSecret = customDialog.findViewById(R.id.secret_input);

        builder.setView(customDialog);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                String _api = editAPI.getText().toString().trim();
//                String _secret = editSecret.getText().toString().trim();
//                String _api = "666c43de4e0244c6b090040beacf64b2========";
//                String _secret = "9e707a83bfea41b99e85aff8c49163b2========";
                String _api = "a9737d74d4fe49968f5e1532368b9f7d";
                String _secret = "e9da69394f9443178d35528e9576de35";
                if (_api != null && _secret != null) {
                    saveKey(getActivity().getApplicationContext(), _api, _secret);
                    Toast.makeText(getActivity(), "Saved key!", Toast.LENGTH_SHORT).show();
                    if (mListener != null) {
                        mListener.onSavedSuccess(_api, _secret);
                    }
                    dismissAllowingStateLoss();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismissAllowingStateLoss();
            }
        });

        return builder.create();
    }

    private void saveKey(Context context, String api, String secret) {
        PreferenceUtils.setAPIKey(context, api);
        PreferenceUtils.setSecretKey(context, secret);
    }
}
