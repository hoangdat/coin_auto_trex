package com.kickass.android.autotrex.search;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kickass.android.autotrex.abstraction.ExchangeAsyncAPI;
import com.kickass.android.autotrex.abstraction.IResponseAPI;
import com.kickass.android.autotrex.abstraction.MarketsMgr;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 12/11/2017.
 */

public class MarketSuggestionProvider extends ContentProvider implements IResponseAPI.OnGetMarketListener {
    private static final String[] COLUMNS = {
            "_id",
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_TEXT_2,
            SearchManager.SUGGEST_COLUMN_INTENT_ACTION,
            SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA,
    };

    private List<MarketItem> mMarketItemList = new ArrayList<>();

    @Override
    public boolean onCreate() {
        ExchangeAsyncAPI.getInstance(getContext()).getMarkets(ExchangeOperationMgr.ExchangeType.Bittrex, this);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Log.d(this, "query() uri: " + uri + "-selection: " + selection + "-selectionArgs = " + selectionArgs[0]);
        if (mMarketItemList == null) {
            Collection<MarketItem> arr = ExchangeOperationMgr.getInstance(getContext()).getMarkets(ExchangeOperationMgr.ExchangeType.Bittrex).values();
            mMarketItemList.addAll(arr);
        }

        String constrain = selectionArgs[0].toLowerCase();
        MatrixCursor cursor = null;
        if (constrain != null && constrain.length() >= 2 && constrain.length() <= 5) {
            if (mMarketItemList != null) {
                cursor = new MatrixCursor(COLUMNS);

                Log.d(this, "query() constrain = " + constrain);
                for (int i = 0; i < mMarketItemList.size(); i++) {
                    MarketItem item = mMarketItemList.get(i);
                    if (item.getMarketCurrency().toLowerCase().contains(constrain) || item.getMarketCurLong().toLowerCase().contains(constrain)) {
                        StringBuilder builder =  new StringBuilder(item.getMarketName());
                        builder.append("^").append(item.getMarketCurrency());
                        builder.append("^").append(item.getMarketCurLong());
                        builder.append("^").append(item.getBasedMarket().getValue());
                        cursor.addRow(new Object[]{i, item.getMarketName(), item.getMarketCurLong(), Constant.SUGGESTION_INTENT_ACTION, builder.toString()});
                    }
                }
            }
        }

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public void onGetMarketDone(ExchangeOperationMgr.ExchangeType exchangeType, HashMap<String, MarketItem> market) {
        Collection<MarketItem> arr = market.values();
        mMarketItemList.addAll(arr);
        MarketsMgr.getInstance().putMarkets(exchangeType, market);
    }
}
