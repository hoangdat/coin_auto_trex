package com.kickass.android.autotrex.bittrex.request;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.kickass.android.autotrex.abstraction.AutoTrexRequest;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.bittrex.BittrexInfo;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.NameValuePair;
import com.kickass.android.autotrex.util.Utils;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by dat.ph on 12/6/2017.
 */

public abstract class AbsBittrexRequest<T> extends JsonObjectRequest {
    private static final String API_PARAMETER = "apikey";
    private static final String NONCE_PARAMETER = "nonce";
    private RequestFuture<JSONObject> mFuture;
    protected Map<String, String> mHeader;

    public AbsBittrexRequest(String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, jsonRequest, listener, errorListener);

        if (isRequiredAuth) {
            addHeader(BittrexConstant.API_SIGN, Utils.signUrl(url, BittrexInfo.sSecretKey));
        }

        if (listener instanceof RequestFuture) {
            mFuture = (RequestFuture<JSONObject>) listener;
        }
    }

    protected static String addParameter(String url, boolean isRequiredAuth) {
        final ArrayList<NameValuePair> parameters = new ArrayList<>();

        StringBuilder urlBuilder = new StringBuilder(url);
        boolean isFirstParameterAdded = url.contains("?");

        if (isRequiredAuth) {
//            Log.d("auto_trex", "add API key: " + BittrexInfo.sKey);
            parameters.add(new NameValuePair(API_PARAMETER, BittrexInfo.sKey));
        }

        long nonce = System.currentTimeMillis();
        parameters.add(new NameValuePair(NONCE_PARAMETER, Long.toString(nonce)));

        for (NameValuePair parameter : parameters) {
            if (parameter != null && !Utils.isNullOrWhiteSpace(parameter.name())) {
                urlBuilder.append(isFirstParameterAdded ? "&" : "?");
                urlBuilder.append(parameter.name());

                if (!Utils.isNullOrWhiteSpace(parameter.value())) {
                    urlBuilder.append("=");
                    urlBuilder.append(parameter.value());
                }

                isFirstParameterAdded = true;
            }
        }

        Log.d("auto_trex", "addParameter final URL = " + urlBuilder.toString());
        return urlBuilder.toString();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (mHeader == null) {
            mHeader = new HashMap<>();
        }
        return mHeader;
    }

    protected void addHeader(String key, String value) {
        if (mHeader == null) {
            mHeader = new HashMap<>();
        }
        mHeader.put(key, value);
    }

    protected RequestFuture<JSONObject> getFuture() {
        return mFuture;
    }

    public T execute(Context context) throws Exception {
        AutoTrexRequest.getInstance(context).addToQueue(this);
        RequestFuture<JSONObject> future = getFuture();
        if (future != null) {
            JSONObject response = null;
            try {
                response = future.get(); //AuthFailed error will throw ExecutionException
            } catch (InterruptedException e) {
                Log.d(this, "execute InterruptedException: " + e.getMessage());
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d(this, "execute ExecutionException: " + e.getMessage());
                handleExecutionException(e);
            }
            return parse(response);
        }
        return null;
    }

    protected void handleExecutionException(ExecutionException e) throws IOException {
        Throwable err = e.getCause();
        e.printStackTrace();

        if (err != null && err instanceof VolleyError) {
            VolleyError vErr = (VolleyError) err;
            NetworkResponse errRes = vErr.networkResponse;
            if (errRes != null) {
                Log.e(this, "handleExecutionException status code = " + errRes.statusCode);
                String errorDetail = null;
                try {
                    errorDetail = (errRes.data != null) ? new String(errRes.data, "UTF-8") : null;
                    Log.e(this, "handleExecutionException errorDetail = " + errorDetail);
                } catch (UnsupportedEncodingException unE) {
                    unE.printStackTrace();
                }
                throw new IOException("errRes.statusCode - " + "errorDetail");
            }
        }
    }

    protected abstract T parse(JSONObject response);
}
