package com.kickass.android.autotrex.abstraction.database.table;

/**
 * Created by dat.ph on 1/17/2018.
 */

public class MarketHistoryTable extends AbsExchangeTable {
    public static final String TABLE_NAME = "market_history";

    public MarketHistoryTable() {
        super();
    }

    public static class MarketHistoryColumns {
        public static final String ID = "_id";
        public static final String HISTORY_ID = "history_id";
        public static final String MARKET_NAME = "market_name";
        public static final String TIME_DONE = "time_done";
        public static final String QUANTITY = "quantity";
        public static final String PRICE = "price";
        public static final String TOTAL = "total";
        public static final String ORDER_TYPE = "orderType";
        public static final String AS_BUY_TOTAL = "buy_volume_total";
        public static final String AS_SELL_TOTAL = "sell_volume_total";
        public static final String AS_MIN_START_DONE = "min_start_done";
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public void initTable() {
        mColumns.add(new ColumnFormat(MarketHistoryColumns.ID, "INTEGER PRIMARY KEY AUTOINCREMENT,"));
        mColumns.add(new ColumnFormat(MarketHistoryColumns.HISTORY_ID, "TEXT,"));
        mColumns.add(new ColumnFormat(MarketHistoryColumns.MARKET_NAME, "TEXT,"));
        mColumns.add(new ColumnFormat(MarketHistoryColumns.TIME_DONE, "INTEGER,"));
        mColumns.add(new ColumnFormat(MarketHistoryColumns.QUANTITY, "REAL,"));
        mColumns.add(new ColumnFormat(MarketHistoryColumns.PRICE, "REAL,"));
        mColumns.add(new ColumnFormat(MarketHistoryColumns.TOTAL, "REAL,"));
        mColumns.add(new ColumnFormat(MarketHistoryColumns.ORDER_TYPE, "TEXT"));
    }
}
