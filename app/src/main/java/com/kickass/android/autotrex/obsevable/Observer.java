package com.kickass.android.autotrex.obsevable;

public interface Observer {
    void update(Observable o, Observable.ObservableType obType, Object arg);
}
