package com.kickass.android.autotrex.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dat.ph on 1/10/2018.
 */

public abstract class AbsAdapterImp extends RecyclerView.Adapter<AppViewHolder>{
    private Context mContext;
    private int mLayoutId;

    public AbsAdapterImp(Context context, int layoutId) {
        mContext = context;
        mLayoutId = layoutId;
    }

    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(mLayoutId, parent, false);
        return _createViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AppViewHolder holder, int position) {
        _bindView(holder, position);
    }

    @Override
    public int getItemCount() {
        return _getItemCount();
    }

    protected abstract AppViewHolder _createViewHolder(View itemView);
    protected abstract void _bindView(AppViewHolder holder, int position);
    protected abstract int _getItemCount();

    protected Context getContext() {
        return mContext;
    }
}
