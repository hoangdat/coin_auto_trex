package com.kickass.android.autotrex.service.filter.data;


import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.Order;

/**
 * Created by Dat on 1/23/2018.
 */
public class TransactionDataFilter {
    private ExchangeOperationMgr.ExchangeType mExchangeType;
    private Order mOrderInfo;
    private ExchangeOperationMgr.ActionType mActionType;

    public TransactionDataFilter(ExchangeOperationMgr.ExchangeType exchangeType, Order order, ExchangeOperationMgr.ActionType mActionType) {
        this.mExchangeType = exchangeType;
        this.mOrderInfo = order;
        this.mActionType = mActionType;
    }

    public ExchangeOperationMgr.ActionType getActionType() {
        return mActionType;
    }

    public ExchangeOperationMgr.ExchangeType getExchangeType() {
        return mExchangeType;
    }

    public Order getOrderInfo() {
        return mOrderInfo;
    }

    public void setActionType(ExchangeOperationMgr.ActionType mActionType) {
        this.mActionType = mActionType;
    }
}
