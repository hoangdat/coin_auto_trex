package com.kickass.android.autotrex.abstraction;

import android.content.Context;

import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.Balance;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.abstraction.response.PriceHistory;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.util.CachedExecutorService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 12/7/2017.
 */

public class ExchangeAsyncAPI {
    private Context mContext;
    private static ExchangeAsyncAPI sInstance;

    private ExchangeAsyncAPI(Context context) {
        mContext = context;
    }

    public static ExchangeAsyncAPI getInstance(Context context) {
        synchronized (ExchangeAsyncAPI.class) {
            if (sInstance == null) {
                sInstance = new ExchangeAsyncAPI(context);
            }
        }
        return sInstance;
    }

    public void getMarkets(final ExchangeOperationMgr.ExchangeType exchangeType,
                           final IResponseAPI.OnGetMarketListener response) {
        CachedExecutorService.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                HashMap<String, MarketItem> markets = ExchangeOperationMgr.getInstance(mContext).getMarkets(exchangeType);
                if (response != null) {
                    response.onGetMarketDone(exchangeType, markets);
                }
            }
        });
    }

    public void getMarketSum(final ExchangeOperationMgr.ExchangeType exchangeType, final String market,
                             final IResponseAPI.OnGetSumListener response) {
        CachedExecutorService.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                MarketSummary summary = ExchangeOperationMgr.getInstance(mContext).getMarketSum(exchangeType, market);
                if (response != null) {
                    response.onGetSumDone(exchangeType, market, summary);
                }
            }
        });
    }

    public void getBalances(final ExchangeOperationMgr.ExchangeType exchangeType,
                            final IResponseAPI.OnGetBalancesListener response) {
        CachedExecutorService.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                ArrayList<Balance> balances = ExchangeOperationMgr.getInstance(mContext).getBalances(exchangeType);
                if (response != null) {
                    response.onGetBalances(exchangeType, balances);
                }
            }
        });
    }

    public void getBalance(final ExchangeOperationMgr.ExchangeType exchangeType, final String currency,
                           final IResponseAPI.OnGetBalancesListener response) {
        CachedExecutorService.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                Balance balance = ExchangeOperationMgr.getInstance(mContext).getBalance(exchangeType, currency);
                if (response != null) {
                    response.onGetBalance(exchangeType, currency, balance);
                }
            }
        });
    }

//    public void buyLimit(final ExchangeOperationMgr.ExchangeType exchangeType, final String market,
//                         final String quantity, final String rate, final IResponseAPI.OnMarketLimitListener response) {
//        CachedExecutorService.getInstance().execute(new Runnable() {
//            @Override
//            public void run() {
//                String uuid = ExchangeOperationMgr.getInstance(mContext).buyLimit(exchangeType, market, quantity, rate);
//                if (response != null) {
//                    response.onBuyDone(exchangeType, uuid);
//                }
//            }
//        });
//    }

//    public void sellLimit(final ExchangeOperationMgr.ExchangeType exchangeType, final String market,
//                          final String quantity, final String rate, final IResponseAPI.OnMarketLimitListener response) {
//        CachedExecutorService.getInstance().execute(new Runnable() {
//            @Override
//            public void run() {
//                String uuid = ExchangeOperationMgr.getInstance(mContext).sellLimit(exchangeType, market, quantity, rate);
//                if (response != null) {
//                    response.onSellDone(exchangeType, uuid);
//                }
//            }
//        });
//    }

    public void getTicker(final ExchangeOperationMgr.ExchangeType exchangeType, final String market, final IResponseAPI.OnGetTickerListener response) {
        CachedExecutorService.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                Ticker ticker = ExchangeOperationMgr.getInstance(mContext).getTicker(exchangeType, market);
                if (response != null) {
                    response.onGetTickerDone(exchangeType, market, ticker);
                }
            }
        });
    }

    public void getMarketHistory(final ExchangeOperationMgr.ExchangeType exchangeType, final String market, final IResponseAPI.OnGetMarketHistory response) {
        CachedExecutorService.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                ArrayList<PriceHistory> priceHistory = ExchangeOperationMgr.getInstance(mContext).getMarketHistory(exchangeType, market);
                if (response != null) {
                    response.onGetMarketHistoryDone(exchangeType, market, priceHistory);
                }
            }
        });
    }

    public void getSummaries(final ExchangeOperationMgr.ExchangeType exchangeType, final IResponseAPI.OnGetSumListener response) {
        CachedExecutorService.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                List<MarketSummary> summaries = ExchangeOperationMgr.getInstance(mContext).getSummaries(exchangeType);
                if (response != null) {
                    response.onGetSummariesDone(exchangeType, summaries);
                }
            }
        });
    }
}
