package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;
import com.kickass.android.autotrex.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dat.ph on 1/19/2018.
 */

public class GetOrderRequest extends AbsBittrexRequest<Order> {
    public GetOrderRequest(String url, boolean isRequiredAuth, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, null, listener, errorListener);
    }

    public static GetOrderRequest createInstance(String uuid, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl(uuid);
        return new GetOrderRequest(url, true, listener, errorListener);
    }

    private static String makeUrl(String uuid) {
        String url = BittrexConstant.ENDPOINT_V1_1 + BittrexConstant.ACCOUNT_API +
                Constant.SPLASH + BittrexConstant.GET_ORDER +
                Constant.QUESTION_MARK + BittrexConstant.RESPONSE_UUID + Constant.EQUAL_SIGN + uuid;
        return addParameter(url, true);
    }

    @Override
    protected Order parse(JSONObject response) {
        Order order = null;
        if (response != null) {
            try {
                JSONObject result = response.getJSONObject(BittrexConstant.RESPONSE_RESULT);
                String uuid = result.getString(BittrexConstant.RESPONSE_ORDER_UUID);
                String marketName = result.getString(BittrexConstant.RESPONSE_ORDER_EXCHANGE);
                String type = result.getString(BittrexConstant.RESPONSE_TYPE);
                double quantity = result.getDouble(BittrexConstant.RESPONSE_QUANTITY);
                double quantityRemaining = result.getDouble(BittrexConstant.RESPONSE_QUANTITY_REMAINING);
                long timeClose = Utils.convertLastModifiedTime(result.getString(BittrexConstant.RESPONSE_TIME_CLOSE));
                double price = result.getDouble(BittrexConstant.RESPONSE_PRICE);
                Double price_per_unit = result.getDouble(BittrexConstant.RESPONSE_PRICE_PER_UNIT);
                boolean isOpen = result.getBoolean(BittrexConstant.RESPONSE_ORDER_IS_OPEN);
                Order.OrderType orderType = Order.OrderType.fromString(type);

                Log.d(this, "parse p = " + price + "-ppu = " + price_per_unit);
                if (orderType == Order.OrderType.LIMIT_BUY) {
                    order = new Order(uuid, null, marketName, quantity, quantityRemaining, orderType, price_per_unit != null ? price_per_unit.doubleValue() : 0);
                } else {
                    order = new Order(null, uuid, marketName, quantity, quantityRemaining, orderType, price_per_unit != null ? price_per_unit.doubleValue() : 0);
                }
                order.setIsOpen(isOpen);

                if (!isOpen) {
                    if (orderType == Order.OrderType.LIMIT_BUY) {
                        order.setBoughtTime(timeClose);
                    } else {
                        order.setSoldTime(timeClose);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return order;
    }
}
