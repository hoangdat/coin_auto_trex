package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.PriceHistory;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;
import com.kickass.android.autotrex.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by dat.ph on 12/8/2017.
 */

public class GetMarketHistoryRequest extends AbsBittrexRequest<ArrayList<PriceHistory>> {
    private String mMarketName;
    public GetMarketHistoryRequest(String url, String marketName, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
        mMarketName = marketName;
    }

    private static String makeUrl(String market) {
        String url = BittrexConstant.ENDPOINT_V1_1 + BittrexConstant.PUBLIC_API + Constant.SPLASH + BittrexConstant.GET_MARKET_HISTORY +
                Constant.QUESTION_MARK + BittrexConstant.MARKET_API + Constant.EQUAL_SIGN + market;
        return addParameter(url, false);
    }

    public static GetMarketHistoryRequest createInstance(String market, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl(market);
        return new GetMarketHistoryRequest(url, market, false, null, listener, errorListener);
    }

    @Override
    protected ArrayList<PriceHistory> parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        ArrayList<PriceHistory> priceHistories = new ArrayList<>();
        if (response != null) {
            try {
                JSONArray resultArr = response.getJSONArray(BittrexConstant.RESPONSE_RESULT);
                for (int i = 0; i < resultArr.length(); i++) {
                    JSONObject priceHistory = resultArr.getJSONObject(i);
                    long id = priceHistory.getLong(BittrexConstant.RESPONSE_ID);
                    String timeStamp = priceHistory.getString(BittrexConstant.RESPONSE_TIMESTAMP);
                    double quantity = priceHistory.getDouble(BittrexConstant.RESPONSE_QUANTITY);
                    double price = priceHistory.getDouble(BittrexConstant.RESPONSE_PRICE);
                    double total = priceHistory.getDouble(BittrexConstant.RESPONSE_TOTAL);
                    String orderType = priceHistory.getString(BittrexConstant.RESPONSE_ORDER_TYPE);
                    PriceHistory tmp = new PriceHistory(mMarketName, id, Utils.convertLastModifiedTime(timeStamp), quantity, price, total, orderType);
                    if (tmp != null) {
                        priceHistories.add(tmp);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(this, "parse Exception: " + e.getMessage());
            }
        }
        return priceHistories;
    }
}
