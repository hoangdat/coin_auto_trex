package com.kickass.android.autotrex.bittrex;

import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.log.Log;

/**
 * Created by dat.ph on 1/9/2018.
 */

public class Utils {
    public static MarketItem.BasedMarketType getBaseFromMarketName(String marketName) {
        MarketItem.BasedMarketType ret = MarketItem.BasedMarketType.NONE;
        try {
            if (marketName != null && marketName.contains("-")) {
                int indexSymbol = marketName.indexOf("-");
                String base = marketName.substring(0, indexSymbol);
                ret = MarketItem.BasedMarketType.fromString(base);
            }
        } catch (Exception e) {
            Log.d(Utils.class.getSimpleName(), "exception: " + e.getMessage());
        } finally {
            return ret;
        }
    }
}
