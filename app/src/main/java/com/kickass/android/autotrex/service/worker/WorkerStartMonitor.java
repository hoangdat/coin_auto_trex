package com.kickass.android.autotrex.service.worker;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.kickass.android.autotrex.abstraction.database.table.TransactionTable;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.abstraction.transaction.TransactionMgr;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.obsevable.Observable;
import com.kickass.android.autotrex.provider.ExchangeProvider;
import com.kickass.android.autotrex.service.filter.data.TransactionDataFilter;

/**
 * Created by dat.ph on 1/25/2018.
 */

public class WorkerStartMonitor implements Runnable{
    private Context mContext;

    private static WorkerStartMonitor sInstance;
    private WorkerStartMonitor(Context context) {
        mContext = context;
    }

    public static WorkerStartMonitor getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new WorkerStartMonitor(context);
        }
        return sInstance;
    }
    @Override
    public void run() {
        Log.d(this, "run start mon *****");
        Uri uri = ExchangeProvider.UriString.getTransactionUri(ExchangeOperationMgr.ExchangeType.Bittrex);
        String where = TransactionTable.TransactionColumns.STATUS + "!=?";

        try {
            try (Cursor cursor = mContext.getContentResolver().query(uri, null, where, new String[]{TransactionMgr.StatusType.SOLD.getValue()}, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        String baseMarket = cursor.getString(cursor.getColumnIndex(TransactionTable.TransactionColumns.BASE_MARKET));
                        String name = cursor.getString(cursor.getColumnIndex(TransactionTable.TransactionColumns.MARKET_NAME));
                        double quantity = cursor.getDouble(cursor.getColumnIndex(TransactionTable.TransactionColumns.QUANTITY));
                        double price = cursor.getDouble(cursor.getColumnIndex(TransactionTable.TransactionColumns.PRICE_BUY));
                        double expectedTp = cursor.getDouble(cursor.getColumnIndex(TransactionTable.TransactionColumns.EXPECTED_TAKE_PROFIT));
                        double expectedSL = cursor.getDouble(cursor.getColumnIndex(TransactionTable.TransactionColumns.EXPECTED_STOP_LOSS));
                        String status = cursor.getString(cursor.getColumnIndex(TransactionTable.TransactionColumns.STATUS));
                        TransactionMgr.StatusType statusType = TransactionMgr.StatusType.fromString(status);
                        Log.d(this, "run start mon status " + status + "-s = " + statusType);
                       if (statusType != null) {
                           Order order = new Order(MarketItem.BasedMarketType.fromString(baseMarket), name, quantity, price, expectedTp, expectedSL);
                           TransactionDataFilter dataFilter = new TransactionDataFilter(ExchangeOperationMgr.ExchangeType.Bittrex, order, ExchangeOperationMgr.ActionType.Sell_Limit);

                           switch (statusType) {
                                case PLACE_BUY:
                                    dataFilter.setActionType(ExchangeOperationMgr.ActionType.Monitor_to_Bought);
                                    break;
                               case BOUGHT:
                                   dataFilter.setActionType(ExchangeOperationMgr.ActionType.Monitor_to_Sell);
                                   break;
                               case PLACE_SELL:
                                   dataFilter.setActionType(ExchangeOperationMgr.ActionType.Monitor_to_Sold);
                                   break;
                               default:
                                   Log.d(this, "not support this status = " + statusType);
                                   break;
                            }

                            if (statusType != TransactionMgr.StatusType.SOLD) {
                                Log.d(this, "run start mon notify");
                                Observable.getInstance().notifyObservers(Observable.ObservableType.TRANSACTION, dataFilter);
                            }
                        }
                    } while (cursor.moveToNext());
                }
            }
        } catch (Exception e) {
            Log.d(this, "run Exception = " + e.getMessage());
        }
    }
}
