package com.kickass.android.autotrex.abstraction;



import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;

import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 1/4/2018.
 */
public class MarketsMgr {
    private static MarketsMgr sInstance;

    private MarketsMgr() {
        mMapMarkets = new HashMap<>();
        mMapHotMarkets = new HashMap<>();
    }

    public synchronized static MarketsMgr getInstance() {
        if (sInstance == null) {
            sInstance = new MarketsMgr();
        }
        return sInstance;
    }

    private HashMap<ExchangeOperationMgr.ExchangeType, HashMap<String, MarketItem>> mMapMarkets;
    private HashMap<ExchangeOperationMgr.ExchangeType, HashMap<String, MarketItem>> mMapHotMarkets;

    public HashMap<String, MarketItem> getMarkets(ExchangeOperationMgr.ExchangeType exchangeType) {
        HashMap<String, MarketItem> ret = null;
        synchronized (mMapMarkets) {
            if (exchangeType != null) {
                ret = mMapMarkets.get(exchangeType);
            }
        }
        return ret;
    }

    public void putMarkets(ExchangeOperationMgr.ExchangeType exchangeType, HashMap<String, MarketItem> lstMArket) {
        synchronized (mMapMarkets) {
            mMapMarkets.put(exchangeType, lstMArket);
        }
    }

    public HashMap<String, MarketItem> getHotMarkets(ExchangeOperationMgr.ExchangeType exchangeType) {
        HashMap<String, MarketItem> ret = null;
        synchronized (mMapHotMarkets) {
            if (exchangeType != null) {
                ret = mMapHotMarkets.get(exchangeType);
            }
        }
        return ret;
    }

    public void putHotMarkets(ExchangeOperationMgr.ExchangeType exchangeType, HashMap<String, MarketItem> lstMArket) {
        synchronized (mMapHotMarkets) {
            mMapHotMarkets.put(exchangeType, lstMArket);
        }
    }

    public MarketItem getHotItem(ExchangeOperationMgr.ExchangeType exchangeType, String marketName) {
        MarketItem item = null;
        if (exchangeType != null) {
            synchronized (mMapHotMarkets) {
                HashMap<String, MarketItem> items = mMapHotMarkets.get(exchangeType);
                if (items != null) {
                    item = items.get(marketName);
                }
            }
        }
        return item;
    }
}
