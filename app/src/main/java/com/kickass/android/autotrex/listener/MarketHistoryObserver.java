package com.kickass.android.autotrex.listener;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import com.kickass.android.autotrex.abstraction.database.table.MarketHistoryTable;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.obsevable.Observable;
import com.kickass.android.autotrex.provider.ExchangeProvider;
import com.kickass.android.autotrex.util.Utils;

/**
 * Created by dat.ph on 1/8/2018.
 */

public class MarketHistoryObserver extends AbsContentObserver {
    private static final float THRESHOLD_FLOOR_MARKET_HISTORY = 2.9f;
    private static final float THRESHOLD_CEIL_MARKET_HISTORY = 4.0f;
    private ExchangeOperationMgr.ExchangeType mExchangeType;
    private String mMarketName;

    public MarketHistoryObserver(Context context, ExchangeOperationMgr.ExchangeType exchangeType,
                                 String marketName, ListenerMgr.LifeCycle registerTime, ListenerMgr.LifeCycle unregisterTime) {
        super(context, registerTime, unregisterTime);
        mMarketName = marketName;
        mExchangeType = exchangeType;
    }

    @Override
    ContentObserver getContentObserver() {
        if (mObserver == null) {
            mObserver = new ContentObserver(new Handler()) {
                @Override
                public void onChange(boolean selfChange, Uri uri) {
//                    Log.d(this, "onChange uri = " + uri);
                    if (uri.equals(getUri())) {
                        String where = MarketHistoryTable.MarketHistoryColumns.MARKET_NAME + "= ?";
                        double buyTotalQuantity = 0f;
                        double sellTotalQuantity = 0f;

                        try (Cursor cursor = mContext.getContentResolver().query(getUri(), getProjection(), where, new String[] {mMarketName}, null)) {
                            if (cursor != null && cursor.moveToFirst()) {
                                String marketName = cursor.getString(cursor.getColumnIndex(MarketHistoryTable.MarketHistoryColumns.MARKET_NAME));
                                if (mMarketName.equals(marketName)) {
                                    buyTotalQuantity = cursor.getDouble(cursor.getColumnIndex(MarketHistoryTable.MarketHistoryColumns.AS_BUY_TOTAL));
                                    sellTotalQuantity = cursor.getDouble(cursor.getColumnIndex(MarketHistoryTable.MarketHistoryColumns.AS_SELL_TOTAL));
                                }
                            }
                        }

                        MarketHistoryDataFilter data = null;

                        if (sellTotalQuantity > 0) {
                            double ratio = buyTotalQuantity / sellTotalQuantity;
                            if (ratio >= THRESHOLD_FLOOR_MARKET_HISTORY && ratio <= THRESHOLD_CEIL_MARKET_HISTORY) {
                                //should buy
//                                Log.d(this, "onChange() should buy:" + mMarketName + " - with r-t = " + ratio + "-at: " + Utils.getCurrentTime());
                                data = new MarketHistoryDataFilter(mExchangeType, mMarketName,
                                        ExchangeOperationMgr.ActionType.Buy_Limit, buyTotalQuantity, sellTotalQuantity);
                            }
//                            else if (ratio < 1 / THRESHOLD_NOTI_MARKET_HISTORY) {
//                                //should sell
////                                Log.d(this, "onChange() should sell:" + mMarketName + " - with r-t = " + ratio + "-at: " + Utils.getCurrentTime());
//                                data = new MarketHistoryDataFilter(mExchangeType, mMarketName,
//                                        ExchangeOperationMgr.ActionType.Sell_Limit, buyTotalQuantity, sellTotalQuantity);
//                            }

                            if (data != null) {
                                Observable.getInstance().notifyObservers(Observable.ObservableType.MARKET_HISTORY, data);
                            }
                        }
                    }
                }
            };
        }
        return mObserver;
    }

    @Override
    Uri getUri() {
        Uri ret = null;
//        Log.d(this, "getUri(): " + mExchangeType + "-" + mMarketName);
        if (mExchangeType != null && mMarketName  != null) {
            ret = ExchangeProvider.UriString.getMarketHistoryListVolumeUri(mExchangeType);
        }
//        Log.d(this, "getUri(): " + ret);
        return ret;
    }

    private String[] getProjection() {
        String[] projection = new String[4];
        projection[0] = MarketHistoryTable.MarketHistoryColumns.MARKET_NAME;
        projection[1] = "TOTAL(CASE " + MarketHistoryTable.MarketHistoryColumns.ORDER_TYPE +
                " WHEN 'SELL' THEN " + MarketHistoryTable.MarketHistoryColumns.QUANTITY +
                " ELSE 0 END) as " + MarketHistoryTable.MarketHistoryColumns.AS_SELL_TOTAL;
        projection[2] = "TOTAL(CASE " + MarketHistoryTable.MarketHistoryColumns.ORDER_TYPE +
                " WHEN 'BUY' THEN " + MarketHistoryTable.MarketHistoryColumns.QUANTITY +
                " ELSE 0 END) as " + MarketHistoryTable.MarketHistoryColumns.AS_BUY_TOTAL;
        projection[3] = "MIN(" + MarketHistoryTable.MarketHistoryColumns.TIME_DONE + ") as " +
                MarketHistoryTable.MarketHistoryColumns.AS_MIN_START_DONE;

        return projection;
    }

    public static class MarketHistoryDataFilter {
        private String mMarketName;
        private ExchangeOperationMgr.ExchangeType mExchangeType;
        private double mBuyQuantity;
        private double mSellQuantity;
        private ExchangeOperationMgr.ActionType mActionType;

        public MarketHistoryDataFilter(ExchangeOperationMgr.ExchangeType exchangeType, String marketName,
                                       ExchangeOperationMgr.ActionType action, double buyQuantity, double sellQuantity) {
            mExchangeType = exchangeType;
            mMarketName = marketName;
            mActionType = action;
            mBuyQuantity = buyQuantity;
            mSellQuantity = sellQuantity;
        }

        public double getBuyQuantity() {
            return mBuyQuantity;
        }

        public double getSellQuantity() {
            return mSellQuantity;
        }

        public ExchangeOperationMgr.ActionType getActionType() {
            return mActionType;
        }

        public String getMarketName() {
            return mMarketName;
        }

        public ExchangeOperationMgr.ExchangeType getExchangeType() {
            return mExchangeType;
        }
    }
}
