package com.kickass.android.autotrex.abstraction.operation;

import android.content.Context;

import com.kickass.android.autotrex.abstraction.response.Balance;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.abstraction.response.PriceHistory;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.abstraction.transaction.TransactionMgr;
import com.kickass.android.autotrex.bittrex.operation.BittrexOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 12/7/2017.
 */

public class ExchangeOperationMgr {
    public enum ExchangeType {
        Bittrex(0),
        None(1);

        private int mValue;

        ExchangeType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }

        public static ExchangeType fromInt(int i) {
            for (ExchangeType exchangeType : ExchangeType.values()) {
                if (exchangeType.getValue() == i)
                    return exchangeType;
            }
            return null;
        }
    }

    public enum ActionType {
        Buy_Limit(0),
        Sell_Limit(1),
        Monitor_to_Bought(2),
        Monitor_to_Sell(3),
        Monitor_to_Sold(4),
        Nothing_to_do(5),
        None(-1);

        private int mValue;

        ActionType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }

        public static ActionType fromInt(int i) {
            for (ActionType actionType : ActionType.values()) {
                if (actionType.getValue() == i)
                    return actionType;
            }
            return null;
        }
    }
    private static HashMap<ExchangeType, AbsOperation> sExchangeOperationList;
    private static ExchangeOperationMgr sInstance;
    private Context mContext;

    public static ExchangeOperationMgr getInstance(Context context) {
        synchronized (ExchangeOperationMgr.class) {
            if (sInstance == null) {
                sInstance = new ExchangeOperationMgr(context);
            }
        }
        return sInstance;
    }

    private ExchangeOperationMgr(Context context) {
        sExchangeOperationList = new HashMap<>();
        mContext = context;
        addExchangeOperationList(context);
    }

    private void addExchangeOperationList(Context context) {
        sExchangeOperationList.put(ExchangeType.Bittrex, new BittrexOperation(context));
    }

    public HashMap<String, MarketItem> getMarkets(ExchangeType exchangeType) {
        HashMap<String, MarketItem> markets = new HashMap<>();
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            markets = operation.getMarkets();
        }
        return markets;
    }

    public MarketSummary getMarketSum(ExchangeType exchangeType, String market) {
        MarketSummary sum = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            sum = operation.getMarketSum(market);
        }
        return sum;
    }

    public ArrayList<Balance> getBalances(ExchangeType exchangeType) {
        ArrayList<Balance> balances = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            balances = operation.getBalances();
        }
        return balances;
    }

    public Balance getBalance(ExchangeType exchangeType, String currency) {
        Balance balance = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            balance = operation.getBalance(currency);
        }
        return balance;
    }

    public String buyLimit(ExchangeType exchangeType, Order order) {
        String uuid = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            uuid = operation.buyLimit(order);
        }
        return uuid;
    }

    public String sellLimit(ExchangeType exchangeType, Order order) {
        String uuid = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            uuid = operation.sellLimit(order);
        }
        return uuid;
    }

    public Ticker getTicker(ExchangeType exchangeType, String market) {
        Ticker ticker = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            ticker = operation.getTicker(market);
        }
        return ticker;
    }

    public ArrayList<PriceHistory> getMarketHistory(ExchangeType exchangeType, String market) {
        ArrayList<PriceHistory> priceHistory = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            priceHistory = operation.getPriceHistory(market);
        }
        return priceHistory;
    }

    public void updateMarketHistory(ExchangeType exchangeType, String market, long internalTime) {
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            operation.updateMarketHistory(market, internalTime);
        }
    }

    public List<MarketSummary> getSummaries(ExchangeType exchangeType) {
        List<MarketSummary> summaries = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            summaries = operation.getSummaries();
        }
        return summaries;
    }

    public Order getOrder(ExchangeType exchangeType, String uuid) {
        Order order = null;
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            order = operation.getOrder(uuid);
        }
        return order;
    }

    public ArrayList<Order> getOpenOrders(ExchangeType exchangeType) {
        ArrayList<Order> ret = new ArrayList<>();
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            ret = operation.getOpenOrders();
        }
        return ret;
    }

    public void updateOrder(ExchangeType exchangeType, TransactionMgr.StatusType statusType, Order order) {
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            operation.updateOrderInfo(statusType, order);
        }
    }

    public ArrayList<Order> getMarketOrderHistory(ExchangeType exchangeType, String name) {
        ArrayList<Order> ordersHistory = new ArrayList<>();
        AbsOperation operation = sExchangeOperationList.get(exchangeType);
        if (operation != null) {
            ordersHistory = operation.getMarketOrderHistory(name);
        }
        return ordersHistory;
    }
}
