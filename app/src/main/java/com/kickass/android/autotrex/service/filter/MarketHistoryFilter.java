package com.kickass.android.autotrex.service.filter;

import android.content.Context;

import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.listener.ListenerMgr;
import com.kickass.android.autotrex.listener.MarketHistoryObserver;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by dat.ph on 1/8/2018.
 */

public class MarketHistoryFilter {
    private ListenerMgr mListenerMgr;
    private Context mContext;

    public MarketHistoryFilter(Context context) {
        mListenerMgr = new ListenerMgr();
        mContext = context;
    }

    public void startFilter(ExchangeOperationMgr.ExchangeType exchangeType, HashMap<String, MarketItem> hotItems) {
        if (mListenerMgr != null) {
            mListenerMgr.notifyDestroy();

            Set<String> marketNames = hotItems.keySet();
            for (String name : marketNames) {
                if (name != null) {
                    mListenerMgr.addListener(new MarketHistoryObserver(mContext, exchangeType, name, ListenerMgr.LifeCycle.CREATE, ListenerMgr.LifeCycle.DESTROY));
                }
            }

            mListenerMgr.notifyCreate();
        }
    }

    public void create() {
        mListenerMgr.notifyCreate();
    }

    public void destroy() {
        mListenerMgr.notifyDestroy();
    }
}
