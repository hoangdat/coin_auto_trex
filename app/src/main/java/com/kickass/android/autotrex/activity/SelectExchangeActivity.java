package com.kickass.android.autotrex.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.abstraction.ExchangeAsyncAPI;
import com.kickass.android.autotrex.abstraction.IResponseAPI;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.abstraction.transaction.TransactionMgr;
import com.kickass.android.autotrex.bittrex.BittrexInfo;
import com.kickass.android.autotrex.service.ExchangeService;

import java.util.List;

public class SelectExchangeActivity extends AppCompatActivity implements View.OnClickListener, IResponseAPI.OnGetSumListener{

    private EditText mEdtAPI;
    private EditText mEdtSecret;
    private EditText mEdtBTC;
    private EditText mEdtETH;
    private EditText mEdtUSDT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_exchange);
        initView();
    }

    private void initView() {
        Button btn = (Button) findViewById(R.id.btnBittrex);
        btn.setOnClickListener(this);

        Button btnTrans = (Button) findViewById(R.id.btnTrans);
        btnTrans.setOnClickListener(this);

        mEdtAPI = (EditText) findViewById(R.id.apiEdt);
        mEdtSecret = (EditText) findViewById(R.id.secretEdt);

        mEdtBTC = (EditText) findViewById(R.id.budgetBTC);
        mEdtETH = (EditText) findViewById(R.id.budgetETH);
        mEdtUSDT = (EditText) findViewById(R.id.budgetUSDT);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnBittrex:
                startExchange();
                break;
            case R.id.btnTrans:
                startTransView();
                break;
            default:
                break;
        }
    }

    public void startExchange() {
//        double USDT;
//        double BTC;
//        double ETH;
//        TransactionMgr transactionMgr = TransactionMgr.getInstance(getApplicationContext());
//        if (mEdtUSDT.getText() != null) {
//            String str = "0";
//            if (mEdtUSDT.getText().toString() != null && !mEdtUSDT.getText().toString().isEmpty()) {
//                str = mEdtUSDT.getText().toString();
//            }
//            USDT = Double.parseDouble(str);
//            if (USDT > 0) {
//                transactionMgr.setBudget(ExchangeOperationMgr.ExchangeType.Bittrex, MarketItem.BasedMarketType.USDT, USDT);
//            }
//        }
//
//        if (mEdtETH.getText() != null) {
//            String str = "0";
//            if (mEdtETH.getText().toString() != null && !mEdtETH.getText().toString().isEmpty()) {
//                str = mEdtETH.getText().toString();
//            }
//            ETH = Double.parseDouble(str);
//            if (ETH > 0) {
//                transactionMgr.setBudget(ExchangeOperationMgr.ExchangeType.Bittrex, MarketItem.BasedMarketType.ETH, ETH);
//            }
//        }
//
//        if (mEdtBTC.getText() != null) {
//            String str = "0";
//            if (mEdtBTC.getText().toString() != null && !mEdtBTC.getText().toString().isEmpty()) {
//                str = mEdtBTC.getText().toString();
//            }
//            BTC = Double.parseDouble(str);
//            if (BTC > 0) {
//                transactionMgr.setBudget(ExchangeOperationMgr.ExchangeType.Bittrex, MarketItem.BasedMarketType.BTC, BTC);
//            }
//        }
//        String api = null;
//        String secret = null;
//        if (mEdtAPI.getText() != null) {
//            api = mEdtAPI.getText().toString();
//            if (api == null || api.isEmpty()) {
//                Toast.makeText(this, "please input API key", Toast.LENGTH_SHORT).show();
//                return;
//            }
//        }
//
//        if (mEdtSecret.getText() != null) {
//            secret = mEdtSecret.getText().toString();
//            if (secret == null || secret.isEmpty()) {
//                Toast.makeText(this, "please input API key", Toast.LENGTH_SHORT).show();
//                return;
//            }
//        }
//
//        if (api != null && secret != null) {
//            BittrexInfo.getInstance(getApplicationContext()).setKey(api, secret);
//        }

        Intent intent = new Intent(this, ExchangeService.class);
        intent.setAction(ExchangeService.START_UPDATE_EXCHANGE_ACTION);
        startService(intent);

        Intent intent_volume = new Intent(this, VolumeActivity.class);
        startActivity(intent_volume);
        finish();
    }

    public void startTransView() {
        Intent intent_volume = new Intent(this, TransactionActivity.class);
        startActivity(intent_volume);
        finish();
    }

    public void test() {
        ExchangeAsyncAPI.getInstance(this).getSummaries(ExchangeOperationMgr.ExchangeType.Bittrex, this);
    }

    @Override
    public void onGetSumDone(ExchangeOperationMgr.ExchangeType exchangeType, String marketName, MarketSummary sum) {

    }

    @Override
    public void onGetSummariesDone(ExchangeOperationMgr.ExchangeType exchangeType, List<MarketSummary> summaries) {

    }
}
