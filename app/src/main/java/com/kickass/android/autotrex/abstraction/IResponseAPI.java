package com.kickass.android.autotrex.abstraction;

import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.Balance;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.abstraction.response.PriceHistory;
import com.kickass.android.autotrex.abstraction.response.Ticker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 12/7/2017.
 */

public class IResponseAPI {
    public interface OnGetMarketListener {
        void onGetMarketDone(ExchangeOperationMgr.ExchangeType exchangeType, HashMap<String, MarketItem> market);
    }

    public interface OnGetSumListener {
        void onGetSumDone(ExchangeOperationMgr.ExchangeType exchangeType, String marketName, MarketSummary sum);
        void onGetSummariesDone(ExchangeOperationMgr.ExchangeType exchangeType, List<MarketSummary> summaries);
    }

    public interface OnGetBalancesListener {
        void onGetBalances(ExchangeOperationMgr.ExchangeType exchangeType, ArrayList<Balance> balances);

        void onGetBalance(ExchangeOperationMgr.ExchangeType exchangeType, String currency, Balance balance);
    }

    public interface OnMarketLimitListener {
        void onBuyDone(ExchangeOperationMgr.ExchangeType exchangeType, String uuid);

        void onSellDone(ExchangeOperationMgr.ExchangeType exchangeType, String uuid);

        void onCancelOrder(ExchangeOperationMgr.ExchangeType exchangeType, boolean isSuccess);
    }

    public interface OnGetTickerListener {
        void onGetTickerDone(ExchangeOperationMgr.ExchangeType exchangeType, String market, Ticker ticker);
    }

    public interface OnGetMarketHistory {
        void onGetMarketHistoryDone(ExchangeOperationMgr.ExchangeType exchangeType, String market, ArrayList<PriceHistory> priceHistory);
    }
}
