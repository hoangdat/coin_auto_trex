package com.kickass.android.autotrex.service.worker;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kickass.android.autotrex.abstraction.database.table.BuySellPointTable;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.abstraction.transaction.MonitorMgr;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.obsevable.Observable;
import com.kickass.android.autotrex.provider.ExchangeProvider;
import com.kickass.android.autotrex.service.filter.data.TransactionDataFilter;
import com.kickass.android.autotrex.util.JUnitHandler;
import com.kickass.android.autotrex.util.Utils;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by dat.ph on 1/24/2018.
 */

public class WorkerMonitorToSell implements Runnable {
    private static final String FIREBASE_PRICE_THREAD_NAME = "firebase_price_thread_name";
    private Context mContext;

    private static WorkerMonitorToSell sInstance;
    private HandlerThread mFirebaseThread;
    private FirebaseMonitorPriceHandler mFirebasePriceHandler;

    private WorkerMonitorToSell(Context context) {
        mContext = context;
        mFirebaseThread = new HandlerThread(FIREBASE_PRICE_THREAD_NAME, Process.THREAD_PRIORITY_BACKGROUND);
        mFirebaseThread.start();

        Looper looper = mFirebaseThread.getLooper();
        if (looper != null) {
            mFirebasePriceHandler = new FirebaseMonitorPriceHandler(mContext, looper);
        }
    }

    public static WorkerMonitorToSell getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new WorkerMonitorToSell(context);
        }
        return sInstance;
    }

    @Override
    public void run() {
        Log.d(this, "run sell *****");
        HashMap<String, TransactionDataFilter> mapUUID = MonitorMgr.getInstance(mContext).getMapUUIDSell();
        Set<String> keys = mapUUID.keySet();

        for (String uuid : keys) {
            Log.d(this, "run sell uuid : " + uuid);
            TransactionDataFilter dataFilter = mapUUID.get(uuid);
            Order order = dataFilter.getOrderInfo();
            Log.d(this, "run sell uuid : " + uuid + "-or = " + order);
            if (order != null) {
                double expectedTP = order.getExpectedTP();
                double expectedSL = order.getExpectedSL();
                ExchangeOperationMgr.ExchangeType exchangeType = dataFilter.getExchangeType();
                String marketName = order.getMarketName();
                Log.d(this, "run sell uuid : " + uuid + "-tp:" + expectedTP + "-sl:" + expectedSL + "-mN:" + marketName);
                if (exchangeType != null && marketName != null) {
                    Ticker ticker = ExchangeOperationMgr.getInstance(mContext).getTicker(exchangeType, marketName);

                    if (ticker == null) {
                        Log.d(this, "run sell uuid : " + uuid + "-ticket null");
                        continue;
                    }
                    double last = ticker.getLast();
                    double bid = ticker.getBid();
                    boolean isSell = false;
                    if (bid >= expectedTP) {
                        Log.d(this, "run sell uuid : " + uuid + "-tp = " + ((last + expectedTP) / 2));
                        order.setPriceEnd(expectedTP / 2);
                        isSell = true;
                    } else if (last <= expectedSL) {
                        Log.d(this, "run sell uuid : " + uuid + "-sl = " + last);
                        order.setPriceEnd(last);
                        isSell = true;
                    }

                    if (isSell) {
                        Log.d(this, "run sell uuid : " + uuid + " Sell");
                        MonitorMgr.getInstance(mContext).clearUUIDSell(uuid);
                        dataFilter.setActionType(ExchangeOperationMgr.ActionType.Sell_Limit);
                        Observable.getInstance().notifyObservers(Observable.ObservableType.TRANSACTION, dataFilter);
                    }

                    FirebaseMonitorPriceData data = new FirebaseMonitorPriceData(order, ticker);
                    mFirebasePriceHandler.sendMessage(mFirebasePriceHandler.obtainMessage(1, data));
                }
            }
        }
    }

    private static class FirebaseMonitorPriceData {
        private Order mOrder;
        private Ticker mTicker;

        public FirebaseMonitorPriceData(Order mOrder, Ticker mTicker) {
            this.mOrder = mOrder;
            this.mTicker = mTicker;
        }
    }

    private final static class FirebaseMonitorPriceHandler extends JUnitHandler<FirebaseMonitorPriceData> {
        private DatabaseReference mDataRef;
        private Context mContext;

        public FirebaseMonitorPriceHandler(Context context, Looper looper) {
            super(looper);
            mContext = context;
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            mDataRef = database.getReference("monitor_price_order");
        }

        @Override
        public void handleMessage(Message msg) {
            FirebaseMonitorPriceData dataFilter = getMsgValue(msg.obj);
            if (dataFilter != null) {
                Order order = dataFilter.mOrder;
                Ticker ticker = dataFilter.mTicker;

                double price = order.getPrice();
                double twoPercent = price * (1 + 0.02f);
                double threePercent = price * (1 + 0.025f);
                double minusOnePercent = price * (1 - 0.01);
                double minusTwoPercent = price * (1 - 0.02);

                double last = ticker.getLast();
                String type = null;
                if (last <= minusTwoPercent) {
                    type = "-2%";
                } else if (last <= minusOnePercent) {
                    type = "-1%";
                } else if (last >= twoPercent) {
                    type = "2%";
                } else if (last >= threePercent) {
                    type = "3%";
                }

                ContentValues values = toValues(last, order, type);
                Uri uri = ExchangeProvider.UriString.getBuySellUri(ExchangeOperationMgr.ExchangeType.Bittrex);
                mContext.getContentResolver().insert(uri, values);
            }
        }

//        private void toUpdateFirebase(double last, Order order, String type) {
//            DatabaseReference child = mDataRef.child(order.getBuyUuid());
//            child.child("last").setValue(last);
//            child.child("type").setValue(type);
//            child.child("expectedTP").setValue(order.getExpectedTP());
//            child.child("expectedSL").setValue(order.getExpectedSL());
//            child.child("timeBuy").setValue(Utils.convertMillisecondToDateTime(order.getBoughtTime()));
//            child.child("priceBuy").setValue(order.getPrice());
//            child.child("currentTime").setValue(Utils.convertMillisecondToDateTime(System.currentTimeMillis()));
//        }

        private ContentValues toValues(double last, Order order, String type) {
            ContentValues values = new ContentValues();

            values.put(BuySellPointTable.BuySellPointColumns.PRICE_LAST, last);
            values.put(BuySellPointTable.BuySellPointColumns.TYPE, type);
            values.put(BuySellPointTable.BuySellPointColumns.EXPECTED_TP, order.getExpectedTP());
            values.put(BuySellPointTable.BuySellPointColumns.EXPECTED_SL, order.getExpectedSL());
            values.put(BuySellPointTable.BuySellPointColumns.TIME_BUY, Utils.convertMillisecondToDateTime(order.getBoughtTime()));
            values.put(BuySellPointTable.BuySellPointColumns.PRICE_BUY, order.getPrice());
            values.put(BuySellPointTable.BuySellPointColumns.CURRENT_TIME, Utils.convertMillisecondToDateTime(System.currentTimeMillis()));

            return values;
        }
    }
}
