package com.kickass.android.autotrex.bittrex;

import android.content.Context;

import com.kickass.android.autotrex.abstraction.account.AbsExchangeInfo;
import com.kickass.android.autotrex.util.PreferenceUtils;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class BittrexInfo extends AbsExchangeInfo {
    public static String sKey;
    public static String sSecretKey;

    private static BittrexInfo mInstance;

    private BittrexInfo(Context context) {
        super(context);
    }

    public static void setKey(String api, String secret) {
        sKey = api;
        sSecretKey = secret;
        sIsAddingKey = false;
    }

    public static BittrexInfo getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new BittrexInfo(context);
        }
        return mInstance;
    }

    public static boolean isAddingKey() {
        return sIsAddingKey;
    }

    public boolean isHasKey() {
        Context context = getContext();
        if (context != null) {
            return (PreferenceUtils.getAPIKey(context) != null && PreferenceUtils.getSecretKey(context) != null);
        }
        return false;
    }

    public boolean reloadKey() {
        Context context = getContext();
        if (context != null) {
            sKey = PreferenceUtils.getAPIKey(context);
            sSecretKey = PreferenceUtils.getSecretKey(context);
            return sSecretKey != null && sKey != null;
        }
        return false;
    }
}
