package com.kickass.android.autotrex.service.worker;

import android.content.Context;
import android.os.Message;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.abstraction.transaction.MonitorMgr;
import com.kickass.android.autotrex.abstraction.transaction.TransactionMgr;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.obsevable.Observable;
import com.kickass.android.autotrex.service.filter.data.TransactionDataFilter;
import com.kickass.android.autotrex.util.JUnitHandler;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by Dat on 1/23/2018.
 */
public class WorkerMonitorOrder implements Runnable {
    private Context mContext;
    private static WorkerMonitorOrder sInstance;
    private WorkerMonitorOrder(Context context) {
        mContext = context;
    }

    public static WorkerMonitorOrder getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new WorkerMonitorOrder(context);
        }
        return sInstance;
    }

    @Override
    public void run() {
        Log.d(this, "run order *****");
        HashMap<String, TransactionDataFilter> mapUUID = MonitorMgr.getInstance(mContext).getMapUUIDOrder();
        Set<String> keys = mapUUID.keySet();

        for (String uuid : keys) {
            Log.d(this, "run order uuid : " + uuid);
            TransactionDataFilter data = mapUUID.get(uuid);
            String buyUUID = null;
            if (data.getOrderInfo() != null) {
                buyUUID = data.getOrderInfo().getBuyUuid();
            }
            ExchangeOperationMgr.ExchangeType exchangeType = data.getExchangeType();
            Order order = ExchangeOperationMgr.getInstance(mContext).getOrder(exchangeType, uuid);
            Log.d(this, "run  order uuid : " + uuid + "-or = " + order);
            if (order != null) {
                boolean isOpen = order.isOpen();
                Log.d(this, "run order uuid : " + uuid + "-iOpen = " + isOpen);
                if (!isOpen) {
                    MonitorMgr.getInstance(mContext).clearUUIDOrder(uuid);
                    Order.OrderType type = order.getOrderType();
                    Log.d(this, "run order uuid : " + uuid + "-iOpen = " + isOpen + "-ty = " + type);
                    switch (type) {
                        case LIMIT_BUY:
                            ExchangeOperationMgr.getInstance(mContext).updateOrder(exchangeType, TransactionMgr.StatusType.BOUGHT, order);
                            data.setActionType(ExchangeOperationMgr.ActionType.Monitor_to_Sell);
                            Observable.getInstance().notifyObservers(Observable.ObservableType.TRANSACTION, data);
                            break;
                        case LIMIT_SELL:
                            order.setBuyUuid(buyUUID);
                            data.setActionType(ExchangeOperationMgr.ActionType.Nothing_to_do);
                            ExchangeOperationMgr.getInstance(mContext).updateOrder(exchangeType, TransactionMgr.StatusType.SOLD, order);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
