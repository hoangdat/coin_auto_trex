package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dat.ph on 12/8/2017.
 */

public class SellLimitRequest extends AbsBittrexRequest<String>{
    public SellLimitRequest(String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
    }

    public static SellLimitRequest createInstance(String market, String quantity, String rate,
                                                  Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl(market, quantity, rate);
        return new SellLimitRequest(url, true, null, listener, errorListener);
    }

    private static String makeUrl(String market, String quantity, String rate) {
        String url = BittrexConstant.ENDPOINT_V1_1 + BittrexConstant.MARKET_API +
                Constant.SPLASH + BittrexConstant.SELL_LIMIT +
                Constant.QUESTION_MARK + BittrexConstant.MARKET_API + Constant.EQUAL_SIGN + market +
                Constant.AND_SIGN + BittrexConstant.QUANTITY + Constant.EQUAL_SIGN + quantity +
                Constant.AND_SIGN + BittrexConstant.RATE + Constant.EQUAL_SIGN + rate;
        return addParameter(url, true);
    }

    @Override
    protected String parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        String uuid = null;
        if (response != null) {
            try {
                JSONObject result = response.getJSONObject(BittrexConstant.RESPONSE_RESULT);
                uuid = result.getString(BittrexConstant.RESPONSE_UUID);
            } catch (JSONException e) {
                Log.d(this, "parse Exception: " + e.getMessage());
            }
        }
        return uuid;
    }
}
