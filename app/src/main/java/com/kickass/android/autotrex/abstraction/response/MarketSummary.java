package com.kickass.android.autotrex.abstraction.response;

/**
 * Created by dat.ph on 12/7/2017.
 */

public class MarketSummary {
    private String mMarketName;
    private double mBaseVolume;
    private MarketItem.BasedMarketType mBaseMarketType;

    public MarketSummary(String mMarketName, double mBaseVolume, MarketItem.BasedMarketType basedMarketType) {
        this.mMarketName = mMarketName;
        this.mBaseVolume = mBaseVolume;
        this.mBaseMarketType = basedMarketType;
    }

    public String getMarketName() {
        return mMarketName;
    }

    public MarketItem.BasedMarketType getBaseMarketType() {
        return mBaseMarketType;
    }

    public double getBaseVolume() {
        return mBaseVolume;
    }
}
