package com.kickass.android.autotrex.abstraction.response;

/**
 * Created by dat.ph on 12/8/2017.
 */

public class Ticker {
    private String mMarketName;
    private double mBid;
    private double mAsk;
    private double mLast;

    public Ticker(String marketName, double mBid, double mAsk, double mLast) {
        this.mMarketName = marketName;
        this.mBid = mBid;
        this.mAsk = mAsk;
        this.mLast = mLast;
    }

    public double getBid() {
        return mBid;
    }

    public double getAsk() {
        return mAsk;
    }

    public double getLast() {
        return mLast;
    }

    public String getMarketName() {
        return mMarketName;
    }
}
