package com.kickass.android.autotrex.listener;

import com.kickass.android.autotrex.log.Log;

import java.util.ArrayList;
import java.util.List;

public class ListenerMgr {
    public enum LifeCycle {
        CREATE,
        START,
        RESUME,
        PAUSE,
        DESTROY
    }

    private List<AbsListenerImp> mListenerList;

    public ListenerMgr() {
        mListenerList = new ArrayList<>();
    }

    public void addListener(AbsListenerImp listener) {
        if (listener != null) {
            if (!mListenerList.contains(listener)) {
                mListenerList.add(listener);
            }
        } else {
            Log.e(this, "can't add the null listener.");
        }
    }

    private void clearListener() {
        mListenerList.clear();
    }

    public void notifyCreate() {
        for (AbsListenerImp imp : mListenerList) {
            if (imp.getRegisterTime() == LifeCycle.CREATE) {
                Log.d(this, "onCreate : register "+ imp.getClass().getSimpleName());
                imp.registerListener();
            }
        }
    }

    public void notifyResume() {
        for (AbsListenerImp imp : mListenerList) {
            if (imp.getRegisterTime() == LifeCycle.RESUME) {
                Log.d(this, "onResume : register "+ imp.getClass().getSimpleName());
                imp.registerListener();
            }
        }
    }

    public void notifyPause() {
        for (AbsListenerImp imp : mListenerList) {
            if (imp.getUnregisterTime() == LifeCycle.PAUSE) {
                Log.d(this, "onPause : unregister "+ imp.getClass().getSimpleName());
                imp.unregisterListener();
            }
        }
    }

    public void notifyDestroy() {
        for (AbsListenerImp imp : mListenerList) {
            if (imp.getUnregisterTime() == LifeCycle.DESTROY) {
                Log.d(this, "onDestory : unregister "+ imp.getClass().getSimpleName());
                imp.unregisterListener();
            }
        }
        clearListener();
    }
}
