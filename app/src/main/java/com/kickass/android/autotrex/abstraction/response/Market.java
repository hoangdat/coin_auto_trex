package com.kickass.android.autotrex.abstraction.response;



/**
 * Created by dat.ph on 12/6/2017.
 */

public class Market {


    private String mMarketName; //BTC, DGB, LTC, ...
    private MarketItem.BasedMarketType mBasedMarket;
    private String mName;

    public Market(String name, String marketName, MarketItem.BasedMarketType basedType) {
        mMarketName = marketName;
        mName = name;
        mBasedMarket = basedType;
    }
}
