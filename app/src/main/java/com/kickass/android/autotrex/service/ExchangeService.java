package com.kickass.android.autotrex.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.abstraction.ExchangeAsyncAPI;
import com.kickass.android.autotrex.abstraction.IResponseAPI;
import com.kickass.android.autotrex.abstraction.MarketsMgr;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.abstraction.transaction.MonitorMgr;
import com.kickass.android.autotrex.abstraction.transaction.TransactionMgr;
import com.kickass.android.autotrex.activity.VolumeActivity;
import com.kickass.android.autotrex.bittrex.Utils;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.service.filter.MarketHistoryFilter;
import com.kickass.android.autotrex.service.worker.WorkerStartMonitor;
import com.kickass.android.autotrex.service.worker.WorkerUpdateHotMarket;
import com.kickass.android.autotrex.service.worker.WorkerUpdateMarketHistory;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ExchangeService extends Service implements IResponseAPI.OnGetMarketListener, IResponseAPI.OnGetSumListener{
    public static final String START_UPDATE_EXCHANGE_ACTION = "start_update_exchange";
    private static final int START_UPDATE_EXCHANGE_MSG = 1;
    private static final int START_MARKET_HISTORY_FILTER = 2;
    private static final int START_MONITOR_TRANSACTION = 3;
    private static final int NOTIFICATION_FOREGROUND_ID = 205;
    private static final int REQUEST_CODE = 610;
    private static final int PERIOD_TIME_QUERY_MARKET_HISTORY_SECONDS = 48;
    private static final int PERIOD_TIME_QUERY_HOT_MARKET_SECONDS = 60 * 60 * 2;

    public static HashMap<MarketItem.BasedMarketType, Double> sMapThresholds = new HashMap<>();
    static {
        sMapThresholds.put(MarketItem.BasedMarketType.BTC, 2100.0);
        sMapThresholds.put(MarketItem.BasedMarketType.ETH, 3000.0);
        sMapThresholds.put(MarketItem.BasedMarketType.USDT, 10000000.0);
    }

    private WorkerHandler mWorkerHandler;
    private Intent mSavedIntent;
    private MarketHistoryFilter mMarketHistoryFilter;
    private static long sStartTime;

    @Override
    public IBinder onBind(Intent intent) {
        throw null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mMarketHistoryFilter = new MarketHistoryFilter(getApplicationContext());
        mWorkerHandler = new WorkerHandler(this);

        TransactionMgr transactionMgr = TransactionMgr.getInstance(this);
        transactionMgr.setBudget(ExchangeOperationMgr.ExchangeType.Bittrex, MarketItem.BasedMarketType.BTC, 0.001);
        transactionMgr.setBudget(ExchangeOperationMgr.ExchangeType.Bittrex, MarketItem.BasedMarketType.ETH, 0.01);
        transactionMgr.setTPSL(ExchangeOperationMgr.ExchangeType.Bittrex, 0.03, -0.02);

        MonitorMgr.getInstance(this);

        mWorkerHandler.sendMessage(mWorkerHandler.obtainMessage(START_MONITOR_TRANSACTION));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(this, "onStartCommand() intent = " + intent + "-flag = " + flags);
        mSavedIntent = intent;

        startForeground(NOTIFICATION_FOREGROUND_ID, getCompatNotification());
        if (MarketsMgr.getInstance().getMarkets(ExchangeOperationMgr.ExchangeType.Bittrex) == null) {
            ExchangeAsyncAPI.getInstance(this).getMarkets(ExchangeOperationMgr.ExchangeType.Bittrex, this);
            return START_REDELIVER_INTENT;
        }

        if (MarketsMgr.getInstance().getHotMarkets(ExchangeOperationMgr.ExchangeType.Bittrex) == null) {
            Log.d(this, "onStartCommand() hot Market null");
            ExchangeAsyncAPI.getInstance(this).getSummaries(ExchangeOperationMgr.ExchangeType.Bittrex, this);
            return START_REDELIVER_INTENT;
        }

        startWork(intent);
        return START_REDELIVER_INTENT;
    }

    private Notification getCompatNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Auto service")
                .setTicker("Auto tool")
                .setWhen(System.currentTimeMillis());
        Intent startIntent = new Intent(getApplicationContext(),
                VolumeActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(
                this, REQUEST_CODE, startIntent, 0);
        builder.setContentIntent(contentIntent);
        return builder.build();
    }

    @Override
    public void onDestroy() {
        Log.d(this, "onDestroy()");
        if (mMarketHistoryFilter != null) {
            mMarketHistoryFilter.destroy();
            mMarketHistoryFilter = null;
        }
        super.onDestroy();
    }

    @Override
    public void onGetMarketDone(ExchangeOperationMgr.ExchangeType exchangeType, HashMap<String, MarketItem> market) {
        Log.d(this, "onGetMarketDone()");
        if (market != null) {
            MarketsMgr.getInstance().putMarkets(exchangeType, market);

            if (MarketsMgr.getInstance().getHotMarkets(ExchangeOperationMgr.ExchangeType.Bittrex) == null) {
                ExchangeAsyncAPI.getInstance(this).getSummaries(ExchangeOperationMgr.ExchangeType.Bittrex, this);
                return;
            }

            startWork(mSavedIntent);
        }
    }

    @Override
    public void onGetSumDone(ExchangeOperationMgr.ExchangeType exchangeType, String marketName, MarketSummary sum) {

    }

    @Override
    public void onGetSummariesDone(ExchangeOperationMgr.ExchangeType exchangeType, List<MarketSummary> summaries) {
        Log.d(this, "onGetSummariesDone()");
        if (summaries != null) {
            updateHotMarkets(summaries);
            startWork(mSavedIntent);
        } else {
            Log.d(this, "onGetSummariesDone() - kill");
            stopForeground(true);
            stopSelf();
        }
    }

    private static class WorkerHandler extends Handler {
        private WeakReference<ExchangeService> mRefService;
        private ScheduledExecutorService mScheduledExecutorService;
        private ScheduledFuture mScheduledFuture;

        WorkerHandler(ExchangeService service) {
            mRefService = new WeakReference<>(service);
            mScheduledExecutorService = Executors.newScheduledThreadPool(2);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.d(this, "handleMessage()");
            ExchangeService service = mRefService.get();
            if (service != null) {
                int what = msg.what;
                switch (what) {
                    case START_UPDATE_EXCHANGE_MSG:
                        Log.d(this, "handleMessage(): " + what);
                        if (mScheduledFuture != null) {
                            if (!mScheduledFuture.isDone() || !mScheduledFuture.isCancelled()) {
                                Log.d(this, "handleMessage: cancel");
                                mScheduledFuture.cancel(true);
                            }
                        }
                        mScheduledFuture = mScheduledExecutorService.scheduleAtFixedRate(
                                WorkerUpdateMarketHistory.getInstance(service), 0,
                                PERIOD_TIME_QUERY_MARKET_HISTORY_SECONDS, TimeUnit.SECONDS);

                        mScheduledExecutorService.scheduleAtFixedRate(
                                WorkerUpdateHotMarket.getInstance(service),
                                PERIOD_TIME_QUERY_HOT_MARKET_SECONDS,
                                PERIOD_TIME_QUERY_HOT_MARKET_SECONDS, TimeUnit.SECONDS);
                        break;
                    case START_MARKET_HISTORY_FILTER:
                        service.mMarketHistoryFilter.startFilter(ExchangeOperationMgr.ExchangeType.Bittrex, MarketsMgr.getInstance().getHotMarkets(ExchangeOperationMgr.ExchangeType.Bittrex));
                        break;
                    case START_MONITOR_TRANSACTION:
                        mScheduledExecutorService.submit(WorkerStartMonitor.getInstance(service));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void startFilter() {
        mWorkerHandler.removeMessages(START_MARKET_HISTORY_FILTER);
        mWorkerHandler.sendEmptyMessage(START_MARKET_HISTORY_FILTER);
    }

    private void startWork(Intent intent) {
        startFilter();
        if (intent != null) {
            if (START_UPDATE_EXCHANGE_ACTION.equals(intent.getAction())) {
                Log.d(this, "onStartCommand()");
                mWorkerHandler.removeMessages(START_UPDATE_EXCHANGE_MSG);
                mWorkerHandler.sendEmptyMessage(START_UPDATE_EXCHANGE_MSG);
                sStartTime = System.currentTimeMillis();
                Log.d(this, "onStartCommand() startTime = " + sStartTime);
            }
        }
    }

    public static void updateHotMarkets(List<MarketSummary> summaries) {
        Log.d(ExchangeService.class.getSimpleName(), "updateHotMarkets");
        if (summaries != null) {
//            Log.d(ExchangeService.class.getSimpleName(), "updateHotMarkets s = " + summaries.size());
            HashMap<String, MarketItem> hotMarkets = new HashMap<>();
            HashMap<String, MarketItem> markets = MarketsMgr.getInstance().getMarkets(ExchangeOperationMgr.ExchangeType.Bittrex);
            for (MarketSummary sum : summaries) {
                if (sum != null) {
                    String marketName = sum.getMarketName();
                    if (marketName == null || markets.get(marketName) == null) {
                        continue;
                    }
                    double baseVolume = sum.getBaseVolume();
                    MarketItem.BasedMarketType basedMarketType = Utils.getBaseFromMarketName(marketName);
//                    Log.d(ExchangeService.class.getSimpleName(), "updateHotMarkets() name = " + marketName + "-b = " + baseVolume);
                    double threshold = sMapThresholds.get(basedMarketType);
                    if (baseVolume >= threshold) {
//                        Log.d(ExchangeService.class.getSimpleName(), "updateHotMarkets() name = " + marketName + "-b = " + baseVolume + "-" + threshold);
                        hotMarkets.put(marketName, new MarketItem(markets.get(marketName), baseVolume));
                    }
                }
            }
            MarketsMgr.getInstance().putHotMarkets(ExchangeOperationMgr.ExchangeType.Bittrex, hotMarkets);
        }
    }

    public static long getStartTime() {
        return sStartTime;
    }
}
