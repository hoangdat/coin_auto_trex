package com.kickass.android.autotrex.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;

import com.kickass.android.autotrex.log.Log;


public abstract class AbsBroadcastReceiverImp extends AbsListenerImp {
    protected Context mContext;
    protected BroadcastReceiver mReceiver;
    protected IntentFilter mIntentFilter;

    abstract IntentFilter getIntentFilter();

    abstract BroadcastReceiver getBroadcastReceiver();

    public AbsBroadcastReceiverImp(Context context, ListenerMgr.LifeCycle registerTime, ListenerMgr.LifeCycle unregisterTime) {
        super(registerTime, unregisterTime);
        mContext = context;
        mReceiver = getBroadcastReceiver();
        mIntentFilter = getIntentFilter();
    }

    @Override
    public void registerListener() {
        try {
            mContext.registerReceiver(mReceiver, mIntentFilter);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unregisterListener() {
        try {
            mContext.unregisterReceiver(mReceiver);
        } catch (Exception e) {
            Log.e(this, "Receiver is already unregistered or Not registered");
            Log.e(this, "Exception:" + e.toString());
        }

    }

}
