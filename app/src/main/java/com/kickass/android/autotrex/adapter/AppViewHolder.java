package com.kickass.android.autotrex.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by dat.ph on 1/10/2018.
 */

public class AppViewHolder extends RecyclerView.ViewHolder {
    private int mMaxSz;
    private View mView[];

    public static AppViewHolder createViewHolder(View itemView, int max) {
        AppViewHolder ret = new AppViewHolder(itemView, max);
        return ret;
    }

    public AppViewHolder(View itemView, int max) {
        super(itemView);
        mView = new View[max];
        mMaxSz = max;
    }

    public void addView(int key, View view) {
        if (key < mMaxSz && mView != null) {
            mView[key] = view;
        }
    }

    public View getView(int key) {
        View ret = null;
        if (key < mMaxSz && mView != null) {
            ret = mView[key];
        }
        return ret;
    }

    public <T extends View> T getView(int key, Class<T> type) {
        T ret = null;
        if (key < mMaxSz && mView != null) {
            ret = type.cast(mView[key]);
        }
        return ret;
    }
}
