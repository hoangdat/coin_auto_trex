package com.kickass.android.autotrex.abstraction.response;

import android.text.TextUtils;

/**
 * Created by dat.ph on 12/11/2017.
 */

public class MarketItem {
    public enum BasedMarketType {
        BTC("BTC"),
        ETH("ETH"),
        USDT("USDT"),
        NONE(null);

        private String mValue;

        BasedMarketType(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        public static BasedMarketType fromString(String value) {
            for (BasedMarketType type : BasedMarketType.values()) {
                if (TextUtils.equals(type.getValue(), value)) {
                    return type;
                }
            }
            return null;
        }
    }
    private String mMarketName; //BTC-LTC
    private String mMarketCurrency; //LTC
    private String mMarketCurLong; //Litecoin
    private BasedMarketType mBasedMarket;
    private double mMinTradeSize;
    private double mBase24hVolume;

    public MarketItem(String mMarketName, String mMarketCurrency, String mMarketCurLong, String basedCurrency, double minTrade) {
        this.mMarketName = mMarketName;
        this.mMarketCurrency = mMarketCurrency;
        this.mMarketCurLong = mMarketCurLong;
        this.mBasedMarket = BasedMarketType.fromString(basedCurrency);
        this.mMinTradeSize = minTrade;
    }

    public MarketItem(MarketItem item, double base24Volume) {
        this.mMarketName = item.getMarketName();
        this.mMarketCurrency = item.getMarketCurrency();
        this.mMarketCurLong = item.getMarketCurLong();
        this.mBasedMarket = item.getBasedMarket();
        this.mMinTradeSize = item.getMinTradeSize();
        this.mBase24hVolume = base24Volume;
    }

    public String getMarketName() {
        return mMarketName;
    }

    public String getMarketCurrency() {
        return mMarketCurrency;
    }

    public String getMarketCurLong() {
        return mMarketCurLong;
    }

    public BasedMarketType getBasedMarket() {
        return mBasedMarket;
    }

    public double getBase24hVolume() {
        return mBase24hVolume;
    }

    public double getMinTradeSize() {
        return mMinTradeSize;
    }

    @Override
    public boolean equals(Object obj) {
        String otherName = null;
        if (obj instanceof MarketItem) {
            otherName = ((MarketItem)obj).getMarketName();
        }

        return TextUtils.equals(mMarketName, otherName);
    }
}
