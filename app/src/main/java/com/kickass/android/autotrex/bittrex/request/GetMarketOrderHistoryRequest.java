package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;
import com.kickass.android.autotrex.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by dat.ph on 1/29/2018.
 */

public class GetMarketOrderHistoryRequest extends AbsBittrexRequest<ArrayList<Order>> {
    public GetMarketOrderHistoryRequest(String url, boolean isRequiredAuth, Response.Listener listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, null, listener, errorListener);
    }

    private static String makeUrl(String name) {
        String url = BittrexConstant.ENDPOINT_V1_1 + BittrexConstant.ACCOUNT_API +
                Constant.SPLASH + BittrexConstant.GET_ORDER_HISTORY +
                Constant.QUESTION_MARK + BittrexConstant.MARKET_API + Constant.EQUAL_SIGN + name;
        return addParameter(url, true);
    }

    public static GetMarketOrderHistoryRequest createInstance(String name, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl(name);
        return new GetMarketOrderHistoryRequest(url, true, listener, errorListener);
    }

    @Override
    protected ArrayList<Order> parse(JSONObject response) {
        Log.d(this, "parse()");
        ArrayList<Order> orders = new ArrayList<>();
        if (response != null) {
            try {
                JSONArray resultArr = response.getJSONArray(BittrexConstant.RESPONSE_RESULT);
                int length = resultArr.length();
                for (int i = 0; i < length; i++) {
                    JSONObject result = resultArr.getJSONObject(i);
                    String uuid = result.getString(BittrexConstant.RESPONSE_ORDER_UUID);
                    String marketName = result.getString(BittrexConstant.RESPONSE_ORDER_EXCHANGE);
                    String type = result.getString(BittrexConstant.RESPONSE_ORDER_TYPE);
                    double quantity = result.getDouble(BittrexConstant.RESPONSE_QUANTITY);
                    Double price_per_unit = result.getDouble(BittrexConstant.RESPONSE_PRICE_PER_UNIT);
                    String time = result.getString(BittrexConstant.RESPONSE_TIMESTAMP);
                    Order.OrderType orderType = Order.OrderType.fromString(type);

                    Log.d(this, "parse ppu = " + price_per_unit);
                    Order order = new Order(uuid, marketName, quantity,
                            price_per_unit != null ? price_per_unit.doubleValue() : 0, orderType, Utils.convertLastModifiedTime(time));
                    orders.add(order);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return orders;
    }
}
