package com.kickass.android.autotrex.service.worker;

import android.util.SparseArray;

import com.kickass.android.autotrex.abstraction.MarketsMgr;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.bittrex.Utils;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.service.ExchangeService;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 1/8/2018.
 */

public class WorkerUpdateHotMarket implements Runnable{
    private WeakReference<ExchangeService> mRef;

    private static WorkerUpdateHotMarket sInstance;
    public static WorkerUpdateHotMarket getInstance(ExchangeService service) {
        if (sInstance == null) {
            sInstance = new WorkerUpdateHotMarket(service);
        }
        return sInstance;
    }

    public WorkerUpdateHotMarket(ExchangeService service) {
        mRef = new WeakReference<>(service);
    }

    @Override
    public void run() {
        Log.d(this, "run() =*********HOT***********************=");
        ExchangeService service = mRef.get();
        if (service != null) {
            HashMap<String, MarketItem> marketItems = null;
            if (MarketsMgr.getInstance().getMarkets(ExchangeOperationMgr.ExchangeType.Bittrex) == null) {
                marketItems = ExchangeOperationMgr.getInstance(service).getMarkets(ExchangeOperationMgr.ExchangeType.Bittrex);
                MarketsMgr.getInstance().putMarkets(ExchangeOperationMgr.ExchangeType.Bittrex, marketItems);
            }

            if (marketItems != null) {
                List<MarketSummary> summaries = ExchangeOperationMgr.getInstance(service.getApplicationContext()).getSummaries(ExchangeOperationMgr.ExchangeType.Bittrex);
                ExchangeService.updateHotMarkets(summaries);
            }
        }
    }
}
