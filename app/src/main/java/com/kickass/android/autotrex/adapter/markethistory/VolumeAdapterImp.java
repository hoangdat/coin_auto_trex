package com.kickass.android.autotrex.adapter.markethistory;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.abstraction.MarketsMgr;
import com.kickass.android.autotrex.abstraction.database.table.MarketHistoryTable;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.adapter.AbsCursorAdapterImp;
import com.kickass.android.autotrex.adapter.AppViewHolder;
import com.kickass.android.autotrex.adapter.IndexViewHolder;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.provider.ExchangeProvider;

import java.text.DecimalFormat;

/**
 * Created by dat.ph on 1/10/2018.
 */

public class VolumeAdapterImp extends AbsCursorAdapterImp {
    public VolumeAdapterImp(Context context, int layoutId) {
        super(context, layoutId);
    }

    @Override
    public Uri getUri() {
        return ExchangeProvider.UriString.getMarketHistoryListVolumeUri(ExchangeOperationMgr.ExchangeType.Bittrex);
    }

    @Override
    public String[] getProjection() {
        Log.d(this, "getProjection()");
        String[] projection = new String[4];
        projection[0] = MarketHistoryTable.MarketHistoryColumns.MARKET_NAME;
        projection[1] = "TOTAL(CASE " + MarketHistoryTable.MarketHistoryColumns.ORDER_TYPE +
                " WHEN 'SELL' THEN " + MarketHistoryTable.MarketHistoryColumns.QUANTITY +
                " ELSE 0 END) as " + MarketHistoryTable.MarketHistoryColumns.AS_SELL_TOTAL;
        projection[2] = "TOTAL(CASE " + MarketHistoryTable.MarketHistoryColumns.ORDER_TYPE +
                " WHEN 'BUY' THEN " + MarketHistoryTable.MarketHistoryColumns.QUANTITY +
                " ELSE 0 END) as " + MarketHistoryTable.MarketHistoryColumns.AS_BUY_TOTAL;
        projection[3] = "MIN(" + MarketHistoryTable.MarketHistoryColumns.TIME_DONE + ") as " +
                MarketHistoryTable.MarketHistoryColumns.AS_MIN_START_DONE;

        return projection;
    }

    @Override
    protected AppViewHolder _createViewHolder(View itemView) {
        AppViewHolder appViewHolder = AppViewHolder.createViewHolder(itemView, IndexViewHolder.MAX);

        appViewHolder.addView(IndexViewHolder.MH_MARKET_NAME, itemView.findViewById(R.id.volume_txtMarketName));
        appViewHolder.addView(IndexViewHolder.MH_BASE_24H_VOLUME, itemView.findViewById(R.id.volume_txtBaseVolume));
        appViewHolder.addView(IndexViewHolder.MH_TOTAL_VOLUME, itemView.findViewById(R.id.volume_txtTotalVolume));
        appViewHolder.addView(IndexViewHolder.MH_BUY_VOLUME, itemView.findViewById(R.id.volume_txtBuy));
        appViewHolder.addView(IndexViewHolder.MH_SELL_VOLUME, itemView.findViewById(R.id.volume_txtSell));
        appViewHolder.addView(IndexViewHolder.MH_MIN_START_DONE, itemView.findViewById(R.id.volume_txtMinTimeDone));

        return appViewHolder;
    }

    @Override
    protected void _bindView(AppViewHolder holder, int position) {
//        Log.d(this, "_bindView()");
        Cursor cursor = getCursor();
        if (cursor == null || cursor.isClosed() || cursor.getCount() <= position) {
            Log.d(this, "_bindView cursor not proper");
            return;
        }
        try {
            cursor.moveToPosition(position);
            String marketName = cursor.getString(cursor.getColumnIndex(MarketHistoryTable.MarketHistoryColumns.MARKET_NAME));
            double buy = cursor.getDouble(cursor.getColumnIndex(MarketHistoryTable.MarketHistoryColumns.AS_BUY_TOTAL));
            double sell = cursor.getDouble(cursor.getColumnIndex(MarketHistoryTable.MarketHistoryColumns.AS_SELL_TOTAL));
            long timeDone = cursor.getLong(cursor.getColumnIndex(MarketHistoryTable.MarketHistoryColumns.AS_MIN_START_DONE));
            String time = com.kickass.android.autotrex.util.Utils.convertMillisecondToTime(timeDone);

//            Log.d(this, "_bindView() name = " + marketName + "-buy = " + buy + "-sell = " + sell + "-td = " + time);

            double total = buy + sell;

            TextView txtName = holder.getView(IndexViewHolder.MH_MARKET_NAME, TextView.class);
            if (txtName != null) {
                txtName.setText(marketName);
            }

            TextView txtBase24hVolume = holder.getView(IndexViewHolder.MH_BASE_24H_VOLUME, TextView.class);
            if (txtBase24hVolume != null) {
                MarketItem item = MarketsMgr.getInstance().getHotItem(ExchangeOperationMgr.ExchangeType.Bittrex, marketName);
                if (item != null && item.getBase24hVolume() > 0) {
                    txtBase24hVolume.setText(String.format("%.2f", item.getBase24hVolume()));
                } else {
                    txtBase24hVolume.setText("-");
                }
            }

            TextView txtTotal = holder.getView(IndexViewHolder.MH_TOTAL_VOLUME, TextView.class);
            if (txtTotal != null) {
                txtTotal.setText(String.format("%.2f", total));
            }

            //get last price
            String patternDigit = "'0'#.'00'%";
            String patternNumber = "##.'00'%";
            DecimalFormat formatter;
            double ratio;

            TextView txtBuy = holder.getView(IndexViewHolder.MH_BUY_VOLUME, TextView.class);
            if (txtBuy != null) {
                ratio = buy / total;
                formatter = (0.095 > ratio || ratio < 0.1) ? new DecimalFormat(patternDigit) : new DecimalFormat(patternNumber);
                txtBuy.setText(formatter.format(ratio));
            }

            TextView txtSell = holder.getView(IndexViewHolder.MH_SELL_VOLUME, TextView.class);
            if (txtSell != null) {
                ratio = sell / total;
                formatter = (0.095 > ratio || ratio < 0.1) ? new DecimalFormat(patternDigit) : new DecimalFormat(patternNumber);
                txtSell.setText(formatter.format(ratio));
            }

            TextView txtLastTimeDone = holder.getView(IndexViewHolder.MH_MIN_START_DONE, TextView.class);
            if (txtLastTimeDone != null) {
                txtLastTimeDone.setText(time);
            }
        } catch (IllegalStateException e) {
            Log.e(this, "_bindView IllegalStateException:" + e.toString());
        }
    }
}
