package com.kickass.android.autotrex.abstraction.account;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;

import com.kickass.android.autotrex.dialog.AddKeyDialog;

/**
 * Created by dat.ph on 1/17/2018.
 */

public abstract class AbsExchangeInfo {
    private static final String KEY_DIALOG_TAG = "addKeyDialog";
    protected static boolean sIsAddingKey;
    private Context mContext;

    public AbsExchangeInfo(Context context) {
        mContext = context;
    }

    protected Context getContext() {
        return mContext;
    }

    public void addKey(Activity activity, AddKeyDialog.OnSavedAPIKeyListener listener) {
        showAddKeyDialog(activity, listener);
        sIsAddingKey = true;
    }

    protected void showAddKeyDialog(Activity activity, AddKeyDialog.OnSavedAPIKeyListener listener) {
        FragmentManager fm = activity.getFragmentManager();
        AddKeyDialog dialog = AddKeyDialog.createInstance("Bittrex", listener);
        if (fm != null) {
            FragmentTransaction ft = fm.beginTransaction();
            Fragment prev = fm.findFragmentByTag(KEY_DIALOG_TAG);
            if (prev != null) {
                ft.remove(prev);
            }

            if (ft != null) {
                ft.add(dialog, KEY_DIALOG_TAG);
                ft.commitAllowingStateLoss();
            }
        }
    }
}
