package com.kickass.android.autotrex.abstraction.database.table;

import android.database.sqlite.SQLiteDatabase;

import com.kickass.android.autotrex.log.Log;

import java.util.ArrayList;

/**
 * Created by dat.ph on 1/17/2018.
 */

public abstract class AbsExchangeTable {
    public static class ColumnFormat {
        protected String name;
        protected String type;

        public ColumnFormat(String _name, String _type) {
            name = _name;
            type = _type;
        }
    }

    public AbsExchangeTable() {
        initTable();
    }

    public static class BaseColumns {
        public static final String ID = "_id";
    }

    protected ArrayList<ColumnFormat> mColumns = new ArrayList<>();

    public void createTable(SQLiteDatabase db) {
        StringBuilder strQuery = new StringBuilder("CREATE TABLE IF NOT EXISTS " + getTableName() + " (");

        for (ColumnFormat marketColumn : mColumns) {
            strQuery.append(marketColumn.name);
            strQuery.append(" ");
            strQuery.append(marketColumn.type);
        }
        strQuery.append(" ); ");
        db.execSQL(strQuery.toString());
    }

    public void dropTable(SQLiteDatabase db) {
        StringBuilder strQuery = new StringBuilder("DROP TABLE IF EXISTS " + getTableName() + ";");
        Log.d(this, "dropTable: " + strQuery.toString());
        db.execSQL(strQuery.toString());
    }

    public abstract String getTableName();
    public abstract void initTable();
}
