package com.kickass.android.autotrex.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class PreferenceUtils {
    private static final String API_KEY = "api_key";
    private static final String SECRET_KEY = "secret_key";

    public static void setAPIKey(Context context, String key) {
        SharedPreferences.Editor e = getDefaultSharedPreferences(context).edit();
        e.putString(API_KEY, key);
        e.apply();
    }

    public static String getAPIKey(Context context) {
        return getDefaultSharedPreferences(context).getString(API_KEY, null);
    }

    public static void setSecretKey(Context context, String secretKey) {
        SharedPreferences.Editor e = getDefaultSharedPreferences(context).edit();
        e.putString(SECRET_KEY, secretKey);
        e.apply();
    }

    public static String getSecretKey(Context context) {
        return getDefaultSharedPreferences(context).getString(SECRET_KEY, null);
    }
}
