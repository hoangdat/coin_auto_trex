package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import org.json.JSONObject;

/**
 * Created by dat.ph on 12/7/2017.
 */

public class GetMarketSumRequest extends AbsBittrexRequest<MarketSummary> {
    public GetMarketSumRequest(String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
    }

    public static GetMarketSumRequest createInstance(String market, Response.Listener<JSONObject> listener, Response.ErrorListener err) {
        String url = makeUrl(market);
        return new GetMarketSumRequest(url, false, null, listener, err);
    }

    private static String makeUrl(String market) {
        return addParameter(BittrexConstant.ENDPOINT + Constant.SPLASH +
                BittrexConstant.VER_1_1 + Constant.SPLASH +
                BittrexConstant.PUBLIC_API + Constant.SPLASH +
                BittrexConstant.GET_SUM + Constant.QUESTION_MARK +
                BittrexConstant.MARKET_API + Constant.EQUAL_SIGN + market, false);
    }

    @Override
    protected MarketSummary parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        return null;
    }
}
