package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.Market;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import org.json.JSONObject;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class GetMarketRequest extends AbsBittrexRequest<Market> {
    public GetMarketRequest(String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
    }

    public static GetMarketRequest getInstance(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl();
        return new GetMarketRequest(url, false, null, listener, errorListener);
    }

    public static String makeUrl() {
        return addParameter(BittrexConstant.ENDPOINT + Constant.SPLASH +
               BittrexConstant.VER_1_1 + Constant.SPLASH +
               BittrexConstant.PUBLIC_API + Constant.SPLASH +
               BittrexConstant.GET_MARKETS, false);
    }

    @Override
    protected Market parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        return null;
    }
}
