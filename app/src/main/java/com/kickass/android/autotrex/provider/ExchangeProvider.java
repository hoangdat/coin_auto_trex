package com.kickass.android.autotrex.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kickass.android.autotrex.abstraction.database.AbsExchangeDbHelper;
import com.kickass.android.autotrex.abstraction.database.table.BuySellPointTable;
import com.kickass.android.autotrex.abstraction.database.table.MarketHistoryTable;
import com.kickass.android.autotrex.abstraction.database.table.TransactionTable;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.bittrex.database.BittrexDBHelper;
import com.kickass.android.autotrex.log.Log;

import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 12/29/2017.
 */

public class ExchangeProvider extends ContentProvider {
    public final static int DATABASE_VERSION = 1;

    public static class UriString {
        public static final String EXCHANGE_AUTHORITY = "auto_trex_exchange";
        public static final String MARKET_HISTORY_LIST_URI_PATH = "market_history_list";
        public static final String MARKET_HISTORY_URI_PATH = "market_history";
        public static final String MARKET_HISTORY_LIST_VOLUME_PATH = "market_history_list_volume";
        public static final String TRANSACTION_LIST_URI_PATH = "transaction_list";
        public static final String TRANSACTION_URI_PATH = "transaction_order";
        public static final String BUY_SELL_URI_PATH = "buy_sell_uri";

        public static Uri getMarketHistoryListUri(ExchangeOperationMgr.ExchangeType exchangeType, String market) {
            return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + EXCHANGE_AUTHORITY + "/"
                    + MARKET_HISTORY_LIST_URI_PATH + "/" + exchangeType + "/" + market);
        }

        public static Uri getMarketHistoryListVolumeUri(ExchangeOperationMgr.ExchangeType exchangeType) {
            return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + EXCHANGE_AUTHORITY + "/"
                    + MARKET_HISTORY_LIST_VOLUME_PATH + "/" + exchangeType);
        }

        public static Uri getMarketHistoryUri(ExchangeOperationMgr.ExchangeType exchangeType) {
            return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + EXCHANGE_AUTHORITY + "/"
                    + MARKET_HISTORY_URI_PATH + "/" + exchangeType);
        }

        public static Uri getTransactionListUri(ExchangeOperationMgr.ExchangeType exchangeType, String marketName) {
            return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + EXCHANGE_AUTHORITY + "/"
                    + TRANSACTION_LIST_URI_PATH + "/" + exchangeType + "/" + marketName);
        }

        public static Uri getTransactionUri(ExchangeOperationMgr.ExchangeType exchangeType) {
            return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + EXCHANGE_AUTHORITY + "/"
                    + TRANSACTION_URI_PATH + "/" + exchangeType);
        }

        public static Uri getBuySellUri(ExchangeOperationMgr.ExchangeType exchangeType) {
            return Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + EXCHANGE_AUTHORITY + "/"
                    + BUY_SELL_URI_PATH + "/" + exchangeType);
        }
    }

    private static UriMatcher URI_MATCHER;
//    private static ExchangeAccountDBHelper sAccountDBHelper;

    enum PatternType {
        MARKET_HISTORY_PATTERN(1),
        MARKET_HISTORY_LIST_PATTERN(2),
        MARKET_HISTORY_LIST_VOLUME_PATTERN(3),
        TRANSACTION_LIST_PATTERN(4),
        TRANSACTION_PATTERN(5),
        BUY_SELL_POINT_PATTERN(6);

        private int mValue;

        PatternType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }

        public static PatternType fromInt(int i) {
            for (PatternType patternType : PatternType.values()) {
                if (patternType.getValue() == i)
                    return patternType;
            }
            return null;
        }
    }

    enum PathType {
        EXCHANGE_TYPE_PATH_SEGMENT(1),
        MARKET_PATH_SEGMENT(2);

        private int mValue;

        PathType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }
    }

    private static HashMap<ExchangeOperationMgr.ExchangeType, AbsExchangeDbHelper> sExchangeDBList = new HashMap<>();

    private void init() {
        sExchangeDBList.put(ExchangeOperationMgr.ExchangeType.Bittrex, BittrexDBHelper.getInstance(getContext()));

        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(UriString.EXCHANGE_AUTHORITY, UriString.MARKET_HISTORY_URI_PATH + "/*", PatternType.MARKET_HISTORY_PATTERN.getValue());
        URI_MATCHER.addURI(UriString.EXCHANGE_AUTHORITY, UriString.MARKET_HISTORY_LIST_URI_PATH + "/*", PatternType.MARKET_HISTORY_LIST_PATTERN.getValue());
        URI_MATCHER.addURI(UriString.EXCHANGE_AUTHORITY, UriString.MARKET_HISTORY_LIST_VOLUME_PATH + "/*", PatternType.MARKET_HISTORY_LIST_VOLUME_PATTERN.getValue());

        URI_MATCHER.addURI(UriString.EXCHANGE_AUTHORITY, UriString.TRANSACTION_URI_PATH + "/*", PatternType.TRANSACTION_PATTERN.getValue());
        URI_MATCHER.addURI(UriString.EXCHANGE_AUTHORITY, UriString.TRANSACTION_LIST_URI_PATH + "/*", PatternType.TRANSACTION_LIST_PATTERN.getValue());

        URI_MATCHER.addURI(UriString.EXCHANGE_AUTHORITY, UriString.BUY_SELL_URI_PATH + "/*", PatternType.BUY_SELL_POINT_PATTERN.getValue());
    }

    private String getDbName(PatternType patternType) {
        String table = null;
        switch (patternType) {
            case MARKET_HISTORY_LIST_PATTERN:
            case MARKET_HISTORY_PATTERN:
            case MARKET_HISTORY_LIST_VOLUME_PATTERN:
                table = MarketHistoryTable.TABLE_NAME;
                break;
            case TRANSACTION_LIST_PATTERN:
            case TRANSACTION_PATTERN:
                table = TransactionTable.TABLE_NAME;
                break;
            case BUY_SELL_POINT_PATTERN:
                table = BuySellPointTable.TABLE_NAME;
                break;
            default:
                break;
        }
        return table;
    }

    public static AbsExchangeDbHelper getDbHelper(List<String> pathSegments) {
        ExchangeOperationMgr.ExchangeType exchangeType = (pathSegments.size() <= 1) ?
                ExchangeOperationMgr.ExchangeType.None : ExchangeOperationMgr.ExchangeType.valueOf(pathSegments.get(PathType.EXCHANGE_TYPE_PATH_SEGMENT.getValue()));

        AbsExchangeDbHelper dbHelper = null;
        switch (exchangeType) {
            case Bittrex:
                dbHelper = sExchangeDBList.get(exchangeType);
                break;
            default:
                break;
        }
        return dbHelper;
    }

    @Override
    public boolean onCreate() {
        Log.d(this, "onCreate() ---------- provider");
        init();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
//        Log.d(this, "query() uri = " + uri);
        int patternId = URI_MATCHER.match(uri);
        PatternType patternType = PatternType.fromInt(patternId);
        if (patternType == null) {
            Log.e(this, "query() - patternType == null");
            return null;
        }

//        Log.d(this, "query() patter = " + patternType);
        List<String> pathSegments = uri.getPathSegments();
        AbsExchangeDbHelper dbHelper = getDbHelper(pathSegments);
        String tableName = getDbName(patternType);
        if (dbHelper == null || tableName == null) {
            Log.e(this, "query() - dbHelper == null, uri = " + uri);
            return null;
        }
//        Log.d(this, "query() dbName = " + tableName);
        String additionalSelection;
        String groupBy;

        switch (patternType) {
            case MARKET_HISTORY_PATTERN:
                additionalSelection = null;
                groupBy = null;
                break;
            case MARKET_HISTORY_LIST_PATTERN:
                String market = pathSegments.get(PathType.MARKET_PATH_SEGMENT.getValue());
                additionalSelection = MarketHistoryTable.MarketHistoryColumns.MARKET_NAME + "='" + market + "'";
                groupBy = null;
                break;
            case MARKET_HISTORY_LIST_VOLUME_PATTERN:
                groupBy = MarketHistoryTable.MarketHistoryColumns.MARKET_NAME;
                additionalSelection = null;
                break;
            case TRANSACTION_LIST_PATTERN:
                String marketName = pathSegments.get(PathType.MARKET_PATH_SEGMENT.getValue());
                additionalSelection = MarketHistoryTable.MarketHistoryColumns.MARKET_NAME + "='" + marketName + "'";
                groupBy = null;
                break;
            case TRANSACTION_PATTERN:
                additionalSelection = null;
                groupBy = null;
                break;
            default:
                Log.e(this, "query this can not be handle");
                return null;
        }

        if (additionalSelection != null) {
            selection = (selection == null) ? additionalSelection : additionalSelection + " AND " + selection;
        }
//        Log.d(this, "query() selection = " + selection);
        Cursor c = dbHelper.query(tableName, projection, selection, selectionArgs, groupBy, null, sortOrder, null);
        if (patternType != PatternType.MARKET_HISTORY_PATTERN ) {
            c.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        int patternId = URI_MATCHER.match(uri);
        PatternType patternType = PatternType.fromInt(patternId);
        if (patternType == null) {
            Log.e(this, "insert() - patternType == null");
            return null;
        }
        List<String> pathSegments = uri.getPathSegments();
        AbsExchangeDbHelper dbHelper = getDbHelper(pathSegments);
        String tableName = getDbName(patternType);
        if (dbHelper == null || tableName == null) {
            Log.e(this, "insert() - dbHelper == null, uri = " + uri);
            return null;
        }
        long rowId = dbHelper.insert(tableName, contentValues);
        return ContentUris.withAppendedId(uri, rowId);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        Log.d(this, "delete() uri = " + uri + "-selection = " + selection);
        if (selectionArgs != null && selectionArgs.length > 0) {
            Log.d(this, "delete() args = " + selectionArgs[0]);
        }
        int patternId = URI_MATCHER.match(uri);
        PatternType patternType = PatternType.fromInt(patternId);
        if (patternType == null) {
            Log.e(this, "delete() - patternType == null");
            return 0;
        }
        List<String> pathSegments = uri.getPathSegments();
        AbsExchangeDbHelper dbHelper = getDbHelper(pathSegments);
        String tableName = getDbName(patternType);
        if (dbHelper == null || tableName == null) {
            Log.e(this, "delete() - dbHelper == null, uri = " + uri);
            return 0;
        }
        String additionalSelection;

        switch (patternType) {
            case TRANSACTION_PATTERN:
            case MARKET_HISTORY_PATTERN:
                additionalSelection = null;
                break;
            case MARKET_HISTORY_LIST_PATTERN:
                String market = pathSegments.get(PathType.MARKET_PATH_SEGMENT.getValue());
                additionalSelection = MarketHistoryTable.MarketHistoryColumns.MARKET_NAME + "='" + market + "'";
                break;
            default:
                Log.e(this, "delete() this can not be handle");
                return 0;
        }
        if (additionalSelection != null) {
            selection = (selection == null) ? additionalSelection : additionalSelection + " AND (" + selection + ")";
        }


        int ret = dbHelper.delete(tableName, selection, selectionArgs);
        Log.d(this, "delete() - selection = " + selection + "-ret = " + ret);
        return ret;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
        int patternId = URI_MATCHER.match(uri);
        PatternType patternType = PatternType.fromInt(patternId);
        if (patternType == null) {
            Log.e(this, "update() - patternType == null");
            return 0;
        }
        List<String> pathSegments = uri.getPathSegments();
        AbsExchangeDbHelper dbHelper = getDbHelper(pathSegments);
        String tableName = getDbName(patternType);
        if (dbHelper == null || tableName == null) {
            Log.e(this, "query() - dbHelper == null, uri = " + uri);
            return 0;
        }
        String additionalSelection;

        switch (patternType) {
            case TRANSACTION_PATTERN:
            case MARKET_HISTORY_PATTERN:
                additionalSelection = null;
                break;
            case MARKET_HISTORY_LIST_PATTERN:
                String market = pathSegments.get(PathType.MARKET_PATH_SEGMENT.getValue());
                additionalSelection = MarketHistoryTable.MarketHistoryColumns.MARKET_NAME + "='" + market + "'";
                break;
            default:
                Log.e(this, "update() this can not be handle");
                return 0;
        }
        if (additionalSelection != null) {
            selection = (selection == null) ? additionalSelection : additionalSelection + " AND (" + selection + ")";
        }

        return dbHelper.update(tableName, contentValues, selection, selectionArgs);
    }
}
