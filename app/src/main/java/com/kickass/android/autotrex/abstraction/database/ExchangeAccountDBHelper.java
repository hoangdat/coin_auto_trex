package com.kickass.android.autotrex.abstraction.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteFullException;

import com.kickass.android.autotrex.abstraction.database.table.AbsExchangeTable;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.provider.ExchangeProvider;

import java.util.ArrayList;

/**
 * Created by dat.ph on 1/2/2018.
 */

public class ExchangeAccountDBHelper extends AbsExchangeDbHelper {
    public ExchangeAccountDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
//    private final static String TABLE_NAME = "account";
//
//    public static class AccountColumns {
//        public static final String ID = "_id";
//        public static final String NAME = "_ex_name";
//        public static final String KEY = "_key";
//        public static final String SECRET_KEY = "_sec_key";
//    }
//
//    protected ArrayList<AbsExchangeTable.ColumnFormat> mAccountColumns = new ArrayList<>();
//
//    public ExchangeAccountDBHelper(Context context) {
//        super(context, TABLE_NAME + ".db", null, ExchangeProvider.DATABASE_VERSION);
//        init();
//    }
//
//    private void init() {
//        mAccountColumns.add(new AbsExchangeTable.ColumnFormat(AccountColumns.ID, "INTEGER PRIMARY KEY AUTOINCREMENT,"));
//        mAccountColumns.add(new AbsExchangeTable.ColumnFormat(AccountColumns.NAME, "TEXT,"));
//        mAccountColumns.add(new AbsExchangeTable.ColumnFormat(AccountColumns.KEY, "TEXT,"));
//        mAccountColumns.add(new AbsExchangeTable.ColumnFormat(AccountColumns.SECRET_KEY, "TEXT"));
//    }
//
//    @Override
//    public String getTableName() {
//        return TABLE_NAME;
//    }
//
//    @Override
//    public void onCreate(SQLiteDatabase sqLiteDatabase) {
//        createTable(sqLiteDatabase);
//        initTable(sqLiteDatabase);
//    }
//
//    private void createTable(SQLiteDatabase db) {
//        StringBuilder strQuery = new StringBuilder("DROP TABLE IF EXISTS " + TABLE_NAME);
//        db.execSQL(strQuery.toString());
//
//        strQuery = new StringBuilder("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (");
//        for (AbsExchangeTable.ColumnFormat accountColumn : mAccountColumns) {
//            strQuery.append(accountColumn.name);
//            strQuery.append(" ");
//            strQuery.append(accountColumn.type);
//        }
//        strQuery.append(" ) ");
//        db.execSQL(strQuery.toString());
//    }
//
//    private void initTable(SQLiteDatabase db) {
//        try {
//            db.beginTransaction();
//
//            for (ExchangeOperationMgr.ExchangeType exchangeType : ExchangeOperationMgr.ExchangeType.values()) {
//                if (exchangeType != ExchangeOperationMgr.ExchangeType.None) {
//                    db.execSQL("INSERT INTO " + TABLE_NAME + "( _ex_name ) VALUES ('" + exchangeType.name() + "')");
//                }
//            }
//            db.setTransactionSuccessful();
//        } catch (SQLiteFullException e) {
//            Log.e(this, "SQLiteFullException:" + e.toString());
//        } finally {
//            db.endTransaction();
//        }
//    }
//
//    @Override
//    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
//
//    }
}
