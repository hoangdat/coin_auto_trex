package com.kickass.android.autotrex.abstraction.response;

import android.text.TextUtils;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class PriceHistory {
    public enum OrderType {
        Buy("BUY"),
        Sell("SELL"),
        None(null);

        private String mValue;

        OrderType(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        public static OrderType fromString(String value) {
            for (OrderType orderType : OrderType.values()) {
                if (TextUtils.equals(orderType.getValue(), value)) {
                    return orderType;
                }
            }
            return null;
        }
    }

    private long mId;
    private double mQuantity;
    private double mPrice;
    private double mTotal;
    private OrderType mOrderType;
    private long mTimeStamp;
    private String mMarketName;

    public PriceHistory(String marketName, long mId, long timeStamp, double mQuantity, double mPrice, double mTotal, String mOrderType) {
        this.mMarketName = marketName;
        this.mId = mId;
        this.mTimeStamp = timeStamp;
        this.mQuantity = mQuantity;
        this.mPrice = mPrice;
        this.mTotal = mTotal;
        this.mOrderType = OrderType.fromString(mOrderType);
    }

    public long getHistoryId() {
        return mId;
    }

    public String getMarketName() {
        return mMarketName;
    }

    public double getQuantity() {
        return mQuantity;
    }

    public double getPrice() {
        return mPrice;
    }

    public double getTotal() {
        return mTotal;
    }

    public String getOrderType() {
        return mOrderType.getValue();
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }
}
