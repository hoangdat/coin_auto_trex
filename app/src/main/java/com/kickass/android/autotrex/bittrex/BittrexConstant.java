package com.kickass.android.autotrex.bittrex;

import com.kickass.android.autotrex.util.Constant;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class BittrexConstant {
    //support version v1.1
    public static final String ENDPOINT = "https://bittrex.com/api";
    public static final String VER_1_1 = "v1.1";

    public static final String ENDPOINT_V1_1 = ENDPOINT + Constant.SPLASH + VER_1_1 + Constant.SPLASH;

    public static final String ACCOUNT_API = "account";
    public static final String PUBLIC_API = "public";
    public static final String MARKET_API = "market";

    public static final String API_SIGN = "apisign";

    public static final String GET_MARKETS = "getmarkets";
    public static final String GET_BALANCE = "getbalance";
    public static final String GET_SUM = "getmarketsummary";
    public static final String GET_SUMMARIES = "getmarketsummaries";
    public static final String GET_BALANCES = "getbalances";
    public static final String BUY_LIMIT = "buylimit";
    public static final String SELL_LIMIT = "selllimit";
    public static final String GET_TICKER = "getticker";
    public static final String GET_MARKET_HISTORY = "getmarkethistory";
    public static final String GET_ORDER = "getorder";
    public static final String GET_OPEN_ORDERS = "getopenorders";
    public static final String GET_ORDER_HISTORY = "getorderhistory";

    public static final String CURRENCY = "currency";
    public static final String QUANTITY = "quantity";
    public static final String RATE = "rate";

    public static final String RESPONSE_RESULT = "result";
    public static final String RESPONSE_BID = "Bid";
    public static final String RESPONSE_ASK = "Ask";
    public static final String RESPONSE_LAST = "Last";
    public static final String RESPONSE_ID = "Id";
    public static final String RESPONSE_QUANTITY = "Quantity";
    public static final String RESPONSE_TIMESTAMP = "TimeStamp";
    public static final String RESPONSE_PRICE = "Price";
    public static final String RESPONSE_PRICE_PER_UNIT = "PricePerUnit";
    public static final String RESPONSE_TOTAL = "Total";
    public static final String RESPONSE_ORDER_TYPE = "OrderType";
    public static final String RESPONSE_MARKET_CUR = "MarketCurrency";
    public static final String RESPONSE_BASE_CUR = "BaseCurrency";
    public static final String RESPONSE_MARKET_NAME = "MarketName";
    public static final String RESPONSE_MIN_TRADE_SIZE = "MinTradeSize";
    public static final String RESPONSE_MARKET_CUR_LONG = "MarketCurrencyLong";
    public static final String RESPONSE_VOLUME = "Volume";
    public static final String RESPONSE_BASE_VOLUME = "BaseVolume";
    public static final String RESPONSE_UUID = "uuid";
    public static final String RESPONSE_ORDER_UUID = "OrderUuid";
    public static final String RESPONSE_ORDER_EXCHANGE = "Exchange";
    public static final String RESPONSE_TYPE = "Type";
    public static final String RESPONSE_QUANTITY_REMAINING = "QuantityRemaining";
    public static final String RESPONSE_TIME_CLOSE = "Closed";
    public static final String RESPONSE_ORDER_IS_OPEN = "IsOpen";

    public static final String RESPONSE_CURRENCY = "Currency";
    public static final String RESPONSE_BALANCE = "Balance";
    public static final String RESPONSE_AVAILABLE = "Available";
    public static final String RESPONSE_PENDING = "Pending";
    public static final String RESPONSE_ADDRESS_CRYPTO = "CryptoAddress";
}
