package com.kickass.android.autotrex.abstraction;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.kickass.android.autotrex.bittrex.request.AbsBittrexRequest;
import com.kickass.android.autotrex.bittrex.request.BuyLimitRequest;
import com.kickass.android.autotrex.bittrex.request.GetBalanceRequest;
import com.kickass.android.autotrex.bittrex.request.GetBalancesRequest;
import com.kickass.android.autotrex.bittrex.request.GetMarketHistoryRequest;
import com.kickass.android.autotrex.bittrex.request.GetMarketOrderHistoryRequest;
import com.kickass.android.autotrex.bittrex.request.GetMarketsRequest;
import com.kickass.android.autotrex.bittrex.request.GetMarketSumRequest;
import com.kickass.android.autotrex.bittrex.request.GetOpenOrdersRequest;
import com.kickass.android.autotrex.bittrex.request.GetOrderRequest;
import com.kickass.android.autotrex.bittrex.request.GetSummariesRequest;
import com.kickass.android.autotrex.bittrex.request.GetTickerRequest;
import com.kickass.android.autotrex.bittrex.request.SellLimitRequest;

import org.json.JSONObject;

/**
 * Created by dat.ph on 8/1/2017.
 */

public class AutoTrexRequest {
    private static AutoTrexRequest sInstance;
    private Context mContext;
    private RequestQueue mRequestQueue;

    public enum RequestType {
        GET_MARKETS,
        GET_MARKET_SUM,
        GET_MARKET_SUMMARIES,
        GET_BALANCES,
        GET_BALANCE,
        BUY_LIMIT,
        SELL_LIMIT,
        CANCEL_ORDER,
        GET_TICKER,
        GET_MARKET_HISTORY,
        GET_ORDER,
        GET_OPEN_ORDERS,
        GET_MARKET_ORDER_HISTORY,
    }

    public static AutoTrexRequest getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new AutoTrexRequest(context);
        }
        return sInstance;
    }

    private AutoTrexRequest(Context context) {
        mContext = context;
        getRequestQueue();
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


//    public void cancelPendingRequests(final Object tag) {
//        mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
//            @Override
//            public boolean apply(Request<?> request) {
//                if (tag == request.getTag() ||
//                        tag == AutoTrexRequest.RequestType.CANCEL_ALL) {
//                    request.cancel();
//                }
//                return true;
//            }
//        });
//    }

    public AbsBittrexRequest getRequest(RequestType type, String... args) {
        AbsBittrexRequest request = null;
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        switch (type) {
            case GET_MARKETS:
                request = GetMarketsRequest.getInstance(future, future);
                break;
            case GET_MARKET_SUM:
                request = GetMarketSumRequest.createInstance(args[0], future, future);
                break;
            case GET_MARKET_SUMMARIES:
                request = GetSummariesRequest.createInstance(future, future);
                break;
            case GET_BALANCES:
                request = GetBalancesRequest.createInstance(future, future);
                break;
            case GET_BALANCE:
                request = GetBalanceRequest.createInstance(args[0], future, future);
                break;
            case BUY_LIMIT:
                request = BuyLimitRequest.createInstance(args[0], args[1], args[2], future, future);
                break;
            case SELL_LIMIT:
                request = SellLimitRequest.createInstance(args[0], args[1], args[2], future, future);
                break;
            case GET_TICKER:
                request = GetTickerRequest.createInstance(args[0], future, future);
                break;
            case GET_MARKET_HISTORY:
                request = GetMarketHistoryRequest.createInstance(args[0], future, future);
                break;
            case GET_ORDER:
                request = GetOrderRequest.createInstance(args[0], future, future);
                break;
            case GET_OPEN_ORDERS:
                request = GetOpenOrdersRequest.createInstance(future, future);
                break;
            case GET_MARKET_ORDER_HISTORY:
                request = GetMarketOrderHistoryRequest.createInstance(args[0], future, future);
                break;
        }

        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        );
        request.setRetryPolicy(retryPolicy);
        request.setTag(type);
        return request;
    }
}
