package com.kickass.android.autotrex.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.abstraction.ExchangeAsyncAPI;
import com.kickass.android.autotrex.abstraction.IResponseAPI;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.Balance;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.PriceHistory;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.bittrex.BittrexInfo;
import com.kickass.android.autotrex.dialog.AddKeyDialog;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements AddKeyDialog.OnSavedAPIKeyListener, View.OnClickListener,
        IResponseAPI.OnGetBalancesListener, IResponseAPI.OnGetTickerListener, IResponseAPI.OnGetMarketHistory {
    private static final int UPDATE_TICKER_MSG = 1000;

    private SearchView mSearchView;
    private MarketItem mCurrentMarketItem = null;
    private ScheduledExecutorService mScheduledExecutorService;
    private ScheduledFuture mScheduledFuture;
    private UiUpdateHandler mUiUpdateHandler = null;
    private TextView mTxtMarketName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(this, "onCreate: " + getIntent());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        mScheduledExecutorService = Executors.newScheduledThreadPool(1);
    }

    private void initView() {
        SearchManager searchMgr = (SearchManager) getSystemService(SEARCH_SERVICE);
        mSearchView = (SearchView) findViewById(R.id.autocomplete_searchview);
        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setDropDownHeight(getResources().getDisplayMetrics().heightPixels/3);
        mSearchView.setSearchableInfo(searchMgr.getSearchableInfo(getComponentName()));
        mSearchView.setIconifiedByDefault(false);

        mTxtMarketName = (TextView) findViewById(R.id.txtMarketName);
        TextView txtLast = (TextView) findViewById(R.id.txtLast);
        TextView txtAsk = (TextView) findViewById(R.id.txtAsk);
        TextView txtBid = (TextView) findViewById(R.id.txtBid);
        mUiUpdateHandler = new UiUpdateHandler(txtLast, txtAsk, txtBid);
    }

    @Override
    protected void onResume() {
        BittrexInfo info = BittrexInfo.getInstance(getApplicationContext());
        if (!info.isHasKey() && !BittrexInfo.isAddingKey()) {
            info.addKey(this, this);
        } else {
            info.reloadKey();
        }
        super.onResume();
    }

    @Override
    public void onSavedSuccess(String api, String secret) {
        Log.d(this, "onSavedSuccess: api = " + api + "-s = " + secret);
        BittrexInfo.setKey(api, secret);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
        }
    }

    @Override
    public void onGetBalances(ExchangeOperationMgr.ExchangeType exchangeType, ArrayList<Balance> balances) {

    }

    @Override
    public void onGetBalance(ExchangeOperationMgr.ExchangeType exchangeType, String currency, Balance balance) {
        Log.d(this, "onGetBalance: " + balance);
    }

    @Override
    protected void onNewIntent(Intent intent) {
//        Log.d(this, "onNewIntent: " + intent + "-data = " + intent.getDataString() + mScheduledExecutorService);
//        String intentAction = intent.getAction();
//        String extras = intent.getExtras().getString(SearchManager.EXTRA_DATA_KEY);
//        String[] itemValue = extras.split("\\^");
//        Log.d(this, "onNewIntent extras: " + extras + "-sz:" + itemValue.length);
//        if (TextUtils.equals(Constant.SUGGESTION_INTENT_ACTION, intentAction) && itemValue != null && itemValue.length == 4) {
//            String newMarket = itemValue[0];
//            String newCur = itemValue[1];
//            String newBasedMarket = itemValue[3];
//            String newCurLong = itemValue[2];
//            Log.d(this, "onNewIntent: " + newMarket + "-" + newCur + "-" + newBasedMarket + "-" + newCurLong);
//            MarketItem newItem = new MarketItem(newMarket, newCur, newCurLong, newBasedMarket);
//
//            if (mCurrentMarketItem == null || newItem != mCurrentMarketItem) {
//                mCurrentMarketItem = newItem;
//
//                mTxtMarketName.setText(mCurrentMarketItem.getMarketName());
//
//                if (mUiUpdateHandler != null) {
//                    mUiUpdateHandler.setMarketItem(mCurrentMarketItem);
//                }
//
//                if (mScheduledFuture != null) {
//                    if (!mScheduledFuture.isDone() || !mScheduledFuture.isCancelled()) {
//                        Log.d(this, "onNewIntent: cancel");
//                        mScheduledFuture.cancel(true);
//                    }
//                }
//                mScheduledFuture = mScheduledExecutorService.scheduleAtFixedRate(
//                        new WorkerUpdateMarketInfo(this), 0, 20, TimeUnit.SECONDS);
//                Log.d(this, "onNewIntent: submitted");
//            }
////        }
        super.onNewIntent(intent);
    }

    @Override
    protected void onDestroy() {
        Log.d(this, "onDestroy");
        if (mScheduledFuture != null) {
            mScheduledFuture.cancel(true);
            mScheduledFuture = null;
        }
        if (mScheduledExecutorService != null) {
            Log.d(this, "onDestroy set null");
            mScheduledExecutorService.shutdownNow();
            mScheduledExecutorService = null;
        }
        super.onDestroy();
    }

    @Override
    public void onGetTickerDone(ExchangeOperationMgr.ExchangeType exchangeType, String market, Ticker ticker) {
        if (TextUtils.equals(mCurrentMarketItem.getMarketName(), market)) {
            mUiUpdateHandler.sendMessage(Message.obtain(mUiUpdateHandler, UPDATE_TICKER_MSG, ticker));
        }
    }

    @Override
    public void onGetMarketHistoryDone(ExchangeOperationMgr.ExchangeType exchangeType, String market, ArrayList<PriceHistory> priceHistory) {
        Log.d(this, "onGetMarketHistoryDone: " + market);
    }

    private static class WorkerUpdateMarketInfo implements Runnable {

        private WeakReference<MainActivity> mRef;
        public WorkerUpdateMarketInfo(MainActivity activity) {
            mRef = new WeakReference<>(activity);
        }

        @Override
        public void run() {
            Log.d(this, "run");
            MainActivity activity = mRef.get();
            if (activity != null && activity.mCurrentMarketItem != null) {
                MarketItem marketItem = activity.mCurrentMarketItem;
                if (marketItem != null) {
                    ExchangeAsyncAPI.getInstance(activity.getApplicationContext()).getTicker(ExchangeOperationMgr.ExchangeType.Bittrex, marketItem.getMarketName(), activity); //6 sec
//                    ExchangeAsyncAPI.getInstance(activity.getApplicationContext()).getMarketHistory(ExchangeOperationMgr.ExchangeType.Bittrex, marketItem.getMarketName(), activity); //30 sec
                }
            }
        }
    }

    private static class UiUpdateHandler extends Handler {
        private MarketItem mMarketItem;
        private final WeakReference<TextView> mTxtLastPrice;
        private final WeakReference<TextView> mTxtAsk;
        private final WeakReference<TextView> mTxtBid;

        private HashMap<Integer, Integer> mViewSize = new HashMap<>();

        UiUpdateHandler(TextView txtLast, TextView txtAsk, TextView txtBid) {
            mTxtLastPrice = new WeakReference<>(txtLast);
            mTxtAsk = new WeakReference<>(txtAsk);
            mTxtBid = new WeakReference<>(txtBid);

            mViewSize.put(UPDATE_TICKER_MSG, 3);
        }

        @Override
        public void handleMessage(Message msg) {
            int what = msg.what;
            switch (what) {
                case UPDATE_TICKER_MSG:
                    Ticker ticker = (Ticker) msg.obj;
                    updateTicker(ticker);
                    break;
            }
        }

        private void updateTicker(Ticker ticker) {
            if (ticker == null) return;
            TextView txtLast = mTxtLastPrice.get();
            TextView txtAsk = mTxtAsk.get();
            TextView txtBid = mTxtBid.get();

            if (txtLast != null) txtLast.setText(Double.toString(ticker.getLast()));
            if (txtAsk != null) txtAsk.setText(Double.toString(ticker.getAsk()));
            if (txtBid != null) txtBid.setText(Double.toString(ticker.getBid()));
        }

        public void setMarketItem(MarketItem marketItem) {
            mMarketItem = marketItem;
        }
    }

    public MarketItem getCurrentMarket() {
        return mCurrentMarketItem;
    }
}
