package com.kickass.android.autotrex.abstraction.transaction;

import android.content.Context;
import android.os.*;

import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.obsevable.Observable;
import com.kickass.android.autotrex.obsevable.Observer;
import com.kickass.android.autotrex.service.filter.data.TransactionDataFilter;
import com.kickass.android.autotrex.service.worker.WorkerMonitorOrder;
import com.kickass.android.autotrex.service.worker.WorkerMonitorToSell;
import com.kickass.android.autotrex.util.JUnitHandler;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by dat.ph on 1/23/2018.
 */
public class MonitorMgr implements Observer {
    private static final int PERIOD_MONITOR_ORDER_SECONDS   = 50;
    private static final int PERIOD_MONITOR_TO_SELL_SECONDS = 50;

    private static MonitorMgr sInstance;
    private static final String MONITOR_THREAD_NAME = "monitor_thread";
    private HashMap<String, TransactionDataFilter> mMapUUIDOrder = new HashMap<>();
    private HashMap<String, TransactionDataFilter> mMapUUIDSell = new HashMap<>();

    private MonitorHandler mMonitorHandler;
    private HandlerThread mMonitorThread;
    private static final int MONITOR_ORDER = 1;
    private static final int MONITOR_TO_SELL = 2;

    private Context mContext;

    private MonitorMgr(Context context) {
        mContext = context;
        Observable.getInstance().addObserver(Observable.ObservableType.TRANSACTION, this);
        mMonitorThread = new HandlerThread(MONITOR_THREAD_NAME, android.os.Process.THREAD_PRIORITY_BACKGROUND);
        mMonitorThread.start();

        Looper looper = mMonitorThread.getLooper();
        if (looper != null) {
            mMonitorHandler = new MonitorHandler(looper, this);
        }
    }

    public synchronized static MonitorMgr getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new MonitorMgr(context);
        }
        return sInstance;
    }

    @Override
    public void update(Observable o, Observable.ObservableType obType, Object arg) {
        switch (obType) {
            case TRANSACTION:
                onTransactionNotify((TransactionDataFilter) arg);
                break;
            default:
                break;
        }
    }

    private void addUUIDSell(String uuid, TransactionDataFilter dataFilter) {
        synchronized (mMapUUIDSell) {
            mMapUUIDSell.put(uuid, dataFilter);
        }
    }

    public void clearUUIDSell(String uuid) {
        synchronized (mMapUUIDSell) {
            mMapUUIDSell.remove(uuid);
        }
    }

    public HashMap<String, TransactionDataFilter> getMapUUIDSell() {
        HashMap<String, TransactionDataFilter> ret = null;
        synchronized (mMapUUIDSell) {
            ret = mMapUUIDSell;
        }
        return ret;
    }

    private void addUUIDOrder(String uuid, TransactionDataFilter dataFilter) {
        synchronized (mMapUUIDOrder) {
            mMapUUIDOrder.put(uuid, dataFilter);
        }
    }

    public void clearUUIDOrder(String uuid) {
        synchronized (mMapUUIDOrder) {
            mMapUUIDOrder.remove(uuid);
        }
    }

    public HashMap<String, TransactionDataFilter> getMapUUIDOrder() {
        HashMap<String, TransactionDataFilter> ret = null;
        synchronized (mMapUUIDOrder) {
            ret = mMapUUIDOrder;
        }
        return ret;
    }

    private void onTransactionNotify(TransactionDataFilter dataFilter) {
        Log.d(this, "onTransactionNotify");
        ExchangeOperationMgr.ActionType actionType = dataFilter.getActionType();
        if (dataFilter != null && dataFilter.getOrderInfo() != null) {
            Log.d(this, "onTransactionNotify act " + actionType);
            switch (actionType) {
                case Monitor_to_Bought:
                    String uuidBought = dataFilter.getOrderInfo().getBuyUuid();
                    if (uuidBought == null) {
                        return;
                    }
                    Log.d(this, "onTransactionNotify uuid = " + uuidBought + "- act " + actionType);
                    addUUIDOrder(uuidBought, dataFilter);
                    mMonitorHandler.sendMessage(mMonitorHandler.obtainMessage(MONITOR_ORDER, dataFilter));
                    break;
                case Monitor_to_Sold:
                    String uuidSold = dataFilter.getOrderInfo().getSellUuid();
                    if (uuidSold == null) {
                        return;
                    }
                    Log.d(this, "onTransactionNotify uuid = " + uuidSold + "- act " + actionType);
                    addUUIDOrder(uuidSold, dataFilter);
                    mMonitorHandler.sendMessage(mMonitorHandler.obtainMessage(MONITOR_ORDER, dataFilter));
                    break;
                case Monitor_to_Sell:
                    String uuidSell = dataFilter.getOrderInfo().getBuyUuid();
                    if (uuidSell == null) {
                        return;
                    }
                    Log.d(this, "onTransactionNotify uuid = " + uuidSell + "- act " + actionType);
                    addUUIDSell(uuidSell, dataFilter);
                    mMonitorHandler.sendMessage(mMonitorHandler.obtainMessage(MONITOR_TO_SELL, dataFilter));
                    break;

            }
        }
    }

    private final static class MonitorHandler extends JUnitHandler<TransactionDataFilter> {
        private WeakReference<MonitorMgr> mRefMonitor;
        private boolean mIsRunningOrderMonitor;
        private boolean mIsRunningSellMonitor;
        private ScheduledExecutorService mScheduledExecutorService;

        public MonitorHandler(Looper looper, MonitorMgr monitorMgr) {
            super(looper);
            mRefMonitor = new WeakReference<>(monitorMgr);
            mScheduledExecutorService = Executors.newScheduledThreadPool(2);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.d(this, "handleMessage()");
            MonitorMgr monitorMgr = mRefMonitor.get();
            if (monitorMgr == null || monitorMgr.mContext == null) {
                return;
            }

            int what = msg.what;
            Log.d(this, "handleMessage() what = " + what);
            switch (what) {
                case MONITOR_ORDER:
                    if (!mIsRunningOrderMonitor) {
                        Log.d(this, "handleMessage() running order monitor");
                        mIsRunningOrderMonitor = true;
                        //schedule worker
                        mScheduledExecutorService.scheduleAtFixedRate(
                                WorkerMonitorOrder.getInstance(monitorMgr.mContext), 0,
                                PERIOD_MONITOR_ORDER_SECONDS, TimeUnit.SECONDS);
                    }
                    break;
                case MONITOR_TO_SELL:
                    if (!mIsRunningSellMonitor) {
                        Log.d(this, "handleMessage() running sell monitor");
                        mIsRunningSellMonitor = true;
                        mScheduledExecutorService.scheduleAtFixedRate(
                                WorkerMonitorToSell.getInstance(monitorMgr.mContext), 0,
                                PERIOD_MONITOR_TO_SELL_SECONDS, TimeUnit.SECONDS);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
