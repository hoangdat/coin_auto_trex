package com.kickass.android.autotrex.abstraction.database.table;

/**
 * Created by dat.ph on 1/17/2018.
 */

public class TransactionTable extends AbsExchangeTable {
    public static final String TABLE_NAME = "transaction_order";

    public TransactionTable() {
        super();
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    public static class TransactionColumns extends BaseColumns {
        public static final String MARKET_NAME = "market_name";
        public static final String PRICE_BUY = "price_buy";
        public static final String TIME_BUY = "time_buy";
        public static final String EXPECTED_TAKE_PROFIT = "expected_tp";
        public static final String EXPECTED_STOP_LOSS = "expected_sl";
        public static final String PRICE_SELL = "price_sell";
        public static final String TIME_SELL = "time_sell";
        public static final String STATUS = "status";
        public static final String QUANTITY = "quantity";
        public static final String BUY_UUID = "buy_uuid";
        public static final String SELL_UUID = "sell_uuid";
        public static final String BASE_MARKET = "base_market";

        public static final String AS_USED_BUDGET = "used_budget";
    }

    @Override
    public void initTable() {
        mColumns.add(new ColumnFormat(TransactionColumns.ID, "INTEGER PRIMARY KEY AUTOINCREMENT,"));
        mColumns.add(new ColumnFormat(TransactionColumns.MARKET_NAME, "TEXT,"));
        mColumns.add(new ColumnFormat(TransactionColumns.PRICE_BUY, "REAL,"));
        mColumns.add(new ColumnFormat(TransactionColumns.TIME_BUY, "INTEGER,"));
        mColumns.add(new ColumnFormat(TransactionColumns.EXPECTED_TAKE_PROFIT, "REAL,"));
        mColumns.add(new ColumnFormat(TransactionColumns.EXPECTED_STOP_LOSS, "REAL,"));
        mColumns.add(new ColumnFormat(TransactionColumns.PRICE_SELL, "REAL,"));
        mColumns.add(new ColumnFormat(TransactionColumns.TIME_SELL, "INTEGER,"));
        mColumns.add(new ColumnFormat(TransactionColumns.STATUS, "TEXT,"));
        mColumns.add(new ColumnFormat(TransactionColumns.QUANTITY, "REAL,"));
        mColumns.add(new ColumnFormat(TransactionColumns.BASE_MARKET, "TEXT,"));
        mColumns.add(new ColumnFormat(TransactionColumns.BUY_UUID, "TEXT,"));
        mColumns.add(new ColumnFormat(TransactionColumns.SELL_UUID, "TEXT"));
    }
}
