package com.kickass.android.autotrex.abstraction.transaction;

import android.content.Context;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;

import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.bittrex.transaction.BittrexTransactionImp;
import com.kickass.android.autotrex.listener.MarketHistoryObserver;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.obsevable.Observable;
import com.kickass.android.autotrex.obsevable.Observer;
import com.kickass.android.autotrex.service.ExchangeService;
import com.kickass.android.autotrex.service.filter.data.TransactionDataFilter;
import com.kickass.android.autotrex.util.JUnitHandler;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dat.ph on 1/17/2018.
 */

public class TransactionMgr implements Observer {
    private final static long ENOUGH_ANALYSIS_TIME = 1000 * 60 * 15;
    private HashMap<ExchangeOperationMgr.ExchangeType, TransactionImp> mMapTransaction = new HashMap<>();
    private TransactionHandler mTransactionHandler;
    private HandlerThread mTransactionThread;

    private static final String TRANSACTION_THREAD_NAME = "transaction_thread";
    private static final int LIMIT_BUY_MSG = 1;
    private static final int LIMIT_SELL_MSG = 2;
    private Map<String, Integer> mProcessingMarket;

    public enum StatusType {
        PLACE_BUY("place_buy"),
        BOUGHT("bought"),
        PLACE_SELL("place_sell"),
        SOLD("sold");


        private String mValue;

        StatusType(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        public static StatusType fromString(String i) {
            for (StatusType statusType : StatusType.values()) {
                if (TextUtils.equals(statusType.getValue(), i)) {
                    return statusType;
                }
            }
            return null;
        }
    }

    private static TransactionMgr sInstance;

    private TransactionMgr(Context context) {
        mMapTransaction.put(ExchangeOperationMgr.ExchangeType.Bittrex, new BittrexTransactionImp(context));

        Observable.getInstance().addObserver(Observable.ObservableType.MARKET_HISTORY, this);
        Observable.getInstance().addObserver(Observable.ObservableType.TRANSACTION, this);

        mProcessingMarket = Collections.synchronizedMap(new HashMap<String, Integer>());

        mTransactionThread = new HandlerThread(TRANSACTION_THREAD_NAME, Process.THREAD_PRIORITY_BACKGROUND);
        mTransactionThread.start();

        Looper looper = mTransactionThread.getLooper();
        if (looper != null) {
            mTransactionHandler = new TransactionHandler(looper, this);
        }
    }

    public synchronized static TransactionMgr getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new TransactionMgr(context);
        }
        return sInstance;
    }

    public void putToProcessingCache(String marketName, int what) {
        mProcessingMarket.put(marketName, what);
    }

    public boolean isInProcessingCache(String marketName, int what) {
        boolean ret = false;
        Integer objInt = mProcessingMarket.get(marketName);
        if (objInt != null) {
            if (what == objInt.intValue()) {
                ret = true;
            }
        }
        Log.d(this, "isInProcessingCache() - m = " + marketName + "-w = " + what + "-r = " + ret);
        return ret;
    }

    public void removeFromProcessingCache(String marketName) {
        mProcessingMarket.remove(marketName);
    }

    @Override
    public void update(Observable o, Observable.ObservableType obType, Object arg) {
        switch (obType) {
            case MARKET_HISTORY:
                onMarketHistoryNotify((MarketHistoryObserver.MarketHistoryDataFilter) arg);
                break;
            case TRANSACTION:
                onTransactionNotify((TransactionDataFilter) arg);
                break;
            default:
                break;
        }
    }

    private void onTransactionNotify(TransactionDataFilter dataFilter) {
        Log.d(this, "onTransactionNotify");
        ExchangeOperationMgr.ActionType actionType = dataFilter.getActionType();
        if (dataFilter != null && dataFilter.getOrderInfo() != null) {
            Order order = dataFilter.getOrderInfo();
//            String uuid = order.getUuid();
//            if (uuid == null) {
//                return;
//            }
            String marketName = order.getMarketName();
            ExchangeOperationMgr.ExchangeType exchangeType = dataFilter.getExchangeType();
            if (marketName != null && exchangeType != null) {
                switch (actionType) {
                    case Sell_Limit:
                        Log.d(this, "sell - " + marketName);
                        if (!readyToSell(marketName)) {
                            Log.d(this, "sell - " + marketName + "-not ready");
                            return;
                        }
                        putToProcessingCache(marketName, LIMIT_SELL_MSG);
                        mTransactionHandler.sendMessage(mTransactionHandler.obtainMessage(LIMIT_SELL_MSG, dataFilter));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void onMarketHistoryNotify(MarketHistoryObserver.MarketHistoryDataFilter data) {
        Log.d(this, "onMarketHistoryNotify");
        ExchangeOperationMgr.ActionType actionType = data.getActionType();
        String marketName = data.getMarketName();
        ExchangeOperationMgr.ExchangeType exchangeType = data.getExchangeType();
        int what = -1;

        if (marketName != null && exchangeType != null) {
            MarketItem.BasedMarketType basedMarketType = mMapTransaction.get(exchangeType).getBaseMarket(marketName);
            switch (actionType) {
                case Buy_Limit:
                    Log.d(this, "buy - " + marketName);
                    if (!readyToBuy(exchangeType, basedMarketType, marketName)) {
                        Log.d(this, "buy - " + marketName + "-not ready");
                        return;
                    }
                    putToProcessingCache(marketName, LIMIT_BUY_MSG);
                    setUsingBudget(exchangeType, basedMarketType, true);
                    what = LIMIT_BUY_MSG;
                    break;
                default:
                    break;
            }
        }

        if (what != -1) {
            Log.d(this, "buy - " + marketName + "-send trans handler");
            mTransactionHandler.sendMessage(mTransactionHandler.obtainMessage(what, data));
        }
    }

    private boolean readyToBuy(ExchangeOperationMgr.ExchangeType exchangeType, MarketItem.BasedMarketType basedMarketType, String marketName) {
        return (!isInProcessingCache(marketName, LIMIT_BUY_MSG) && isEnoughAnalysisTime() && !isUsingBudget(exchangeType, basedMarketType));
    }

    private boolean readyToSell(String marketName) {
        return !isInProcessingCache(marketName, LIMIT_SELL_MSG);
    }

    private boolean isUsingBudget(ExchangeOperationMgr.ExchangeType exchangeType, MarketItem.BasedMarketType basedMarketType) {
        boolean ret = true;
        TransactionImp transactionImp = mMapTransaction.get(exchangeType);
        if (transactionImp != null) {
//            Log.d(this, "isUsingBudget() b = " + basedMarketType +  "not null");
            ret = transactionImp.isBudgetUsing(basedMarketType);
            Log.d(this, "isUsingBudget() b = " + basedMarketType +  "-ret = " + ret);
        }
        Log.d(this, "isUsingBudget() b = " + basedMarketType +  "-ret = " + ret);
        return ret;
    }

    private void setUsingBudget(ExchangeOperationMgr.ExchangeType exchangeType, MarketItem.BasedMarketType basedMarketType, boolean using) {
        Log.d(this, "setUsingBudget " + basedMarketType);
        TransactionImp transactionImp = mMapTransaction.get(exchangeType);
        if (transactionImp != null) {
            Log.d(this, "setUsingBudget base = " + basedMarketType + "-" + using);
            transactionImp.setUsingBudget(basedMarketType, using);
        }
    }

    private boolean isEnoughAnalysisTime() {
        long elapse = System.currentTimeMillis() - ExchangeService.getStartTime();
//        Log.d(this, "isEnoughAnalysisTime() elapse = " + elapse);
        boolean ret = (elapse >= ENOUGH_ANALYSIS_TIME);
        Log.d(this, "isEnoughAnalysisTime() ret = " + ret);
        return ret;
    }

    public void setBudget(ExchangeOperationMgr.ExchangeType exchangeType, MarketItem.BasedMarketType basedMarketType, double budget) {
        Log.d(this, "setBudget budget = " + budget);
        TransactionImp transactionImp = mMapTransaction.get(exchangeType);
        if (transactionImp != null) {
            transactionImp.setBudget(basedMarketType, budget);
        }
    }

    public void setTPSL(ExchangeOperationMgr.ExchangeType exchangeType, double takeProfit, double stopLoss) {
        TransactionImp transactionImp = mMapTransaction.get(exchangeType);
        if (transactionImp != null) {
            transactionImp.setPercentTPSL(takeProfit, stopLoss);
        }
    }

    private void doSellLimit(TransactionDataFilter data) {
        Log.d(this, "doSellLimit(): " + data);
        ExchangeOperationMgr.ExchangeType exchangeType = data.getExchangeType();
        TransactionImp transactionImp = mMapTransaction.get(exchangeType);
        if (transactionImp != null) {
            transactionImp.doSellLimit(data);
        }
    }

    private void doBuyLimit(MarketHistoryObserver.MarketHistoryDataFilter data) {
        Log.d(this, "doBuyLimit(): " + data);
        if (data == null) {
            return;
        }
        ExchangeOperationMgr.ExchangeType exchangeType = data.getExchangeType();
        TransactionImp transactionImp = mMapTransaction.get(exchangeType);
        if (transactionImp != null) {
            transactionImp.doBuyLimit(data);
        }
    }

    private final static class TransactionHandler extends JUnitHandler {
        private WeakReference<TransactionMgr> mRefTransaction;
        public TransactionHandler(Looper looper, TransactionMgr transactionMgr) {
            super(looper);
            mRefTransaction = new WeakReference<>(transactionMgr);
        }

        @Override
        public void handleMessage(Message msg) {
            int what = msg.what;
            TransactionMgr transactionMgr = mRefTransaction != null ? mRefTransaction.get() : null;
            if (transactionMgr != null) {
                Log.d(this, "handleMessage() " + what);
                switch (what) {
                    case LIMIT_BUY_MSG:
                        MarketHistoryObserver.MarketHistoryDataFilter dataFilter = (MarketHistoryObserver.MarketHistoryDataFilter) msg.obj;
                        transactionMgr.doBuyLimit(dataFilter);
                        break;
                    case LIMIT_SELL_MSG:
                        TransactionDataFilter transactionDataFilter = (TransactionDataFilter) msg.obj;
                        transactionMgr.doSellLimit(transactionDataFilter);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
