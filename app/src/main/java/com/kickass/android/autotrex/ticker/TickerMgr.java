package com.kickass.android.autotrex.ticker;

import android.content.Context;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.widget.TextView;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.abstraction.operation.ExchangeOperationMgr;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.JUnitHandler;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dat.ph on 1/11/2018.
 */

public class TickerMgr {
    private static final String TICKER_THREAD_NAME = "ticker_thread_";
    private static final int MAX_THREAD = Runtime.getRuntime().availableProcessors() / 2;

    private HandlerThread[] mTickerThread;
    private TickerHandler[] mTickerHandler;
    private int mCurThreadIndex = 0;

    private UIWeakRefHandler mUiHandler = new UIWeakRefHandler();

    private Context mContext;

    private static TickerMgr sInstance;

    public static TickerMgr getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new TickerMgr(context.getApplicationContext());
        }
        return sInstance;
    }

    private TickerMgr(Context context) {
        mContext = context;

        mTickerThread = new HandlerThread[MAX_THREAD];
        mTickerHandler = new TickerHandler[MAX_THREAD];

        for (int i = 0; i < MAX_THREAD; i++) {
            mTickerThread[i] = new HandlerThread(TICKER_THREAD_NAME + i, Process.THREAD_PRIORITY_BACKGROUND);
            mTickerThread[i].start();

            Looper looper = mTickerThread[i].getLooper();

            if (looper != null) {
                mTickerHandler[i] = new TickerHandler(looper);
            }
        }
    }

    public void loadPrice(ExchangeOperationMgr.ExchangeType exchangeType, String marketName, TextView txtPrice) {
        if (txtPrice == null) {
            Log.d(this, "loadPrice");
        }

        txtPrice.setTag(marketName);
        txtPrice.setText("-");
        TickerRegInfo info = new TickerRegInfo(exchangeType, marketName, txtPrice);
        mTickerHandler[mCurThreadIndex].sendMessageAtFrontOfQueue(mTickerHandler[mCurThreadIndex].obtainMessage(0, info));

        mCurThreadIndex++;
        if (mCurThreadIndex >= MAX_THREAD) {
            mCurThreadIndex = 0;
        }
    }

    private final class TickerHandler extends JUnitHandler<TickerRegInfo> {
        public TickerHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            TickerRegInfo info = getMsgValue(msg.obj);

            if (info != null) {
                if (info.mTicker == null) {
                    info.mTicker = ExchangeOperationMgr.getInstance(mContext).getTicker(info.mExchangeType, info.mMarketName);
                }

                if (info.mTicker != null) {
                    mUiHandler.sendMessageAtFrontOfQueue(mUiHandler.obtainMessage(0, new Object[]{info, mContext}));
                }
            }
        }
    }

    private static class UIWeakRefHandler extends JUnitHandler<Object[]> {
        @Override
        public void handleMessage(Message msg) {
            if (msg != null) {
                Object[] params = getMsgValue(msg.obj);
                TickerRegInfo tickerRegInfo = (TickerRegInfo) params[0];
                Context context = (Context) params[1];
                if (tickerRegInfo != null) {
                    TextView txtPrice = tickerRegInfo.mTxtPrice.get();
                    if (txtPrice != null) {
                        String tag = (String) txtPrice.getTag();
                        if (tag != null && tag.equals(tickerRegInfo.mMarketName)) {
                            setLastPriceText(context, txtPrice, tickerRegInfo);
                        }
                    }
                }
            }
        }
    }

    private static class TickerRegInfo {
        private ExchangeOperationMgr.ExchangeType mExchangeType;
        private String mMarketName;
        private WeakReference<TextView> mTxtPrice;
        private Ticker mTicker;

        private TickerRegInfo(ExchangeOperationMgr.ExchangeType exchangeType, String marketName, TextView txtPrice) {
            mExchangeType = exchangeType;
            mMarketName = marketName;
            mTxtPrice = new WeakReference<>(txtPrice);
        }
    }

    private static void setLastPriceText(Context context, TextView txtView, TickerRegInfo tickerRegInfo) {
        if (tickerRegInfo == null) {
            return;
        }

        Ticker ticker = tickerRegInfo.mTicker;

        if (ticker == null) {
            return;
        }

        HashMap<Integer, ArrayList<Integer>> mapValueIndex = new HashMap<>();
        ArrayList<Integer> listIndex = new ArrayList<>();

        SpannableStringBuilder displayString = new SpannableStringBuilder("Bid: ");
        int curIndex = displayString.length();
        int startIndex = curIndex;
        String bidValue = Double.toString(ticker.getBid());
        displayString.append(bidValue);
        curIndex = displayString.length();
        TextAppearanceSpan textSpan = new TextAppearanceSpan(context, R.style.Span_Bid);
        displayString.setSpan(textSpan, startIndex, curIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        displayString.append("   Last: ");
        curIndex = displayString.length();

        String priceValue = Double.toString(ticker.getLast());
        listIndex.clear();
        startIndex = curIndex;
        displayString.append(priceValue);
        curIndex = displayString.length();
        textSpan = new TextAppearanceSpan(context, R.style.Span_Price);
        displayString.setSpan(textSpan, startIndex, curIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        displayString.append("   Ask: ");
        curIndex = displayString.length();

        String askValue = Double.toString(ticker.getAsk());
        listIndex.clear();
        startIndex = curIndex;
        displayString.append(askValue);
        curIndex = displayString.length();
        textSpan = new TextAppearanceSpan(context, R.style.Span_Ask);
        displayString.setSpan(textSpan, startIndex, curIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtView.setText(displayString);
    }
}
