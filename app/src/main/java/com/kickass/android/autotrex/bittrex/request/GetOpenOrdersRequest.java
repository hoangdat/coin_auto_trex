package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;
import com.kickass.android.autotrex.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by dat.ph on 1/26/2018.
 */

public class GetOpenOrdersRequest extends AbsBittrexRequest<ArrayList<Order>>{
    public GetOpenOrdersRequest(String url, boolean isRequiredAuth, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, null, listener, errorListener);
    }

    public static GetOpenOrdersRequest createInstance(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl();
        return new GetOpenOrdersRequest(url, true, listener, errorListener);
    }

    private static String makeUrl() {
        String url = BittrexConstant.ENDPOINT_V1_1 + BittrexConstant.ACCOUNT_API +
                Constant.SPLASH + BittrexConstant.GET_OPEN_ORDERS;
        return addParameter(url, true);
    }

    @Override
    protected ArrayList<Order> parse(JSONObject response) {
        Log.d(this, "parse: res " + response);
        ArrayList<Order> orders = new ArrayList<>();
        if (response != null) {
            try {
                JSONArray resultArr = response.getJSONArray(BittrexConstant.RESPONSE_RESULT);
                int length = resultArr.length();
                for (int i = 0; i < length; i++) {
                    JSONObject result = resultArr.getJSONObject(i);
                    String uuid = result.getString(BittrexConstant.RESPONSE_ORDER_UUID);
                    String marketName = result.getString(BittrexConstant.RESPONSE_ORDER_EXCHANGE);
                    String type = result.getString(BittrexConstant.RESPONSE_TYPE);
                    double quantity = result.getDouble(BittrexConstant.RESPONSE_QUANTITY);
                    double quantityRemaining = result.getDouble(BittrexConstant.RESPONSE_QUANTITY_REMAINING);
                    double price = result.getDouble(BittrexConstant.RESPONSE_PRICE);
                    Double price_per_unit = result.getDouble(BittrexConstant.RESPONSE_PRICE_PER_UNIT);
                    Order.OrderType orderType = Order.OrderType.fromString(type);

                    Log.d(this, "parse p = " + price + "-ppu = " + price_per_unit);
                    Order order;
                    if (orderType == Order.OrderType.LIMIT_BUY) {
                        order = new Order(uuid, null, marketName, quantity, quantityRemaining, orderType,
                                price_per_unit != null ? price_per_unit.doubleValue() : 0);
                    } else {
                        order = new Order(null, uuid, marketName, quantity, quantityRemaining, orderType,
                                price_per_unit != null ? price_per_unit.doubleValue() : 0);
                    }
                    orders.add(order);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return orders;
    }
}
