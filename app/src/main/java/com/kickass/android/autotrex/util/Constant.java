package com.kickass.android.autotrex.util;

/**
 * Created by dat.ph on 12/6/2017.
 */

public class Constant {
    public static final String SPLASH = "/";
    public static final String QUESTION_MARK = "?";
    public static final String EQUAL_SIGN = "=";
    public static final String AND_SIGN = "&";

    public static final String DISMISS_DIALOG = "com.kickass.android.autotrex.DISMISS_DIALOG";
    public static final String EXTRA_TAG = "com.kickass.android.autotrex.TAG";

    public static final String SUGGESTION_INTENT_ACTION = "com.kickass.android.autotrex.provider.coinsuggession.intent_action";
}
