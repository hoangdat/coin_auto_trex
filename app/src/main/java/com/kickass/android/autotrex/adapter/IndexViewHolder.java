package com.kickass.android.autotrex.adapter;

/**
 * Created by Dat on 9/9/2017.
 */
public class IndexViewHolder {
    public static int index = 0;
    public static final int MH_MARKET_NAME = index++;
    public static final int MH_BASE_24H_VOLUME = index++;
    public static final int MH_TOTAL_VOLUME = index++;
    public static final int MH_BUY_VOLUME = index++;
    public static final int MH_SELL_VOLUME = index++;
    public static final int MH_MIN_START_DONE = index++;
    public static final int TR_PRICE_BUY_VALUE = index++;
    public static final int TR_QUANTITY_VALUE = index++;
    public static final int TR_PRICE_SELL_VALUE = index++;
    public static final int TR_TIME_BUY_VALUE = index++;
    public static final int TR_TIME_SELL_VALUE = index++;
    public static final int TR_UUID_VALUE = index++;
    public static final int TR_STT_VALUE = index++;
    public static final int MAX = index;
}
