package com.kickass.android.autotrex.bittrex.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.kickass.android.autotrex.abstraction.database.AbsExchangeDbHelper;
import com.kickass.android.autotrex.abstraction.database.table.AbsExchangeTable;
import com.kickass.android.autotrex.abstraction.database.table.BuySellPointTable;
import com.kickass.android.autotrex.abstraction.database.table.MarketHistoryTable;
import com.kickass.android.autotrex.abstraction.database.table.TransactionTable;
import com.kickass.android.autotrex.provider.ExchangeProvider;

import java.util.HashMap;

/**
 * Created by dat.ph on 12/29/2017.
 */

public class BittrexDBHelper extends AbsExchangeDbHelper {
    private final static String DB_NAME = "bittrex";
    private HashMap<String, AbsExchangeTable> mTables;

    private BittrexDBHelper(Context context) {
        super(context, DB_NAME + ".db", null, ExchangeProvider.DATABASE_VERSION);
        mTables = new HashMap<>();
        mTables.put(MarketHistoryTable.TABLE_NAME, new MarketHistoryTable());
        mTables.put(TransactionTable.TABLE_NAME, new TransactionTable());
        mTables.put(BuySellPointTable.TABLE_NAME, new BuySellPointTable());
    }

    private static BittrexDBHelper sInstance;

    public synchronized static BittrexDBHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new BittrexDBHelper(context);
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (AbsExchangeTable table : mTables.values()) {
            table.dropTable(sqLiteDatabase);
            table.createTable(sqLiteDatabase);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}
