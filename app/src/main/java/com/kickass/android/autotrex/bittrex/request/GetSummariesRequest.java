package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.bittrex.Utils;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dat.ph on 1/8/2018.
 */

public class GetSummariesRequest extends AbsBittrexRequest<List<MarketSummary>> {
    public GetSummariesRequest(String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
    }

    public static GetSummariesRequest createInstance(Response.Listener<JSONObject> listener, Response.ErrorListener err) {
        String url = makeUrl();
        return new GetSummariesRequest(url, false, null, listener, err);
    }

    private static String makeUrl() {
        return addParameter(BittrexConstant.ENDPOINT + Constant.SPLASH +
                BittrexConstant.VER_1_1 + Constant.SPLASH +
                BittrexConstant.PUBLIC_API + Constant.SPLASH +
                BittrexConstant.GET_SUMMARIES, false);
    }

    @Override
    protected List<MarketSummary> parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        ArrayList<MarketSummary> summaries = new ArrayList<>();
        if (response != null) {
            try {
                JSONArray resultArr = response.getJSONArray(BittrexConstant.RESPONSE_RESULT);
                for (int i = 0; i < resultArr.length(); i++) {
                    JSONObject summary = resultArr.getJSONObject(i);
                    String name = summary.getString(BittrexConstant.RESPONSE_MARKET_NAME);
                    double volume = summary.getDouble(BittrexConstant.RESPONSE_VOLUME);
                    double baseVolume = summary.getDouble(BittrexConstant.RESPONSE_BASE_VOLUME);
                    MarketItem.BasedMarketType basedMarketType = Utils.getBaseFromMarketName(name);
                    Log.d(this, "parse() - name = " + name + "-" + "volume-base = " + volume+ "-" + baseVolume);
                    MarketSummary tmp = new MarketSummary(name, baseVolume, basedMarketType);
                    if (tmp != null) {
                        summaries.add(tmp);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(this, "parse Exception: " + e.getMessage());
            }
        }
        return summaries;
    }
}
