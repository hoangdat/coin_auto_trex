package com.kickass.android.autotrex.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;

import com.kickass.android.autotrex.listener.DialogReceiver;
import com.kickass.android.autotrex.listener.ListenerMgr;
import com.kickass.android.autotrex.log.Log;



public abstract class AbsDialogFragment extends DialogFragment {
    private ListenerMgr mListenerMgr;
    protected Context mContext;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity a = getActivity();
        mContext = a.getBaseContext();
        mListenerMgr = new ListenerMgr();
        mListenerMgr.addListener(new DialogReceiver(mContext, this, ListenerMgr.LifeCycle.CREATE, ListenerMgr.LifeCycle.DESTROY));
        mListenerMgr.notifyCreate();
        return _createDialog();
    }

    public void dismissByBroadcast() {
        dismissAllowingStateLoss();
    }

    @Override
    public void dismissAllowingStateLoss() {
        mListenerMgr.notifyDestroy();
        try {
            super.dismissAllowingStateLoss();
        } catch (IllegalStateException e) {
            Log.e(this, "IllegalStateException:" + e.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mListenerMgr.notifyResume();
    }

    @Override
    public void onPause() {
        mListenerMgr.notifyPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        dismissAllowingStateLoss();
        super.onDestroy();
    }

    protected abstract Dialog _createDialog();
}