package com.kickass.android.autotrex.activity;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kickass.android.autotrex.R;
import com.kickass.android.autotrex.adapter.transaction.TransactionAdapterImp;
import com.kickass.android.autotrex.bittrex.BittrexInfo;
import com.kickass.android.autotrex.dialog.AddKeyDialog;
import com.kickass.android.autotrex.log.Log;

public class TransactionActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, AddKeyDialog.OnSavedAPIKeyListener{
    private TransactionAdapterImp mTransactionAdapter;
    private RecyclerView mRecycleVolume;
    LoaderManager mLoaderMgr;
    int mLoaderID;
    private LinearLayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        init();
    }

    private void init() {
        mRecycleVolume = (RecyclerView) findViewById(R.id.listTransaction);
        mTransactionAdapter = new TransactionAdapterImp(getApplicationContext(), R.layout.transaction_row_item);

        mLayoutManager = new LinearLayoutManager(this);
        mRecycleVolume.setLayoutManager(mLayoutManager);
        mRecycleVolume.setAdapter(mTransactionAdapter);

        mLoaderMgr = getSupportLoaderManager();
        mLoaderID = this.hashCode();

        try {
            mLoaderMgr.initLoader(mLoaderID, null, this);
        } catch (IllegalStateException e) {
            Log.e(this, "IllegalStateException:" + e.toString());
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        if (mTransactionAdapter == null) {
            return null;
        }
        return new CursorLoader(getApplicationContext(), mTransactionAdapter.getUri(), mTransactionAdapter.getProjection(), null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (mTransactionAdapter != null) {
            mTransactionAdapter.changeCursor(cursor);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        try {
            mTransactionAdapter.changeCursor(null);
        } catch (NullPointerException e) {
            Log.e(this, "NullPointerException:" + e.toString());
        }
    }

    @Override
    protected void onResume() {
        BittrexInfo info = BittrexInfo.getInstance(getApplicationContext());
        if (!info.isHasKey() && !BittrexInfo.isAddingKey()) {
            info.addKey(this, this);
        } else {
            info.reloadKey();
        }
        super.onResume();
    }

    @Override
    public void onSavedSuccess(String api, String secret) {
        Log.d(this, "onSavedSuccess: api = " + api + "-s = " + secret);
        BittrexInfo.setKey(api, secret);
    }
}
