package com.kickass.android.autotrex.abstraction.operation;

import android.content.Context;

import com.kickass.android.autotrex.abstraction.response.Balance;
import com.kickass.android.autotrex.abstraction.response.MarketItem;
import com.kickass.android.autotrex.abstraction.response.MarketSummary;
import com.kickass.android.autotrex.abstraction.response.Order;
import com.kickass.android.autotrex.abstraction.response.PriceHistory;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.abstraction.transaction.TransactionMgr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dat.ph on 12/7/2017.
 */

public abstract class AbsOperation {
    private Context mContext;
    public AbsOperation(Context context) {
        mContext = context;
    }

    protected Context getContext() {
        return mContext;
    }

    public abstract HashMap<String, MarketItem> getMarkets();

    public abstract MarketSummary getMarketSum(String market);

    public abstract List<MarketSummary> getSummaries();

    public abstract ArrayList<Balance> getBalances();

    public abstract Balance getBalance(String currency);

    public abstract String buyLimit(Order order);

    public abstract String sellLimit(Order order);

    public abstract Ticker getTicker(String market);

    public abstract ArrayList<PriceHistory> getPriceHistory(String market);

    public abstract boolean updateMarketHistory(String market, long internalTime);

    public abstract ExchangeOperationMgr.ExchangeType getExchangeType();

    public abstract Order getOrder(String uuid);

    public abstract ArrayList<Order> getOpenOrders();

    public abstract void updateOrderInfo(TransactionMgr.StatusType statusType, Order order);

    public abstract ArrayList<Order> getMarketOrderHistory(String market);
}
