package com.kickass.android.autotrex.bittrex.request;

import com.android.volley.Response;
import com.kickass.android.autotrex.abstraction.response.Ticker;
import com.kickass.android.autotrex.bittrex.BittrexConstant;
import com.kickass.android.autotrex.log.Log;
import com.kickass.android.autotrex.util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dat.ph on 12/8/2017.
 */

public class GetTickerRequest extends AbsBittrexRequest<Ticker> {
    private String mMarketName;
    public GetTickerRequest(String marketName, String url, boolean isRequiredAuth, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, isRequiredAuth, jsonRequest, listener, errorListener);
        mMarketName = marketName;
    }

    public static GetTickerRequest createInstance(String market, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        String url = makeUrl(market);
        return new GetTickerRequest(market, url, false, null, listener, errorListener);
    }

    private static String makeUrl(String market) {
        String url = BittrexConstant.ENDPOINT_V1_1 + BittrexConstant.PUBLIC_API +
                Constant.SPLASH + BittrexConstant.GET_TICKER +
                Constant.QUESTION_MARK + BittrexConstant.MARKET_API + Constant.EQUAL_SIGN + market;
        return addParameter(url, false);
    }

    @Override
    protected Ticker parse(JSONObject response) {
        Log.d(this, "parse: " + response);
        Ticker ticker = null;
        try {
            if (response != null) {
                JSONObject result = response.getJSONObject(BittrexConstant.RESPONSE_RESULT);
                double bid = result.getDouble(BittrexConstant.RESPONSE_BID);
                double ask = result.getDouble(BittrexConstant.RESPONSE_ASK);
                double last = result.getDouble(BittrexConstant.RESPONSE_LAST);
                ticker = new Ticker(mMarketName, bid, ask, last);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d(this, "parse: " + response);
        }
        return ticker;
    }
}
